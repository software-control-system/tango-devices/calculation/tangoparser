package fr.soleil.management;

import javax.management.AttributeChangeNotification;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationEmitter;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;

/**
 * Class TangoParserStats
 *
 * @author HARDION
 */
public final class TangoParserStats implements NotificationEmitter {

    private static final double TO_SECS = 1000.0;
    private final NotificationBroadcasterSupport broadcaster = new NotificationBroadcasterSupport();
    private long seqNumber;
    /**
     * Attribute : MeanResponseTime
     */
    private double meanResponseTime;
    /**
     * Attribute : MinResponseTime
     */
    private double minResponseTime;
    /**
     * Attribute : MaxResponseTime
     */
    private double maxResponseTime;
    /**
     * Attribute : LastResponseTime
     */
    private double lastResponseTime;
    /**
     * Attribute : numberOfReading
     */
    private long numberOfReading;

    public TangoParserStats() {
    }

    /**
     * Get Response time of the last read_attributes
     */
    public double getLastResponseTime() {
        return lastResponseTime;
    }

    /**
     * Get Mean response of read attributes
     */
    public double getMeanResponseTime() {
        double result = 0.0D;
        if (numberOfReading != 0) {
            result = meanResponseTime / numberOfReading;
        }
        return result;
    }

    /**
     * Get Min response of read attributes
     */
    public double getMinResponseTime() {
        return minResponseTime;
    }

    /**
     * Get Max response of read attributes
     */
    public double getMaxResponseTime() {
        return maxResponseTime;
    }

    /**
     * Reset Statistic
     */
    public void resetAll() {
        meanResponseTime = 0;
        minResponseTime = 0;
        maxResponseTime = 0;
        lastResponseTime = 0;
        numberOfReading = 0;
    }

    @Override
    public void addNotificationListener(final NotificationListener listener, final NotificationFilter filter,
            final Object handback) {
        broadcaster.addNotificationListener(listener, filter, handback);
    }

    @Override
    public MBeanNotificationInfo[] getNotificationInfo() {
        return new MBeanNotificationInfo[] { new MBeanNotificationInfo(
                new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE },
                javax.management.AttributeChangeNotification.class.getName(), "Attributes has been reading") };
    }

    @Override
    public void removeNotificationListener(final NotificationListener listener) throws ListenerNotFoundException {
        broadcaster.removeNotificationListener(listener);
    }

    @Override
    public void removeNotificationListener(final NotificationListener listener, final NotificationFilter filter,
            final Object handback) throws ListenerNotFoundException {
        broadcaster.removeNotificationListener(listener, filter, handback);
    }

    public synchronized long getNextSeqNumber() {
        return seqNumber++;
    }

    /*
     * Methods exposed to Anagrams application to feed management with data.
     */

    // Stores the time at which a new anagram is proposed to the user.
    private long startTime;

    /**
     * A new Anagram is proposed to the user: store current time.
     */
    public void startReading() {
        startTime = System.currentTimeMillis();
    }

    /**
     * An Anagram has been resolved.
     */
    public void stopReading() {

        // Update the number of resolved anagrams
        numberOfReading++;

        // Compute last, min and max thinking times
        lastResponseTime = (System.currentTimeMillis() - startTime) / TO_SECS;
        minResponseTime = lastResponseTime < minResponseTime || minResponseTime == 0 ? lastResponseTime
                : minResponseTime;
        maxResponseTime = lastResponseTime > maxResponseTime ? lastResponseTime : maxResponseTime;
        meanResponseTime += lastResponseTime;
        // Create a JMX Notification
        final Notification notification = new Notification(AttributeChangeNotification.ATTRIBUTE_CHANGE, this,
                getNextSeqNumber(), "" + "" + "Attributes read: " + numberOfReading);

        // Send a JMX notification.
        broadcaster.sendNotification(notification);
    }
}
