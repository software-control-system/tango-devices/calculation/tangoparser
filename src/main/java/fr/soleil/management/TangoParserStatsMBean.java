package fr.soleil.management;

/**
 * Interface TangoParserStatsMBean
 *
 * @author HARDION
 */
public interface TangoParserStatsMBean {

    /**
     * Get Response time of the last read_attributes
     */
    double getLastResponseTime();

    /**
     * Get Max response of read attributes
     */
    double getMaxResponseTime();

    /**
     * Get Mean response of read attributes
     */
    double getMeanResponseTime();

    /**
     * Get Min response of read attributes
     */
    double getMinResponseTime();

    /**
     * Reset Statistic
     */
    void resetAll();

}
