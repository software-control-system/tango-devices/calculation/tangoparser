package fr.soleil.tango.server.tangoparser;

import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.attribute.ISetValueUpdater;

import fr.esrf.Tango.DevFailed;

public interface IWriteLockAttributeBehaviour extends IAttributeBehavior, ISetValueUpdater {

	
	public void blockExternalWriting();

	public void releaseExternalWriting();
	
	public boolean isWritingLocked();

	public void setWriteValue(final AttributeValue value);
	
	public void internalSetValue(final AttributeValue value) throws DevFailed;
	
}
