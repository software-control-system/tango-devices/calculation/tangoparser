package fr.soleil.tango.server.tangoparser;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.nfunk.jep.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributePropertiesImpl;
import org.tango.server.attribute.AttributeValue;
import org.tango.utils.DevFailedUtils;

import com.google.common.collect.BiMap;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.util.TypeConversionUtil;
import fr.soleil.tango.parser.AJepParser;
import fr.soleil.tango.parser.JepParserRead;
import fr.soleil.tango.parser.JepParserWrite;
import fr.soleil.tango.parser.constants.ParserConstant;
import fr.soleil.tango.parser.datasource.TangoSource;
import fr.soleil.tango.parser.function.interpolation.TangoParserCustomFunction;

/**
 * @author fourneau
 */
public final class DynamicAttributeParser implements IWriteLockAttributeBehaviour {

	private final Logger logger = LoggerFactory.getLogger(DynamicAttributeParser.class);
	private final AttributeConfiguration config;
	// Threaded or not
	final List<JepParserWrite> jepParserWriteList = new ArrayList<JepParserWrite>();
	protected final TangoSource dataSource;
	private final boolean isSynchronous;
	private WriteExpressionGroup jepParserWriterGroup;
	private final AttributeValue value = new AttributeValue();
	private final List<AJepParser> variablesJep;
	private JepParserRead jepParserRead;
	private boolean isWriteInitialized = false;

	private boolean writingLock = false;
	private Object writeValue;

	public DynamicAttributeParser(final String deviceName, final AttributeConfiguration config,
			final String expressionsR, final List<String> expressionsW, final BiMap<String, String> variables,
			Map<String, ParserConstant> constants, List<TangoParserCustomFunction> functions,
			final TangoSource sourceGroup) throws DevFailed {
		this(deviceName, config, expressionsR, expressionsW, variables, new ArrayList<AJepParser>(), constants,
				functions, sourceGroup);
	}

	public DynamicAttributeParser(final String deviceName, final AttributeConfiguration config,
			final String expressionsR, final List<String> expressionsW, final BiMap<String, String> variables,
			final List<AJepParser> variablesJep, Map<String, ParserConstant> constants,
			List<TangoParserCustomFunction> functions, final TangoSource sourceGroup) throws DevFailed {
		logger.info("/!\\ building attribute \"{}\"", config.getName());
		dataSource = sourceGroup;
		this.variablesJep = variablesJep;
//		if (config.getFormat().equals(AttrDataFormat.IMAGE)) {
//			throw DevFailedUtils.newDevFailed("CONFIG_ERROR", "IMAGE unusupported");
//		}
		this.config = config;
		final AttributePropertiesImpl attributeProperties = new AttributePropertiesImpl();

		// config.setMemorized(true);
		this.isSynchronous = sourceGroup.isSynchronous();
		final AttrWriteType attrWryteType = config.getWritable();
		try {
			if (attrWryteType.equals(AttrWriteType.READ)) {
				jepParserRead = new JepParserRead(deviceName, config.getName(), expressionsR, variables, variablesJep,
						constants, functions, sourceGroup);
				variablesJep.add(jepParserRead);
				attributeProperties.setDescription("expression = " + expressionsR);
			} else if (attrWryteType.equals(AttrWriteType.WRITE)) {
				jepParserRead = null;
				for (final String expression : expressionsW) {
					final JepParserWrite jepParserWrite = new JepParserWrite(deviceName, expression, config.getName(),
							variables, variablesJep, constants, functions, sourceGroup);
					variablesJep.add(jepParserWrite);
					jepParserWriteList.add(jepParserWrite);
				}
				attributeProperties.setDescription("expressions = " + expressionsW);
			} else if (attrWryteType.equals(AttrWriteType.READ_WRITE)) {
				for (final String expression : expressionsW) {
					final JepParserWrite jepParserWrite = new JepParserWrite(deviceName, expression, config.getName(),
							variables, variablesJep, constants, functions, sourceGroup);
					variablesJep.add(jepParserWrite);
					jepParserWriteList.add(jepParserWrite);
				}
				jepParserRead = new JepParserRead(deviceName, config.getName(), expressionsR, variables, variablesJep,
						constants, functions, sourceGroup);
				variablesJep.add(jepParserRead);
				attributeProperties
						.setDescription("expression read: " + expressionsR + "\nexpressions write: " + expressionsW);
			} else {
				throw DevFailedUtils.newDevFailed("CONFIG_ERROR", "unusupported write type");
			}
			this.config.setAttributeProperties(attributeProperties);
			// default value
			if (config.getFormat().equals(AttrDataFormat.SCALAR)) {
				value.setValue(TypeConversionUtil.castToType(config.getType(), Double.NaN));
			} else if (config.getFormat().equals(AttrDataFormat.SPECTRUM)) {
				value.setValue(TypeConversionUtil.castToArray(config.getScalarType(), new double[] { Double.NaN }));
			} else if (config.getFormat().equals(AttrDataFormat.IMAGE)) {
				Class<? extends Object> rowClass = Array.newInstance(config.getScalarType(), 0).getClass();
				Object img = Array.newInstance(rowClass, 1);
				Array.set(img, 0, TypeConversionUtil.castToArray(config.getScalarType(), new double[] { Double.NaN }));
				value.setValue(img);
			}
			logger.info("attribute \"{}\" is ready", this.config.getName());
		} catch (final DevFailed e) {
			logger.error("attribute \"{}\" not built. {}", this.config.getName(), DevFailedUtils.toString(e));
			throw e;
		}

	}

	public String getName() {
		if (jepParserRead != null) {
			return jepParserRead.getName();
		} else {
			// Unlikely case. Only if an error occurred.
			return "";
		}
	}

	@Override
	public AttributeConfiguration getConfiguration() {
		return config;
	}

	public String[] getExpression() {
		String[] allExp;
		final String[] writeExpr = jepParserWriterGroup.getExpressions();
		if (jepParserRead != null) {
			allExp = ArrayUtils.add(writeExpr, 0, jepParserRead.getExpression());
		} else {
			allExp = writeExpr;
		}
		return allExp;
	}

	@Override
	public StateMachineBehavior getStateMachine() throws DevFailed {
		return null;
	}

	@Override
	public AttributeValue getValue() throws DevFailed {
		if (isSynchronous) {
			launchRefresh(true);
		}
		refreshValue(true);
		return value;
	}

	@Override
	public void setValue(final AttributeValue value) throws DevFailed {
		if (!isWritingLocked()) {
			internalSetValue(value);
		} else {
			throw DevFailedUtils.newDevFailed("Attribute writing is locked",
					"The attribute " + getName() + " is not currently writable.");
		}
	}

	public JepParserRead getJepParserRead() {
		return jepParserRead;
	}

	public List<JepParserWrite> getJepParsersWrite() {
		return jepParserWriteList;
	}

	public void launchRefresh(final boolean lock) throws DevFailed {
		if (jepParserRead != null) {
			jepParserRead.refresh(lock);
		}
	}

	public void refreshValue(final boolean lock) throws DevFailed {
		try {
			String name = config.getName();
			logger.debug("refresh value for attribute {}", name);
			Object result;
			if (jepParserRead != null) {
				result = jepParserRead.getValue(lock);
				if (config.getFormat().equals(AttrDataFormat.SCALAR)) {
					value.setValue(TypeConversionUtil.castToType(config.getType(), result), System.currentTimeMillis());
				} else if (config.getFormat().equals(AttrDataFormat.SPECTRUM)) {
					if (result.getClass().isArray()) {
						value.setValue(TypeConversionUtil.castToArray(config.getScalarType(), result),
								System.currentTimeMillis());
					} else {
						// in some cases, result is not an array. ex: test1 = doubleSpectrum/2 with
						// doubleSpectrum with one element
						// set value into an array
						Object array = Array.newInstance(config.getScalarType(), 1);
						Array.set(array, 0, result);
						value.setValue(TypeConversionUtil.castToArray(config.getScalarType(), array),
								System.currentTimeMillis());
					}
				} else if (config.getFormat().equals(AttrDataFormat.IMAGE)) {
					if (result.getClass().isArray() && result.getClass().getComponentType().isArray()) {
						int dim1 = Array.getLength(result);
						Class<?> matClass = Array.newInstance(config.getScalarType(), 0).getClass();
						Object array = Array.newInstance(matClass, dim1);
						for (int i = 0; i < dim1; i++) {
							Object subArray = Array.get(result, i);
							Object translated = TypeConversionUtil.castToArray(config.getScalarType(), subArray);
							Array.set(array, i, translated);
						}
						value.setValue(array, System.currentTimeMillis());
					} else {
						// in some cases, result is not a matrix. ex: test1 = doubleImage/2 with
						// doubleSpectrum with one element
						// set value into an array
						Object array = Array.newInstance(config.getScalarType(), 1);
						Array.set(array, 0, result);
						Object array2 = Array.newInstance(array.getClass(), 1);
						Array.set(array2, 0, array);
						value.setValue(array2, System.currentTimeMillis());
					}
				}
			}
		} catch (final DevFailed e) {
			logger.error(DevFailedUtils.toString(e));
			if (!isWriteInitialized && config.getWritable().equals(AttrWriteType.READ_WRITE)) {
				// set a default value
				value.setValue(TypeConversionUtil.castToType(config.getType(), Double.NaN), System.currentTimeMillis());
			} else {
				throw e;
			}
		}
		logger.debug("read {} on {}", value.getValue(), config.getName());
	}

	public void initExpression() throws ParseException, DevFailed {
		for (AJepParser jepParser : jepParserWriteList) {
			jepParser.initExpression();
		}
		if (jepParserRead != null) {
			jepParserRead.initExpression();
		}
		jepParserWriterGroup = new WriteExpressionGroup(config.getName(), jepParserWriteList, dataSource);
	}

	public List<AJepParser> getJepParsers() {
		return variablesJep;
	}

	@Override
	public void blockExternalWriting() {
		writingLock = true;
	}

	@Override
	public void releaseExternalWriting() {
		writingLock = false;
	}

	@Override
	public void internalSetValue(final AttributeValue value) throws DevFailed {
		logger.debug("writing {} on {}", config.getName(), value.getValue());
		isWriteInitialized = true;
		setWriteValue(value);
		jepParserWriterGroup.setValue(value.getValue(), true);
	}

	@Override
	public boolean isWritingLocked() {
		return writingLock;
	}

	@Override
	public void setWriteValue(final AttributeValue value) {
		this.writeValue = value.getValue();
	}

	@Override
	public AttributeValue getSetValue() throws DevFailed {
		if (writeValue != null) {
			return new AttributeValue(writeValue);
		} else {
			return null;
		}
	}
}
