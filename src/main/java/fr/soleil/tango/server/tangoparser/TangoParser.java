package fr.soleil.tango.server.tangoparser;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.nfunk.jep.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.DeviceState;
import org.tango.server.InvocationContext;
import org.tango.server.ServerManager;
import org.tango.server.annotation.AroundInvoke;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceManagement;
import org.tango.server.annotation.DeviceProperties;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.attribute.log.LogAttribute;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.dynamic.command.AsyncGroupCommand;
import org.tango.server.dynamic.command.GroupCommand;
import org.tango.utils.CircuitBreakerCommand;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.SimpleCircuitBreaker;
import org.tango.utils.TangoUtil;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.management.TangoParserStats;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.util.TypeConversionUtil;
import fr.soleil.tango.parser.AJepParser;
import fr.soleil.tango.parser.JepParserRead;
import fr.soleil.tango.parser.autoinput.AutoInputActivatorAttribute;
import fr.soleil.tango.parser.autoinput.AutoInputAttributeManager;
import fr.soleil.tango.parser.autoinput.AutoInputProperties;
import fr.soleil.tango.parser.constants.ConstantsLoader;
import fr.soleil.tango.parser.datasource.CacheRefresher;
import fr.soleil.tango.parser.datasource.SourceManager;
import fr.soleil.tango.parser.datasource.TangoSource;
import fr.soleil.tango.parser.function.interpolation.InterpolationFunctionPropertyParser;
import fr.soleil.tango.parser.function.interpolation.TangoParserCustomFunction;
import fr.soleil.tango.statecomposer.StateResolver;

@Device(transactionType = TransactionType.NONE)
public final class TangoParser {

	private static final int DEFAULT_REFRESH_PERIOD = 3000;
	private static final String STOP_COMMAND_NAME = "StopAll";
	private static final String ARRAY_CONSTANT_PREFIX = "array_";

	private static final String FR_SOLEIL_MANAGEMENT_TYPE_TANGO_PARSER_STATS = "fr.soleil.management:type=TangoParserStats";
	private static final String autoInputActivatorTag = "AutoInputActivation";
	private final Logger logger = LoggerFactory.getLogger(TangoParser.class);
	private final XLogger xlogger = XLoggerFactory.getXLogger(TangoParser.class);
	private static volatile CacheRefresher CACHE_REFRESHER = null;

	/**
	 * The request grouped commands
	 */
	@DeviceProperty
	private String[] commandNameList;

	/**
	 * The number version of the device
	 */
	@Attribute(displayLevel = DispLevel._EXPERT)
	private static String version = "";

	/**
	 * To create diagnostic functions (mean, std, min, max)
	 */
	@DeviceProperty(name = "autoInitEnabled", description = "Enable the auto init.", defaultValue = "true")
	private boolean autoInitEnabled = true;

	/**
	 * List of attributes used with the format : name,proxyAttribute
	 */
	@DeviceProperty(description = "List of attributes used with the format : name,proxyAttribute")
	private String[] attributeNames = { "" };

	/**
	 * List of scalar values used with the format : name,type value
	 */
	@DeviceProperty(description = "List of scalar constant used with the format : name,type value. Example: 'c, int 299792458' or 'planck, double 6.62607015e10-34'")
	private String[] scalarConstants = { "" };

	/**
	 * List of attributes used with the format : name,proxyAttribute
	 */
	@DeviceProperty(description = "List of file containing arrays used with the format : name,file.")
	private String[] doubleArrayConstants = { "" };

	/**
	 * List of scalar values used with the format : name,type value
	 */
	@DeviceProperty(description = "List of interpolation functions. Each lines must be like : functionName, interpolation type, parameters, ...")
	private String[] interpolationFunctions = { "" };

	/**
	 * To create diagnostic functions (mean, std, min, max)
	 */
	@DeviceProperty(name = "diagnosticFunctions", description = "To create diagnostic functions (mean, std, min, max)")
	private boolean useDiagnosticFunctions = false;
	/**
	 * Manage dynamic WRITE attributes. format : type name,attributeName=expression
	 */
	@DeviceProperty(description = "Manage dynamic WRITE attributes. format : type,name,attributeName=expression")
	private String[] inputNames = { "" };

	/**
	 * Manage dynamic READ_WRITE attributes. format : type
	 * name;readExpression;writeExp1;...;writeExpN
	 */
	@DeviceProperty(description = "Manage dynamic READ_WRITE attributes. format : type name;readExpression;writeExp1;...;writeExpN")
	private String[] iONames = { "" };
	/**
	 * Property used by Scan server if ScanMode is set to true.
	 */
	@DeviceProperty(description = "Property used by Scan server if ScanMode is set to true.")
	private String movingState = "";

	/**
	 * Manage dynamic READ_WRITE attributes. format : type
	 * name;readExpression;writeExp1;...;writeExpN
	 */
	@DeviceProperty(description = "Manage automating of the writing part of READ_WRITE attributes.\n" + "Format: \n"
			+ "   IO_ATTRIBUTE_NAME;ACTIVATED_AT_INIT;POOLING_PERIOD;PRECISION\n\n" + "Description:\n"
			+ "    IO_ATTRIBUTE_NAME = from the property iONames\n" + "    ACTIVATED_AT_INIT = TRUE|FALSE\n"
			+ "    POOLING_PERIOD=in seconds\n" + "    PRECISION = optionnal\n")
	private String[] autoInputProperties = { "" };

	/**
	 * Manage dynamic READ attributes. format : type name,attributeName=expression
	 */
	@DeviceProperty(description = "Manage dynamic READ attributes. format : type name,attributeName=expression")
	private String[] outputNames = { "" };
	/**
	 * The period at which state and attributes are refreshed in ms if ScanMode is
	 * false. Default value is 3000
	 */
	@DeviceProperty(description = "The period at which state and attributes are refreshed in ms if ScanMode is false."
			+ " Default value is 3000", defaultValue = "3000")
	private int periodRefresh = DEFAULT_REFRESH_PERIOD;

	/**
	 * The priority list of states : STATE,priority
	 */
	@DeviceProperty(description = "The priority list of states : STATE,priority")
	private String[] priorityList = { "" };
	/**
	 * Define if attributes and state client request are synchronous or not. If not
	 * synchronous a refresh period must be defined. To scan this device ScanMode
	 * must be true.
	 */
	@DeviceProperty(description = "Define if attributes and state client request are synchronous or not. "
			+ "If not synchronous a refresh period must be defined. To scan this device ScanMode must be true.")
	private boolean scanMode = true;

	@DeviceProperties
	private Map<String, String[]> deviceProperties;

	/**
	 * The list of commands to stop all underlying devices
	 */
	@DeviceProperty
	private String[] stopCommandFullNameList;

	@DynamicManagement
	private DynamicManager dynMngt;

	@DeviceManagement
	private DeviceManager device;

	private boolean initFailed = false;
	private List<String> initFailedAttributes = new ArrayList<>();
	private Set<String> initAttributesRemaining = new TreeSet<>();

	// key is attribute name , value is variable name
	private final BiMap<String, String> attribMap = HashBiMap.create();

	private final TangoParserStats mBeans = new TangoParserStats();

	private String[] oNames;

	/**
	 * The state of the device
	 */
	@State
	private DeviceState state = DeviceState.ON;

	private StateResolver stateResolver;

	/**
	 * The status of the device
	 */
	@Status
	private String status = "";

	private String initErrorText = "";

	private List<AJepParser> variablesJep = new ArrayList<AJepParser>();

	private ConstantsLoader constantsLoader = new ConstantsLoader();

	private List<TangoParserCustomFunction> functions = new ArrayList<>();

	private List<AutoInputAttributeManager> autoInputAttributeManagerList = null;
	/**
	 * A single datasource manager per server. When scanMode==false, all cache is
	 * managed per server
	 */
	private static SourceManager srcManager = new SourceManager();

	/**
	 * This function is called before and after a read/write attribute or execution
	 * command
	 *
	 * In the case of reading an/() attribute(s) we need to refresh the attributes
	 * values BEFORE parsing the expression.
	 *
	 * In the case of writing an attribute we need to parse expression AFTER
	 * updating the attribute.
	 *
	 * @param ctx
	 * @throws DevFailed
	 */
	@AroundInvoke
	public void aroundInvoke(final InvocationContext ctx) throws DevFailed {
		xlogger.entry(ctx);
		switch (ctx.getContext()) {
		case PRE_READ_ATTRIBUTES:
			mBeans.startReading();
			break;
		case POST_READ_ATTRIBUTES:
			mBeans.stopReading();
			break;
		default:
			break;
		}
		xlogger.exit();
	}

	/**
	 * Check if Attributes, InputNames or OutputNames is correctly initialized. If
	 * she correlty iniatlized, the methode return an empty String else the methode
	 * return a String which indicate the error( attributeNames empty, in
	 * propertyName the comma is missing at line ...).
	 *
	 * AttributeNames musn't be empty. Inputnames and OutputNames can be empty.
	 *
	 * If property (AttributeNames, Inputnames or OutputNames) isn't empty, she must
	 * contains a varaible name and a comma.
	 *
	 * @param propertyName    The property name.
	 * @param propertyValue   property which have to check.
	 * @param canBeEmpty      This parameter = true is the property can be empty.
	 * @param propertyPattern regex to check if the property has correct syntaxe. if
	 *                        property can be empty the pattern isn't check.
	 * @param syntax          pattern and exemple of property
	 * @return errorMessage if the property is correlty iniatlized, errorMessage =
	 *         "" else error message equals a String which indicate the error
	 *         (attributeNames empty, in propertyName the comma is missing at line
	 *         ...).
	 * @throws DevFailed
	 */
	private void checkProperty(final String propertyName, final boolean canBeEmpty, final String propertyPattern,
			final String syntax, final String... propertyValue) throws DevFailed {
		xlogger.entry();
		logger.debug("checking prop {} with values {}", propertyName, Arrays.toString(propertyValue));

		// contains error
		// message if the
		// property doesn't contains a comma and a
		// variable name.

		// attributeName mustn't be empty.
		if (!canBeEmpty && propertyValue.length == 0) {
			final StringBuilder errorMessage = new StringBuilder().append(propertyName).append(" property is empty");
			throw DevFailedUtils.newDevFailed(errorMessage.toString());
		} else {
			// the property can be empty.If the property isn't empty we test if
			// comma and variable name are present.
			if (propertyValue.length >= 1 && !propertyValue[0].trim().isEmpty()) {
				// contains the list of incorrect lines in porpertyValue.
				final StringBuilder propertyLinesError = new StringBuilder();
				// check if the property has a correct pattern
				for (int i = 0; i < propertyValue.length; i++) {
					if (!propertyValue[i].matches(propertyPattern)) {
						propertyLinesError.append(i + 1).append(", ");
					}
				}

				// if the pattern is incorrect then we delete
				// the last comma of propertyLineError and set errorMessage.
				if (!(propertyLinesError.length() == 0)) {
					final StringBuilder errorMessage = new StringBuilder();// String which
					errorMessage.append(propertyName);
					errorMessage.append(" has incorrect syntax at line(s): ");
					errorMessage.append(propertyLinesError.substring(0, propertyLinesError.length() - 2));
					errorMessage.append(".\nCorrect syntax is: ");
					errorMessage.append(syntax);
					throw DevFailedUtils.newDevFailed("Error Property " + propertyName, errorMessage.toString());
				}
			}
		}
	}

	/**
	 * Clear all the the tables and the global variables
	 *
	 * @throws DevFailed
	 *
	 */
	@Delete
	public void deleteDevice() throws DevFailed {
		xlogger.entry();
		dynMngt.clearAll();
		attribMap.clear();
		variablesJep.clear();
		if (stateResolver != null && stateResolver.isStarted()) {
			stateResolver.stop();
		}

		if (autoInputAttributeManagerList != null) {
			autoInputAttributeManagerList.forEach(autoInputAttrMgr -> autoInputAttrMgr.stop());
			autoInputAttributeManagerList.clear();
			autoInputAttributeManagerList = null;
		}
		xlogger.exit();
	}

	/**
	 *
	 * @param argin
	 * @return
	 * @throws DevFailed
	 */
	@Command(name = "EvaluateExpression", inTypeDesc = "an expression to evaluate", outTypeDesc = "the evaluated expression")
	public String[] evaluateExpression(final String argin) throws DevFailed {
		xlogger.entry();
		final String expression = argin;
		final String deviceName = device.getName();
		final JepParserRead jepParserRead = new JepParserRead(deviceName, "tmp", expression, attribMap, variablesJep,
				constantsLoader.getConstants(), functions, srcManager.build(true, device.getName(), expression));
		try {
			jepParserRead.initExpression();
		} catch (ParseException e) {
			logger.error("expresion \"{}\" contains errors. {}", argin, e.getMessage());
			e.printStackTrace();
			throw DevFailedUtils.newDevFailed(e);
		}
		jepParserRead.refresh(true);
		final Object result = jepParserRead.getValue(true);
		String[] argout;
		if (result.getClass().isArray()) {
			if (result.getClass().getComponentType().isArray()) {
				throw DevFailedUtils.newDevFailed("The result value is an image. It's not managed as a command return value.");
			} else {
				argout = (String[]) TypeConversionUtil.castToArray(String.class, result);
			}
		} else {
			argout = new String[] { String.valueOf(result) };
		}

		xlogger.exit();
		return argout;
	}

	@Attribute(displayLevel = DispLevel._EXPERT)
	public String getLastCacheError() {
		String error = "";
		if (CACHE_REFRESHER != null) {
			error = CACHE_REFRESHER.getLastError();
		}
		return error;
	}

	/**
	 * Get the expression of an attribute
	 *
	 * @param argin The attribute name
	 * @return The expression
	 * @throws DevFailed
	 */
	@Command(name = "GetExpression", inTypeDesc = "The attribute name", outTypeDesc = "The expression")
	public String[] getExpression(final String argin) throws DevFailed {
		xlogger.entry(argin);
		String[] argout = null;
		final IAttributeBehavior dynamicAttrParser = dynMngt.getAttribute(argin);
		if (dynamicAttrParser instanceof DynamicAttributeParser) {
			argout = ((DynamicAttributeParser) dynamicAttrParser).getExpression();
		} else {
			throw DevFailedUtils.newDevFailed("argin error", argin + " does not exist");
		}
		xlogger.exit();
		return argout;
	}

	/**
	 * Get state
	 *
	 * @return
	 * @throws DevFailed
	 */
	public DeviceState getState() {
		xlogger.entry();
		if (CACHE_REFRESHER != null && !CACHE_REFRESHER.getLastError().isEmpty() || initFailed) {
			state = DeviceState.FAULT;
		} else if (stateResolver != null) {
			final DevState ds = stateResolver.getState();
			if (ds != null) {
				state = DeviceState.getDeviceState(ds);
			}
		}
		return state;
	}

	/**
	 * Get status
	 *
	 * @return
	 */
	public String getStatus() {
		final StringBuilder sb = new StringBuilder();
		if (initFailed) {
			sb.append("ERROR creating attributes");
			if (!initFailedAttributes.isEmpty()) {
				sb.append(":\n");
				initFailedAttributes.forEach(attr -> sb.append("\t" + attr + "\n"));
			} else {
				sb.append(". ");
			}
			sb.append("Check 'log' attribute for more details\n");
		} else if (CACHE_REFRESHER != null && !CACHE_REFRESHER.getLastError().isEmpty()) {
			sb.append("ERROR retrieving reading attributes, check 'LastCacheError' for more details\n");
		} else if (stateResolver != null) {
			final String[] st = stateResolver.getDeviceStateArray();
			sb.append("At least one device is in ");
			sb.append(DeviceState.getDeviceState(stateResolver.getState()));
			sb.append(" state:\n");
			for (final String element : st) {
				sb.append(element);
				sb.append("\n");
			}
		}
		if (initAttributesRemaining.size() > 0) {
			sb.append("Remaining attributes to connect:\n");
			initAttributesRemaining.forEach(attrName -> sb.append("\t" + attrName + "\n"));
		}
		if (!initErrorText.isEmpty()) {
			if (sb.length() > 0) {
				sb.append('\n');
			}
			sb.append(initErrorText);
		}
		status = sb.toString();
		return status;
	}

	/**
	 * Initialize the device.
	 *
	 * @throws MalformedObjectNameException
	 * @throws NotCompliantMBeanException
	 * @throws MBeanRegistrationException
	 * @throws InstanceAlreadyExistsException
	 */
	@Init(lazyLoading = true)
	public void initDevice() throws DevFailed, MalformedObjectNameException, InstanceAlreadyExistsException,
			MBeanRegistrationException, NotCompliantMBeanException {
		xlogger.entry();
		initFailed = false;
		initFailedAttributes.clear();
		initErrorText = "";
		initAttributesRemaining.clear();
		// add log attribute
		dynMngt.addAttribute(new LogAttribute(1000, logger, LoggerFactory.getLogger(DynamicAttributeParser.class),
				LoggerFactory.getLogger(CacheRefresher.class), LoggerFactory.getLogger(TangoSource.class)));

		initManagement();

		if (scanMode) {
			checkProperty("MovingState", false, TangoParserUtil.STATE, "tango_State\nExemple: MOVING\n", movingState);
		}

		// init dynamic attributes
		initTangoJep();

		// start refresher if not synchronous. ONLY one refresh per server (can have
		// several devices).
		logger.debug("scan mode {}", scanMode);
		if (!scanMode && !dynMngt.getDynamicAttributes().isEmpty()) {
			synchronized (TangoParser.class) {
				if (CACHE_REFRESHER == null) {
					CACHE_REFRESHER = new CacheRefresher(srcManager.build(false, "", ""), periodRefresh);
					CACHE_REFRESHER.start();
				}
			}
		}

		// retrieve device names from attribute names
		final Set<String> deviceNameList = new HashSet<String>();
		for (final String element : attribMap.keySet()) {
			final String deviceName = TangoUtil.getfullDeviceNameForAttribute(element);
			deviceNameList.add(deviceName);
		}
		// configure state refresher
		if (deviceNameList.isEmpty()) {
			state = DeviceState.ON;
			status = "no device to monitor";
		} else {
			stateResolver = new StateResolver(periodRefresh, scanMode);
			stateResolver.setMonitoredDevices(periodRefresh, deviceNameList.toArray(new String[deviceNameList.size()]));
			stateResolver.configurePriorities(priorityList);
			stateResolver.start(device.getName());
		}
		// add StopAll command
		// create a "StopAll" command
		final Set<String> stopCmdList = copyArrayAndClean(stopCommandFullNameList);
		if (!stopCmdList.isEmpty()) {
			final AsyncGroupCommand behavior = new AsyncGroupCommand(STOP_COMMAND_NAME,
					stopCmdList.toArray(new String[stopCmdList.size()]));
			dynMngt.addCommand(behavior);

		}
		// add group command on devices
		final Set<String> cmdList = copyArrayAndClean(commandNameList);
		for (final String commandName : cmdList) {
			if (!commandName.isEmpty()) {
				final GroupCommand behavior = new GroupCommand(commandName, stateResolver.getGroup());
				dynMngt.addCommand(behavior);
			}
		}

		// Parsing the autoInputProperties property.
		List<AutoInputProperties> autoInputPropertiesList = AutoInputProperties.parseProperty(autoInputProperties);
		if (!autoInputPropertiesList.isEmpty()) {
			Map<String, DynamicAttributeParser> dynAttributesMaps = new TreeMap<>();
			for (IAttributeBehavior attr : dynMngt.getDynamicAttributes()) {
				if (attr != null && attr instanceof DynamicAttributeParser) {
					final AttrWriteType attrWryteType = attr.getConfiguration().getWritable();
					if (attrWryteType.equals(AttrWriteType.READ_WRITE)) {
						DynamicAttributeParser dynAttrParser = (DynamicAttributeParser) attr;
						dynAttributesMaps.put(dynAttrParser.getName().toLowerCase(), dynAttrParser);
					}
				}
			}

			autoInputAttributeManagerList = new ArrayList<>(autoInputPropertiesList.size());
			for (AutoInputProperties autoInputProperties : autoInputPropertiesList) {
				DynamicAttributeParser dynAttrParser = dynAttributesMaps
						.get(autoInputProperties.getInputAttributeName().toLowerCase());
				if (dynAttrParser != null) {
					// Creating the thread for pooling.
					AutoInputAttributeManager autoInputAttributeManager = new AutoInputAttributeManager(dynAttrParser,
							autoInputProperties);

					// Creation of a dynamic boolean attribute for autoinput activation /
					// deactivation
					String attr_activator_name = autoInputProperties.getInputAttributeName() + autoInputActivatorTag;
					AutoInputActivatorAttribute activatorAttribute = new AutoInputActivatorAttribute(
							attr_activator_name, autoInputProperties.isActivated(), autoInputAttributeManager);
					autoInputAttributeManagerList.add(autoInputAttributeManager);
					dynMngt.addAttribute(activatorAttribute);
				} else {
					logger.error("The auto input configuration '" + autoInputProperties.getInputAttributeName()
							+ "' does not match any IONames.");
				}
			}
		}

		logger.info("init device done");

		xlogger.exit();
	}

	private final Set<String> copyArrayAndClean(final String[] array) {
		final Set<String> tmp = new HashSet<String>(Arrays.asList(array));
		tmp.removeAll(Arrays.asList("", null));
		final Set<String> result = new HashSet<String>();
		for (final String element : tmp) {
			final String trimmed = element.trim();
			if (!trimmed.isEmpty()) {
				result.add(trimmed);
			}
		}
		return result;
	}

	/**
	 * INIT WRITE EXP
	 *
	 * @throws DevFailed
	 */
	private void initInputExp() {

		String[] iNames = null;
		String[] inputExpressions = null;
		int[] iTypes = null;
		AttrDataFormat[] iFormat = null;
		// Parsing expression of our math parser
		// key attributeName, value list of expressions
		final Map<String, List<String>> attributeExpressionMap = new HashMap<String, List<String>>();
		final Map<String, AttributeConfiguration> attributeConfigMap = new HashMap<String, AttributeConfiguration>();

		if (inputNames != null && inputNames.length >= 1 && !inputNames[0].equals("")) {
			iNames = new String[inputNames.length];
			iTypes = new int[inputNames.length];
			iFormat = new AttrDataFormat[inputNames.length];
			// Default type is Double
			Arrays.fill(iTypes, TangoConst.Tango_DEV_DOUBLE);
			Arrays.fill(iFormat, AttrDataFormat.SCALAR);
			inputExpressions = new String[inputNames.length];
			for (int i = 0; i < inputNames.length; i++) {
				final int comma = inputNames[i].indexOf(',');
				iNames[i] = inputNames[i].substring(0, comma);
				// try to get the type
				if (iNames[i].indexOf(' ') != -1) {
					final String[] tokens = iNames[i].split(" ");
					int index = 0;
					if (tokens.length == 3) {
						iFormat[i] = toFormat(tokens[index++]);
					}

					final int type = toType(tokens[index++].trim());
					iNames[i] = tokens[index++].trim();
					// try to treat type from String format of TangoConst
					if (type != -1) {
						iTypes[i] = type;
					}
				}
				inputExpressions[i] = inputNames[i].substring(comma + 1);
				// we have a map with all the input attributes. If attribute
				// don't exist we will create its configuration.
				// If it exists, we just had new expressions.
				if (!attributeExpressionMap.containsKey(iNames[i])) {
					// Create a list we the input expressions
					final List<String> expressionList = new ArrayList<String>();
					expressionList.add(inputExpressions[i]);
					// We put the list in the map of input attributes
					attributeExpressionMap.put(iNames[i], expressionList);
					// Create configuration : name, type, format (scalar), write
					// type (WRITE)
					final AttributeConfiguration config = new AttributeConfiguration();
					config.setName(iNames[i]);
					try {
						config.setTangoType(iTypes[i], iFormat[i]);
						config.setWritable(AttrWriteType.WRITE);
						attributeConfigMap.put(iNames[i], config);
					} catch (final DevFailed e) {
						initFailed = true;
						logger.error("attribute \"{}\" not built. {}", config.getName(), DevFailedUtils.toString(e));
					}
				} else {
					attributeExpressionMap.get(iNames[i]).add(inputExpressions[i]);
				}
			}

			// when we covered the entire list of input expressions, we can
			// create attributes.
			for (final Map.Entry<String, List<String>> entry : attributeExpressionMap.entrySet()) {
				final String expressionName = entry.getKey();
				boolean attributeNotExist = dynMngt.getAttribute(expressionName) == null;

				if (attributeNotExist) {
					try {
						final AttributeConfiguration config = attributeConfigMap.get(entry.getKey());
						final List<String> expressionW = entry.getValue();
						final String deviceName = device.getName();
						final DynamicAttributeParser attr = new DynamicAttributeParser(deviceName, config, null,
								expressionW, attribMap, variablesJep, constantsLoader.getConstants(), functions,
								srcManager.build(scanMode, device.getName(), expressionName));
						variablesJep = attr.getJepParsers();
						dynMngt.addAttribute(attr);
					} catch (final DevFailed e) {
						initFailed = true;
						logger.error("attribute \"{}\" not built. {}", entry.getKey(), DevFailedUtils.toString(e));
					}
				} else {
					initFailed = true;
					initFailedAttributes.add(expressionName);
					logger.error("attribute \"{}\" declared in InputNames already exists.", expressionName);
				}
			}
		}
	}

	private void initConstants() {
		try {
			constantsLoader.loadFromStringArray(scalarConstants);
		} catch (DevFailed e) {
			initFailed = true;
			logger.error("Loading of property ScalarConstants failed.", e);
		}

		for (String doubleArray : doubleArrayConstants) {
			String[] keyValuePair = doubleArray.split(",", 2);
			if (keyValuePair.length > 1) {
				String constantName = keyValuePair[0].trim();
				String fileName = keyValuePair[1].trim();
				try {
					constantsLoader.loadDoubleArrayFromCommentedFile(constantName, fileName);
				} catch (DevFailed e) {
					initFailed = true;
					logger.error("Loading of property DoubleArrayConstants failed on array " + constantName, e);
				}
			} else {
				initFailed = true;
				logger.error("Loading of property DoubleArrayConstants failed on line " + keyValuePair);
			}
		}

		for (Entry<String, String[]> propertyEntry : deviceProperties.entrySet()) {
			String propName = propertyEntry.getKey();
			if (propName.toLowerCase().startsWith(ARRAY_CONSTANT_PREFIX)) {
				String arrayName = propName.substring(ARRAY_CONSTANT_PREFIX.length());
				try {
					constantsLoader.loadSimpleDoubleArrayFromStringArray(arrayName, propertyEntry.getValue());
				} catch (DevFailed e) {
					initFailed = true;
					logger.error("Loading of property " + propName + " as array failed", e);
				}
			}
		}
	}

	private void initFunctions() {
		InterpolationFunctionPropertyParser interpolationFunctionPropertyParser = new InterpolationFunctionPropertyParser(
				constantsLoader.getConstants());
		for (String interpFunction : interpolationFunctions) {
			try {
				TangoParserCustomFunction interpolator = interpolationFunctionPropertyParser
						.buildInterpolator(interpFunction);
				functions.add(interpolator);
			} catch (DevFailed e) {
				initFailed = true;
				logger.error("Error while loading property InterpolationFunctions on line '" + interpFunction + "'", e);
			}
		}
	}

	/**
	 * INIT READ-WRITE EXP
	 *
	 * @throws DevFailed
	 */
	private void initIOExp() {
		// Names of READ-WRITE ATTRIBUTES
		String[] oiNames = null;
		// Expressions of READ-WRITE ATTRIBUTES
		String[] inputOutputExpression = null;
		// Output expressions of READ-WRITE ATTRIBUTES
		String[] oiExpression = null;
		// Types of READ-WRITE ATTRIBUTES
		int[] ioTypes = null;
		// Format of READ-WRITE ATTRIBUTES
		AttrDataFormat[] ioFormat = null;

		if (iONames != null && iONames.length >= 1 && !iONames[0].equals("")) {
			oiExpression = new String[iONames.length];
			oiNames = new String[iONames.length];
			ioTypes = new int[iONames.length];
			ioFormat = new AttrDataFormat[iONames.length];
			// Default type is Double
			Arrays.fill(ioTypes, TangoConst.Tango_DEV_DOUBLE);
			Arrays.fill(ioFormat, AttrDataFormat.SCALAR);
			for (int i = 0; i < iONames.length; i++) {
				// Input expressions of READ-WRITE ATTRIBUTES
				String[] ioExpression;
				final String[] elements = iONames[i].split(";");
				oiNames[i] = elements[0];
				boolean attributeNotExist = dynMngt.getAttribute(oiNames[i]) == null;

				if (attributeNotExist) {
					// try to get the type
					if (oiNames[i].indexOf(' ') != -1) {
						final String[] tokens = oiNames[i].split(" ");
						int index = 0;
						if (tokens.length == 3) {
							ioFormat[i] = toFormat(tokens[index++]);
						}

						final int type = toType(tokens[index++].trim());
						oiNames[i] = tokens[index++].trim();
						// try to treat type from String format of TangoConst
						if (type != -1) {
							ioTypes[i] = type;
						}

					}
					inputOutputExpression = new String[elements.length - 1];

					System.arraycopy(elements, 1, inputOutputExpression, 0, elements.length - 1);

					oiExpression[i] = inputOutputExpression[0];
					ioExpression = new String[inputOutputExpression.length - 1];

					System.arraycopy(inputOutputExpression, 1, ioExpression, 0, inputOutputExpression.length - 1);
					final AttributeConfiguration config = new AttributeConfiguration();
					try {
						// srt configuration : name, type and write type (here :
						// READ_WRITE)
						config.setName(oiNames[i]);
						config.setTangoType(ioTypes[i], ioFormat[i]);
						config.setWritable(AttrWriteType.READ_WRITE);
						config.setMemorized(true);
						config.setMemorizedAtInit(false);

						// Create list for Write expression and put in write
						// expression(s).
						final List<String> exprW = new ArrayList<String>();
						for (int j = 0; j < ioExpression.length; j++) {
							exprW.add(ioExpression[j]);
						}
						final String deviceName = device.getName();
						final DynamicAttributeParser attr = new DynamicAttributeParser(deviceName, config,
								oiExpression[i], exprW, attribMap, variablesJep, constantsLoader.getConstants(),
								functions, srcManager.build(scanMode, deviceName, config.getName()));
						dynMngt.addAttribute(attr);
						variablesJep = attr.getJepParsers();
					} catch (final DevFailed e) {
						initFailed = true;
						logger.error("attribute \"{}\" not built. {}", config.getName(), DevFailedUtils.toString(e));
					}
				} else {
					initFailed = true;
					initFailedAttributes.add(iONames[i]);
					logger.error("attribute \"{}\" declared in IONames already exists.", iONames[i]);
				}
			}

		}
	}

	/**
	 * JMX initialization: Create and register TangoStat MBean in Platform
	 * MBeanServer. Initialize reading time.
	 *
	 * @throws NullPointerException
	 * @throws MalformedObjectNameException
	 * @throws NotCompliantMBeanException
	 * @throws MBeanRegistrationException
	 * @throws InstanceAlreadyExistsException
	 */
	private void initManagement() throws MalformedObjectNameException, InstanceAlreadyExistsException,
			MBeanRegistrationException, NotCompliantMBeanException {
		xlogger.entry();
		// Register MBean in Platform MBeanServer
		final MBeanServer platform = ManagementFactory.getPlatformMBeanServer();
		final ObjectName objectName = new ObjectName(FR_SOLEIL_MANAGEMENT_TYPE_TANGO_PARSER_STATS);
		if (platform.isRegistered(objectName)) {
			platform.registerMBean(mBeans, objectName);
		}
		xlogger.exit();
	}

	/**
	 * INIT READ PART
	 *
	 * @throws DevFailed
	 */
	private void initOutputExp() {
		AttrDataFormat[] outputFormats = null;
		int[] outputTypes = null;
		String[] outputExpressions = null;
		// Parsing expression of our math parser

		if (outputNames != null && outputNames.length >= 1 && !outputNames[0].equals("")) {
			oNames = new String[outputNames.length];
			outputTypes = new int[outputNames.length];
			// Default type is Double
			Arrays.fill(outputTypes, TangoConst.Tango_DEV_DOUBLE);
			outputFormats = new AttrDataFormat[outputNames.length];
			// Default format is Scalar
			Arrays.fill(outputFormats, AttrDataFormat.SCALAR);
			outputExpressions = new String[outputNames.length];
			for (int i = 0; i < outputNames.length; i++) {
				final int comma = outputNames[i].indexOf(',');
				oNames[i] = outputNames[i].substring(0, comma);

				boolean attributeNotExist = dynMngt.getAttribute(oNames[i]) == null;

				if (attributeNotExist) {
					// try to get the type
					if (oNames[i].indexOf(' ') != -1) {
						final String[] tokens = oNames[i].split(" ");
						int index = 0;
						// if we have 3 tokens, it's that the format was
						// precised
						// [FORMAT] [TYPE] NAME
						if (tokens.length == 3) {
							outputFormats[i] = toFormat(tokens[index++]);
						}

						// we get the type
						final int type = toType(tokens[index++]);

						oNames[i] = tokens[index];
						// try to treat type from String format of TangoConst
						if (type != -1) {
							outputTypes[i] = type;
						}
					}
					outputExpressions[i] = outputNames[i].substring(comma + 1, outputNames[i].length());
					final AttributeConfiguration config = new AttributeConfiguration();
					try {
						// set configuration : name, type, format and write type
						// (here : READ)
						config.setName(oNames[i]);
						config.setTangoType(outputTypes[i], outputFormats[i]);
						config.setWritable(AttrWriteType.READ);
						final String deviceName = device.getName();
						final DynamicAttributeParser attr = new DynamicAttributeParser(deviceName, config,
								outputExpressions[i], null, attribMap, variablesJep, constantsLoader.getConstants(),
								functions, srcManager.build(scanMode, device.getName(), oNames[i]));
						variablesJep = attr.getJepParsers();
						dynMngt.addAttribute(attr);
					} catch (final DevFailed e) {
						initFailed = true;
						logger.error("attribute \"{}\" not built. {}", config.getName(), DevFailedUtils.toString(e));
					}
				} else {
					initFailed = true;
					initFailedAttributes.add(oNames[i]);
					logger.error("attribute \"{}\" declared in OutputNames already exists.", oNames[i]);
				}
			}
		}

	}

	/**
	 * This method parse Input,Output and Input-Output properties and create
	 * attributes.
	 *
	 * @throws DevFailed
	 */
	private void initTangoJep() {
		xlogger.entry();
		logger.info("init jep started");

		if (useDiagnosticFunctions) {
			logger.debug("diagnose function ON");
			// prepare args
			// Only scalar number attributes are accepted
			final StringBuffer args = new StringBuffer();
			for (final Entry<String, String> entry : attribMap.entrySet()) {
				final String variableName = entry.getValue();
				final String attributeName = entry.getKey();
				// Check if the attribute are a scalar number
				try {
					final TangoAttribute ta = new TangoAttribute(attributeName);
					if (ta.isNumber() && ta.isScalar()) {
						args.append(variableName);
						args.append(",");
					}
				} catch (final DevFailed e) {
					initFailed = true;
					logger.error("attribute \"{}\" not built. {}", variableName, DevFailedUtils.toString(e));
				}

			}
			// Trim outputNames
			if (!args.toString().isEmpty()) {
				String arguments = args.toString();
				if (arguments.endsWith(",")) {
					arguments = arguments.substring(0, arguments.length() - 1);
				}
				logger.debug("arguments = {}", arguments);
				final List<String> expressionList = new ArrayList<String>(Arrays.asList(outputNames));
				expressionList.removeAll(Arrays.asList(new String[] { "" }));

				expressionList.add("mean,mean(" + arguments + ")");
				expressionList.add("min,min(" + arguments + ")");
				expressionList.add("max,max(" + arguments + ")");
				expressionList.add("std,std(" + arguments + ")");
				// Add diagnostic functions in outputExpression. They are just read attribute.
				outputNames = expressionList.toArray(outputNames);
			}
		}

		initConstants();
		initFunctions();
		initIOExp();
		initInputExp();
		initOutputExp();

		attribMap.keySet().forEach(attrName -> initAttributesRemaining.add(attrName.toLowerCase()));

		DynamicAttributesInitializer dynamicAttributesInitializer = new DynamicAttributesInitializer();
		try {
			if (autoInitEnabled) {
				SimpleCircuitBreaker circuitBreaker = new SimpleCircuitBreaker(dynamicAttributesInitializer);
				circuitBreaker.execute();
			} else {
				dynamicAttributesInitializer.execute();
			}
		} catch (final DevFailed e) {
			initFailed = true;
			e.printStackTrace();
			final StringBuilder sb = new StringBuilder();
			sb.append("INIT FAILED\n").append(DevFailedUtils.toString(e));
			initErrorText = sb.toString();
			logger.error("{}", initErrorText);
		}

		boolean consistency = checkConsistency();
		if (!consistency) {
			initFailed = true;
			logger.error("There is at least one cyclic reference between expressions.");
		}

		logger.debug("init JEP done");

		xlogger.exit();
	}

	private class DynamicAttributesInitializer implements CircuitBreakerCommand {

		@Override
		public void execute() throws DevFailed {
			for (IAttributeBehavior attribute : TangoParser.this.dynMngt.getDynamicAttributes()) {
				boolean attrInitFailed = false;
				if (attribute instanceof DynamicAttributeParser) {
					DynamicAttributeParser dynAttrParser = (DynamicAttributeParser) attribute;
					try {
						dynAttrParser.initExpression();
					} catch (final DevFailed e) {
						boolean parseError = e.getCause() != null && (e.getCause() instanceof ParseException);
						if (!parseError && e.errors != null & e.errors.length > 0) {
							for (DevError error : e.errors) {
								parseError = error.desc != null
										&& error.desc.equals(ParseException.class.getCanonicalName());
								if (parseError) {
									break;
								}
							}
						}
						if (parseError) {
							// If it's a parsing exception, it's absorbed. It's useless to retry.
							initFailed = true;
							attrInitFailed = true;
							e.printStackTrace();
							logger.error("attribute \"{}\" not built. {}", dynAttrParser.getName(),
									DevFailedUtils.toString(e));
						} else {
							if (autoInitEnabled) {
								throw e;
							} else {
								initFailed = true;
								attrInitFailed = true;
								e.printStackTrace();
								String localInitError = getInitError(e);
								if (!initErrorText.contains(localInitError)) {
									initErrorText += localInitError;
								}
								logger.error("attribute \"{}\" not built. {}", dynAttrParser.getName(),
										DevFailedUtils.toString(e));
							}
						}
					} catch (ParseException e) {
						initFailed = true;
						attrInitFailed = true;
						e.printStackTrace();
						initErrorText += e.getMessage();
						logger.error("attribute \"{}\" not built. {}", dynAttrParser.getName(), e.getMessage());
					}
					if (!attrInitFailed) {
						Set<String> validatedAttributes = new TreeSet<>();
						dynAttrParser.getJepParsersWrite().forEach(jepParser -> {
							jepParser.getAttributes().keySet()
									.forEach(attr -> validatedAttributes.add(attr.toLowerCase()));
						});
						if (dynAttrParser.getJepParserRead() != null) {
							dynAttrParser.getJepParserRead().getAttributes().keySet()
									.forEach(attr -> validatedAttributes.add(attr.toLowerCase()));
						}
						initAttributesRemaining.removeAll(validatedAttributes);
					}
				}
			}
			if (!initFailed) {
				initErrorText = "";
				initAttributesRemaining.clear();
			}
			logger.info("init of dynamic attributes done.");
		}

		@Override
		public void getFallback() throws DevFailed {
			// Nothing to do, just a log.
			logger.info("init failure fallback");
		}

		@Override
		public void notifyError(DevFailed e) {
			// Logging the error.
			String localInitError = getInitError(e);
			if (!initErrorText.contains(localInitError)) {
				initErrorText += localInitError;
			}
			logger.error("{}", initErrorText);
		}

		private String getInitError(DevFailed e) {
			final StringBuilder sb = new StringBuilder();
			sb.append("INIT FAILED, will retry in a while\n").append(DevFailedUtils.toString(e));
			return sb.toString();
		}

	}

	/**
	 * Consistency check for read expressions. It's Searching for cyclic references.
	 * 
	 * @return <code>true</code> if the expressions are consistent.
	 */
	private boolean checkConsistency() {
		boolean consistency = true;
		List<AJepParser> parserReadList = new ArrayList<>();
		for (AJepParser jepParser : variablesJep) {
			if (jepParser instanceof JepParserRead) {
				parserReadList.add(jepParser);
			}
		}

		for (AJepParser jepParser : parserReadList) {

			Queue<AJepParser> dependanciesQueue = new LinkedList<AJepParser>();
			dependanciesQueue.add(jepParser);

			while (!dependanciesQueue.isEmpty() && consistency) {
				AJepParser dependancy = dependanciesQueue.remove();

				for (AJepParser nextDependancy : dependancy.getDependantJepParserList()) {
					if (!nextDependancy.getName().equals(jepParser.getName())
							&& !nextDependancy.getName().equals(dependancy.getName())) {
						dependanciesQueue.add(nextDependancy);
					} else {
						logger.error("There is a cyclic reference for expression " + jepParser.getName());
						consistency = false;
						break;
					}
				}
			}
			if (!consistency) {
				break;
			}
		}
		return consistency;
	}

	/**
	 * This methode remove EmptyLine(s) of propertyValue
	 *
	 * @param propertyName  name of the property
	 * @param propertyValue property value
	 * @return property value without empty line(s)
	 */
	private String[] removeEmptyLineFromProperty(final String propertyName, final String[] propertyValue) {
		xlogger.entry();
		// remove Empty lines
		final String[] temp = TangoParserUtil.removeEmptyLines(propertyValue);

		// get log
		if (propertyValue.length != temp.length) {
			logger.debug(propertyName + " : " + (propertyValue.length - temp.length) + " lines removed");
		} else {
			logger.debug(propertyName + " no lines removed");
		}
		xlogger.exit();
		return temp;
	}

	/**
	 *
	 * @param argin
	 * @throws DevFailed
	 */
	@Command(name = "SetExpression")
	public void setExpression(final String[] argin) throws DevFailed {
		xlogger.entry();
		final String name = argin[0];
		if (dynMngt.getAttribute(name) == null) {
			throw DevFailedUtils.newDevFailed("Unknown attribute", "The attribute \"" + name + "\" does not exist.");
		} else if (argin.length < 2 || argin[1].trim().equals("")) {
			throw DevFailedUtils.newDevFailed("Incorrect syntax",
					"You must precise the attribute name and at least one expression. For example \"A\",\"expr1\",\"expr2\"(...)");
		}

		final AttributeConfiguration config = dynMngt.getAttribute(name).getConfiguration();
		// associate an attribute with a variable
		final String deviceName = device.getName();
		DynamicAttributeParser attribute = null;
		if (config.getWritable().equals(AttrWriteType.READ)) {
			final String expression = argin[1];
			attribute = new DynamicAttributeParser(deviceName, config, expression, null, attribMap,
					constantsLoader.getConstants(), functions, srcManager.build(scanMode, device.getName(), name));
		} else if (config.getWritable().equals(AttrWriteType.WRITE)) {
			final List<String> expressionList = new ArrayList<String>();
			for (int i = 1; i < argin.length; i++) {
				expressionList.add(argin[i]);
			}
			attribute = new DynamicAttributeParser(deviceName, config, null, expressionList, attribMap,
					constantsLoader.getConstants(), functions, srcManager.build(scanMode, device.getName(), name));
		} else if (config.getWritable().equals(AttrWriteType.READ_WRITE)) {
			final String exprR = argin[1];

			final List<String> expressionW = new ArrayList<String>();
			for (int i = 2; i < argin.length; i++) {
				expressionW.add(argin[i]);
			}
			attribute = new DynamicAttributeParser(deviceName, config, exprR, expressionW, attribMap,
					constantsLoader.getConstants(), functions, srcManager.build(scanMode, device.getName(), name));
		}
		if (attribute != null) {
			try {
				attribute.initExpression();
			} catch (ParseException e) {
				logger.error("attribute \"{}\" not built. {}", name, e.getMessage());
				e.printStackTrace();
				throw DevFailedUtils.newDevFailed(e);
			}
			dynMngt.removeAttribute(name);
			dynMngt.addAttribute(attribute);
		}
		xlogger.exit();
	}

	public void setState(final DeviceState state) {
		this.state = state;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public void setDynMngt(final DynamicManager dynMngt) {
		this.dynMngt = dynMngt;
	}

	public void setAttributeNames(final String[] attributeNames) throws DevFailed {
		// remove empty lines
		this.attributeNames = removeEmptyLineFromProperty("AttributeNames", attributeNames);
		// remove white spaces
		for (int i = 0; i < attributeNames.length; i++) {
			this.attributeNames[i] = attributeNames[i].replace(" ", "");
		}
		// Create Map with attributes in property AttributeNames
		for (final String propertyLine : this.attributeNames) {
			if (!propertyLine.isEmpty()) {
				final String[] line = propertyLine.split(",");
				attribMap.put(line[1], line[0]);
			}
		}
		checkProperty("AttributeNames", true, TangoParserUtil.PATTERN_ATTR, TangoParserUtil.SYNTAX_ATTRIBUTE_NAMES,
				attributeNames);
	}

	/**
	 *
	 * @param inputNames
	 * @throws DevFailed
	 */
	public void setInputNames(final String[] inputNames) throws DevFailed {
		this.inputNames = removeEmptyLineFromProperty("InputNames", inputNames);
		checkProperty("InputNames", true, TangoParserUtil.PATTERN_INPUT, TangoParserUtil.SYNTAX_INPUT_NAMES,
				inputNames);
	}

	public void setIONames(final String[] iONames) throws DevFailed {
		this.iONames = removeEmptyLineFromProperty("IONames", iONames);
		checkProperty("IONames", true, TangoParserUtil.PATTERN_IO, TangoParserUtil.SYNTAX_IO_NAMES, iONames);
	}

	public void setAutoInputProperties(final String[] autoInputProperties) throws DevFailed {
		this.autoInputProperties = removeEmptyLineFromProperty("AutoInputProperties", autoInputProperties);
		// checkProperty("AutoInputProperties", true, TangoParserUtil.PATTERN_IO,
		// TangoParserUtil.SYNTAX_IO_NAMES, autoInputProperties);
	}

	public void setOutputNames(final String[] outputNames) throws DevFailed {
		this.outputNames = removeEmptyLineFromProperty("OutputNames", outputNames);
		checkProperty("OutputNames", true, TangoParserUtil.PATTERN_OUTPUT, TangoParserUtil.SYNTAX_OUTPUT_NAMES,
				outputNames);
	}

	public void setUseDiagnosticFunctions(final boolean useDiagnosticFunctions) {
		this.useDiagnosticFunctions = useDiagnosticFunctions;
	}

	public void setMovingState(final String movingState) {
		this.movingState = movingState;
	}

	public void setPeriodRefresh(final int periodRefresh) {
		this.periodRefresh = periodRefresh;
	}

	public void setPriorityList(final String[] priorityList) throws DevFailed {
		this.priorityList = Arrays.copyOf(priorityList, priorityList.length);
		checkProperty("PriorityList", true, TangoParserUtil.PATTERN_PRIORITY, TangoParserUtil.SYNTAX_PRIORITY_LIST,
				priorityList);
	}

	public Map<String, String[]> getDeviceProperties() {
		return deviceProperties;
	}

	public void setDeviceProperties(Map<String, String[]> deviceProperties) {
		this.deviceProperties = deviceProperties;
	}

	public void setStopCommandFullNameList(final String[] stopCommandFullNameList) {
		this.stopCommandFullNameList = Arrays.copyOf(stopCommandFullNameList, stopCommandFullNameList.length);
	}

	public void setScanMode(final boolean scanMode) {
		this.scanMode = scanMode;
	}

	public void setAutoInitEnabled(boolean autoInitEnabled) {
		this.autoInitEnabled = autoInitEnabled;
	}

	public void setDevice(final DeviceManager device) {
		this.device = device;
	}

	public static String getVersion() {
		return version;
	}

	public void setScalarConstants(String[] scalarConstants) {
		this.scalarConstants = scalarConstants;
	}

	public void setDoubleArrayConstants(String[] doubleArrayConstants) {
		this.doubleArrayConstants = doubleArrayConstants;
	}

	public static void setSrcManager(final SourceManager srcManager) {
		TangoParser.srcManager = srcManager;
	}

	public void setCommandNameList(final String[] commandNameList) {
		this.commandNameList = Arrays.copyOf(commandNameList, commandNameList.length);
	}

	public String[] getInterpolationFunctions() {
		return interpolationFunctions;
	}

	public void setInterpolationFunctions(String[] interpolationFunctions) {
		this.interpolationFunctions = interpolationFunctions;
	}

	/**
	 * MAIN
	 */
	public static void main(final String[] args) {
		final ResourceBundle rb = ResourceBundle.getBundle("fr.soleil.application");
		version = rb.getString("project.version");
		ServerManager.getInstance().start(args, TangoParser.class);
	}

	/**
	 * No result = -1
	 *
	 * @param string
	 * @return
	 */
	private static int toType(final String string) {
		for (int i = 0; i < TangoConst.Tango_CmdArgTypeName.length; i++) {
			if (string.equalsIgnoreCase(TangoConst.Tango_CmdArgTypeName[i])) {
				return i;
			}
		}
		return -1;
	}

	private static AttrDataFormat toFormat(final String string) {
		if (string.equalsIgnoreCase("SCALAR")) {
			return AttrDataFormat.SCALAR;
		} else if (string.equalsIgnoreCase("SPECTRUM")) {
			return AttrDataFormat.SPECTRUM;
		} else if (string.equalsIgnoreCase("IMAGE")) {
			return AttrDataFormat.IMAGE;
		} else {
			return AttrDataFormat.IMAGE;
			// DevFailedUtils.throwDevFailed("DevFailed",
			// "initTangoJep -> Image format not supported yet.");
		}
	}
}