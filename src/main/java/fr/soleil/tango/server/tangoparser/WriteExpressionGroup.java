package fr.soleil.tango.server.tangoparser;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.JepParserWrite;
import fr.soleil.tango.parser.datasource.TangoSource;

/**
 * Manage a group of writers
 *
 * @author ABEILLE
 *
 */
public class WriteExpressionGroup {
    private final Logger logger = LoggerFactory.getLogger(WriteExpressionGroup.class);
    private final List<JepParserWrite> jepParserWriteList;
    private final TangoSource sourceGroup;
    private final String expressionName;

    /**
     * Create a group of write expressions
     *
     * @param string
     *
     * @param jepParserWriteList
     * @param sourceGroup
     * @throws DevFailed
     */
    public WriteExpressionGroup(final String expressionName, final List<JepParserWrite> jepParserWriteList,
            final TangoSource sourceGroup) throws DevFailed {
        this.expressionName = expressionName;
        this.sourceGroup = sourceGroup;
        this.jepParserWriteList = new ArrayList<JepParserWrite>(jepParserWriteList);
        final List<String> attributeNames = new ArrayList<String>();
        for (final JepParserWrite writer : jepParserWriteList) {
            if (writer.isAttribute()) {
                attributeNames.add(writer.getAttributeName());
            }
        }
        if (!attributeNames.isEmpty()) {
            this.sourceGroup.addWriteSource(expressionName, attributeNames.toArray(new String[attributeNames.size()]));
        }
    }

    /**
     * set a value on a group of write expressions and write to the tango attributes
     *
     * @param value
     * @throws DevFailed
     */
    public void setValue(final Object value, final boolean lock) throws DevFailed {
        for (final JepParserWrite writer : jepParserWriteList) {
            writer.refresh(lock);
        }
        final List<Object> values = new ArrayList<Object>();
        for (final JepParserWrite writer : jepParserWriteList) {
            logger.debug("setting {}", writer);
            writer.setValue(value);
            if (writer.isAttribute()) {
                final Object result = writer.getValue(lock);
                logger.debug("write result {} to {}", result, writer.getAttributeName());
                values.add(result);
            }
        }
        sourceGroup.write(expressionName, values.toArray(new Object[values.size()]));
    }

    /**
     * Get all write expressions
     *
     * @return
     */
    public String[] getExpressions() {
        final String[] expressions = new String[jepParserWriteList.size()];
        final int i = 0;
        for (final JepParserWrite writer : jepParserWriteList) {
            expressions[i] = writer.getExpression();
        }
        return expressions;
    }
}
