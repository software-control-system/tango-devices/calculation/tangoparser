/**
 *
 */
package fr.soleil.tango.server.tangoparser;

import java.util.ArrayList;

/**
 * @author GRAMER
 * 
 */
public final class TangoParserUtil {

    private static final String VARIABLE = "([^,/ \t]+)";
    /**
     * pattern for AttributeNames
     */
    public static final String PATTERN_ATTR = VARIABLE + " *, *" + VARIABLE + "/" + VARIABLE + "/" + VARIABLE + "/"
	    + VARIABLE;

    private static final String ALL_EXCEPT_EMPTY = "((\\s)*(\\S)+(\\s)*)+";
    private static final String TYPE = "((?i)DevBoolean |DevShort |DevLong |DevFloat |DevDouble |DevUShort |DevULong |DevString |State |DevUChar |DevLong64 |DevULong64 )?";
    private static final String FORMAT = "(SPECTRUM |SCALAR |IMAGE )?";
    /**
     * pattern for OutputNames
     */
    public static final String PATTERN_OUTPUT = FORMAT + TYPE + VARIABLE + "," + ALL_EXCEPT_EMPTY;

    /**
     * pattern for InputNames
     */
    public static final String PATTERN_INPUT = FORMAT + TYPE + VARIABLE + ", *" + VARIABLE + " *= *" + ALL_EXCEPT_EMPTY;

    /**
     * pattern for IONames
     */
    public static final String PATTERN_IO = FORMAT + TYPE + VARIABLE + "\\s*;.*;\\s*" + VARIABLE + ".*=" + ALL_EXCEPT_EMPTY;

    public static final String STATE = "((?i)ON|OFF|CLOSE|OPEN|INSERT|EXTRACT|MOVING|STANDBY|FAULT|INIT|RUNNING|ALARM|DISABLE|UNKNOWN)";
    /**
     * pattern for InputNames
     */
    public static final String PATTERN_PRIORITY = STATE + "," + "[0-9]+";

    public static final String SYNTAX_ATTRIBUTE_NAMES = "variable_name,device_name/attribute\nExemple: vx,test/TangoTest/1/double_scalar\n";
    public static final String SYNTAX_INPUT_NAMES = "[type] variable_name,expression\nBy default type = DevDouble\nExemple: DevDouble X,vx=X/2";
    public static final String SYNTAX_OUTPUT_NAMES = "[format] [type] variable_name,expression\nBy default format= SCALAR and type= DevDouble\nExemple: SCALAR DevDouble Vx,vx*2\n";
    public static final String SYNTAX_PRIORITY_LIST = "state,priority\nPriority is a positive integer. More the priority is hight more the state is important.\nExemple: ON,10\n";
    public static final String SYNTAX_IO_NAMES = "[type] variable_name;expressionRead;expressionW ...\nOnly one expression for Read, and so much expression for Write";

    // One day, if TangoParser accept device alias in attributeNames use this
    // pattern
    // private static final String alias = variable + "," + variable + "/" +
    // variable;
    // private static final String pattern = variable + " *, *" + variable + "/"
    // + variable + "/" + variable + "/" + variable;
    // public static final String patternAlias = pattern + "|" + alias;

    // is a utility class that must not be instantiated.
    private TangoParserUtil() {

    }

    /**
     * 
     * @param propertyValue
     * @return propertyValue without empty lines.
     */
    public static String[] removeEmptyLines(final String[] propertyValue) {

	String[] propWithoutEmptyLines;

	final ArrayList<String> buffer = new ArrayList<String>();// buffer to
	// store non
	// empty
	// lines of
	// propertyValue.
	final int propertySize = propertyValue.length;

	for (int i = 0; i < propertySize; i++) {
	    if (!propertyValue[i].trim().isEmpty()) {
		buffer.add(propertyValue[i]);
	    }
	}

	final int bufferSize = buffer.size();

	// if propertyValue contains only empty lines.
	if (bufferSize == 0) {
	    propWithoutEmptyLines = new String[] { "" };
	} else {
	    // if propertySize == bufferSize ==> propertyValue is already
	    // correct.
	    propWithoutEmptyLines = propertySize == bufferSize ? propertyValue : buffer.toArray(new String[bufferSize]);
	}

	return propWithoutEmptyLines;

    }
}
