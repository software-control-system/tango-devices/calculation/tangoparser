package fr.soleil.tango.parser.datasource;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;

public class AttributeResults {
    private final Logger logger = LoggerFactory.getLogger(TangoSource.class);
    private final Table<String, String, AttributeResult> table = HashBasedTable.create();
    private final Map<String, AttrDataFormat> formats = new TreeMap<String, AttrDataFormat>();

    void addAttributeResult(final AttributeResult attributeResult) {
        logger.debug("adding source {} = {}", attributeResult.getSourceName(), attributeResult);
        table.put(attributeResult.getSourceName(), attributeResult.getVariable(), attributeResult);
        formats.put(attributeResult.getName(), attributeResult.getFormat());
    }

    boolean contains(final String attributeName) {
        return formats.containsKey(attributeName);
    }

    String[] getAllAttributes() {
        final Set<String> attributeList = formats.keySet();
        return attributeList.toArray(new String[attributeList.size()]);
    }

    AttrDataFormat getFormat(final String attributeName) {
        return formats.get(attributeName);
    }

    AttributeResult getAttributeResult(final String sourceName, final String variableName) {
        return table.get(sourceName, variableName);
    }

    void setAttributeResult(final String attributeName, final Object value) {
        final Map<String, Map<String, AttributeResult>> columnMap = table.columnMap();
        for (final Map<String, AttributeResult> line : columnMap.values()) {
            for (final Map.Entry<String, AttributeResult> entry : line.entrySet()) {
                final AttributeResult result = entry.getValue();
                if (attributeName.equalsIgnoreCase(result.getName())) {
                    result.setValue(value);
                    logger.debug("set value of {}", attributeName);
                }
            }
        }
    }

    void resetAttributeResultError(final String attributeName) {
        final Map<String, Map<String, AttributeResult>> columnMap = table.columnMap();
        for (final Map<String, AttributeResult> line : columnMap.values()) {
            for (final Map.Entry<String, AttributeResult> entry : line.entrySet()) {
                final AttributeResult result = entry.getValue();
                if (attributeName.equalsIgnoreCase(result.getName())) {
                    result.setError(null);
                }
            }
        }
    }

    void setAttributeResultError(final String attributeName, final DevFailed error) {
        final Map<String, Map<String, AttributeResult>> columnMap = table.columnMap();
        for (final Map<String, AttributeResult> line : columnMap.values()) {
            for (final Map.Entry<String, AttributeResult> entry : line.entrySet()) {
                final AttributeResult result = entry.getValue();
                if (attributeName.equalsIgnoreCase(result.getName())) {
                    result.setError(error);
                    logger.debug("set error of {}", attributeName);
                }
            }
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public void clear() {
        table.clear();
        formats.clear();
    }

}
