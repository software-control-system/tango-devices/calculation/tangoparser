package fr.soleil.tango.parser.datasource;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.Matrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import fr.soleil.tango.clientapi.TangoGroupAttribute;
import fr.soleil.tango.clientapi.factory.ProxyFactory;

/**
 * A group managing read and write attributes for an expression
 *
 * @author ABEILLE
 */
public class TangoSource {
	private final Logger logger = LoggerFactory.getLogger(TangoSource.class);
	private final String deviceName;
	private final String name;
	private TangoGroupAttribute readGroup;
	private final AttributeResults attributeData = new AttributeResults();
	private final Map<String, TangoGroupAttribute> writeGroups = new TreeMap<String, TangoGroupAttribute>();
	private final boolean isSynchronous;

	TangoSource(final String deviceName, final String name, final boolean isSynchronous) {
		this.deviceName = deviceName.toLowerCase(Locale.ENGLISH);
		this.name = name.toLowerCase(Locale.ENGLISH);
		this.isSynchronous = isSynchronous;
	}

	TangoSource() {
		this.deviceName = "";
		this.name = "";
		this.isSynchronous = false;
	}

	/**
	 * @param attributeName
	 * @param variableName
	 * @return true if is an array evaluation
	 * @throws DevFailed
	 */
	public synchronized void addSource(final String sourceName, final Map<String, String> map) throws DevFailed {
		logger.debug("add read attributes {} for {}", map, sourceName);
		for (final Entry<String, String> entry : map.entrySet()) {
			final String attributeName = entry.getKey().toLowerCase(Locale.ENGLISH);
			final String variableName = entry.getValue();
			final AttrDataFormat format = ProxyFactory.getInstance().createAttributeProxy(attributeName)
					.get_info().data_format;
			attributeData.addAttributeResult(new AttributeResult(sourceName, attributeName, format, variableName));
		}
		readGroup = new TangoGroupAttribute(attributeData.getAllAttributes());
		logger.debug("read attributes {} for {} successfully added", map, sourceName);
	}

	public void addWriteSource(final String sourceName, final String... attributeNames) throws DevFailed {
		logger.debug("add write attributes {} for {}", Arrays.toString(attributeNames), sourceName);
		final TangoGroupAttribute attributeGroup = new TangoGroupAttribute(attributeNames);
		writeGroups.put(sourceName, attributeGroup);
		logger.debug("write attributes {} for {} successfully added", Arrays.toString(attributeNames), sourceName);
	}

	public void write(final String sourceName, final Object... values) throws DevFailed {
		final TangoGroupAttribute group = writeGroups.get(sourceName);
		if (group != null) {
			group.write(values);
		}
	}

	public synchronized void refresh() throws DevFailed {
		logger.debug("read attributes for {}", attributeData);
		if (!isEmpty()) {
			readGroup.readAync();
			getResults();
		}
	}

	public AttributeResult getResult(final String sourceName, final String variableName) {
		return attributeData.getAttributeResult(sourceName, variableName);
	}

	public void getResults() throws DevFailed {
		// get results for tango attributes
		final DeviceAttribute[] results = readGroup.getReadAsyncReplies();
		// final Iterator<AttributeResult> it = attributeData.values().iterator();
		final String[] attributeNames = attributeData.getAllAttributes();
		for (int i = 0; i < results.length; i++) {
			// final AttributeResult result = it.next();
			final String attributeName = attributeNames[i];
			// final String attributeName = result.getName();
			final AttrDataFormat format = attributeData.getFormat(attributeName);
			// reset last error
			attributeData.setAttributeResult(attributeName, null);
			attributeData.resetAttributeResultError(attributeName);
			if (results[i] != null) {
				try {
					final int type = results[i].getType();
					if (format.equals(AttrDataFormat.SCALAR)) {
						Object tmpReadValue = InsertExtractUtils.extractRead(results[i], AttrDataFormat.SCALAR);
						if (tmpReadValue instanceof DevState) {
							tmpReadValue = ((DevState) tmpReadValue).value();
						}
						attributeData.setAttributeResult(attributeName, tmpReadValue);
					} else if (format.equals(AttrDataFormat.SPECTRUM)) {
						if (type == TangoConst.Tango_DEV_STRING) {
							throw DevFailedUtils.newDevFailed("TYPE_ERROR", "array of strings not supported");
						} else if (type == TangoConst.Tango_DEV_BOOLEAN) {
							throw DevFailedUtils.newDevFailed("TYPE_ERROR", "array of boolean not supported");
						}
						final Object tmpReadValue = InsertExtractUtils.extractRead(results[i], AttrDataFormat.SPECTRUM);
						final MVector vect = new MVector(Array.getLength(tmpReadValue));
						for (int j = 0; j < Array.getLength(tmpReadValue); j++) {
							vect.setEle(j, Array.get(tmpReadValue, j));
						}
						attributeData.setAttributeResult(attributeName, vect);
					} else {
						if (format.equals(AttrDataFormat.FMT_UNKNOWN)) {
							throw DevFailedUtils.newDevFailed("TYPE_ERROR", "Unknown format");
						} else if (type == TangoConst.Tango_DEV_STRING) {
							throw DevFailedUtils.newDevFailed("TYPE_ERROR", "images of strings not supported");
						} else if (type == TangoConst.Tango_DEV_BOOLEAN) {
							throw DevFailedUtils.newDevFailed("TYPE_ERROR", "images of boolean not supported");
						}
						final Object tmpReadValue = InsertExtractUtils.extractRead(results[i], AttrDataFormat.IMAGE);
						int dim1 = Array.getLength(tmpReadValue);
						int dim2 = 0;
						if (dim1 > 0) {
							Object subArray = Array.get(tmpReadValue, 0);
							dim2 = Array.getLength(subArray);
						}
						Matrix matrix = (Matrix) Matrix.getInstance(dim1, dim2);
						for (int j = 0; j < dim1; j++) {
							Object subArray = Array.get(tmpReadValue, j);
							for (int k = 0; k < dim2; k++) {
								matrix.setEle(j, k, Array.get(subArray, k));
							}
						}
						attributeData.setAttributeResult(attributeName, matrix);
					}
				} catch (final DevFailed e) {
					attributeData.setAttributeResultError(attributeName, e);
				}
			} else {
				attributeData.setAttributeResultError(attributeName,
						DevFailedUtils.newDevFailed("Device down for " + attributeName));
			}
		}
	}

	public boolean isEmpty() {
		return readGroup == null;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public String getName() {
		return name;
	}

	public void clear() {
		readGroup = null;
		// attributeNameList.clear();
		attributeData.clear();
	}

	@Override
	public String toString() {
		return "SourceGroup [deviceName=" + deviceName + ", name=" + name + "]";
	}

	public boolean isSynchronous() {
		return isSynchronous;
	}

}
