package fr.soleil.tango.parser.datasource;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AttributeResult {
    private final String name;
    private final String sourceName;
    private final String variable;
    private final AttrDataFormat format;
    private Object value;
    private DevFailed error;

    public AttributeResult(final String sourceName, final String name, final AttrDataFormat format,
            final String variable) {
        super();
        this.sourceName = sourceName;
        this.name = name;
        this.format = format;
        this.variable = variable;
    }

    public String getName() {
        return name;
    }

    public AttrDataFormat getFormat() {
        return format;
    }

    public Object getValue() {
        return value;
    }

    public String getVariable() {
        return variable;
    }

    public void setValue(final Object value) {
        this.value = value;
    }

    public DevFailed getError() {
        return error;
    }

    public void setError(final DevFailed error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public String getSourceName() {
        return sourceName;
    }
}
