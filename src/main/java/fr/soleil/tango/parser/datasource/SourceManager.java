package fr.soleil.tango.parser.datasource;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SourceManager {

    private final Logger logger = LoggerFactory.getLogger(SourceManager.class);
    /**
     * list of groups. One group per expression. Used for synchronous update
     */
    private final List<TangoSource> synchronousGroups = new ArrayList<TangoSource>();
    /**
     * A group containing all attributes of a server (shared for all devices)
     * Use to perform asynchronous update.
     */
    private final TangoSource asynchronousGroup = new TangoSource();

    public TangoSource build(final boolean isSynchronous, final String deviceName, final String expressionName) {
        logger.debug("build new source for {}/{}", deviceName, expressionName);
        if (isSynchronous) {
            return getSynchronousGroup(deviceName, expressionName);
        } else {
            return getAsynchronousGroup();
        }
    }

    private TangoSource getAsynchronousGroup() {
        return asynchronousGroup;
    }

    private TangoSource getSynchronousGroup(final String deviceName, final String expressionName) {
        TangoSource group = null;
        boolean found = false;
        for (final TangoSource groupResult : synchronousGroups) {
            if (groupResult.getDeviceName().equals(deviceName) && groupResult.getName().equals(expressionName)) {
                group = groupResult;
                found = true;
                break;
            }
        }
        if (!found) {
            group = new TangoSource(deviceName, expressionName, true);
            synchronousGroups.add(group);
        }
        logger.debug("build group {}", group);
        return group;
    }

    public void clear() {
        synchronousGroups.clear();
        // TODO clear only deleted attributes asynchronousGroup.clear();
    }

}
