package fr.soleil.tango.parser.datasource;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;

/**
 * Update tango attributes values' cache
 *
 * @author ABEILLE
 *
 */
public class CacheRefresher {

    private final int refreshRate;

    private final Logger logger = LoggerFactory.getLogger(CacheRefresher.class);

    private final RefreshRunner refreshRunner;

    /**
     * A runnable to get tango attributes values
     *
     * @author ABEILLE
     *
     */
    private class RefreshRunner implements Runnable {

        private final TangoSource source;
        private volatile String lastError = "";

        public RefreshRunner(final TangoSource source) {
            this.source = source;
        }

        public String getLastError() {
            return lastError;
        }

        @Override
        public void run() {
            // read tango attributes
            try {
                source.refresh();
                lastError = "";
            } catch (final DevFailed e) {
                lastError = new Date(System.currentTimeMillis()) + " - " + DevFailedUtils.toString(e);
                logger.error("error when launchRefresh {}", DevFailedUtils.toString(e));
            } catch (final Throwable e) {
                lastError = new Date(System.currentTimeMillis()) + " - " + e.getMessage();
                logger.error("error when launchRefresh {}", e);
            }
        }
    }

    private static class CacheRefresherFactory implements ThreadFactory {
        @Override
        public Thread newThread(final Runnable r) {
            return new Thread(r, "TangoParserRefresher");
        }
    }

    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1, new CacheRefresherFactory());

    private ScheduledFuture<?> future;

    public CacheRefresher(final TangoSource source, final int refreshRate) {
        refreshRunner = new RefreshRunner(source);
        this.refreshRate = refreshRate;
    }

    /**
     * Start refreshing
     */
    public synchronized void start() {
        if (!isStarted()) {
            logger.debug("start refresher at a {} ms rate", refreshRate);
            future = executor.scheduleAtFixedRate(refreshRunner, 0L, refreshRate, TimeUnit.MILLISECONDS);
        }
    }

    public synchronized boolean isStarted() {
        return future != null;
    }

    public synchronized void stop() {
        if (future != null) {
            future.cancel(true);
        }
        future = null;
    }

    public String getLastError() {
        return refreshRunner.getLastError();
    }

}
