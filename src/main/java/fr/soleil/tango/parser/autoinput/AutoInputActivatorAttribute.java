package fr.soleil.tango.parser.autoinput;

import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoDs.TangoConst;

public class AutoInputActivatorAttribute implements IAttributeBehavior {
	private final AttributeValue value = new AttributeValue();
	private final AttributeConfiguration config = new AttributeConfiguration();
	private final AutoInputAttributeManager autoInputAttributeManager;

	public AutoInputActivatorAttribute(String name, boolean activated, final AutoInputAttributeManager autoInputAttributeManager) throws DevFailed {
		super();
		config.setName(name);
		config.setTangoType(TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat.SCALAR);
		config.setWritable(AttrWriteType.READ_WRITE);
		config.setDispLevel(DispLevel.OPERATOR);
		this.autoInputAttributeManager = autoInputAttributeManager;
	}

	@Override
	public AttributeConfiguration getConfiguration() throws DevFailed {
		return config;
	}

	@Override
	public AttributeValue getValue() throws DevFailed {
		this.value.setValue(autoInputAttributeManager.isRunning());
		return value;
	}

	public boolean getBooleanValue() {
		return autoInputAttributeManager.isRunning();
	}

	@Override
	public void setValue(AttributeValue value) throws DevFailed {
		boolean newValue = false;
		if (value.getValue() != null && value.getValue() instanceof Boolean) {
			newValue = ((Boolean) value.getValue()).booleanValue();
		}
		if( newValue != getBooleanValue() ) {
			if( newValue ) {
				autoInputAttributeManager.start();
			} else {
				autoInputAttributeManager.stop();
			}
		}
	}

	@Override
	public StateMachineBehavior getStateMachine() throws DevFailed {
		return null;
	}

}
