package fr.soleil.tango.parser.autoinput;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoInputProperties {
	private static final Logger logger = LoggerFactory.getLogger(AutoInputProperties.class);
	private String inputAttributeName;
	private boolean activated;
	private double poolingPeriod;
	private double precision;
	private boolean percentPrecision;
	private boolean lastRead;

	public static List<AutoInputProperties> parseProperty(String[] tangoParserProperty) {
		List<AutoInputProperties> result = new ArrayList<>();

		for (String propertyLine : tangoParserProperty) {
			propertyLine = propertyLine.trim();
			if (!propertyLine.isEmpty() && propertyLine.contains(";")) {
				String[] splittedLine = propertyLine.split(";");
				if (splittedLine.length > 2) {
					String name = splittedLine[0].trim();
					boolean error = name.isEmpty();
					if (error) {
						continue;
					}

					boolean activated = splittedLine[1].trim().toLowerCase().equals("true");
					activated |= splittedLine[1].trim().toLowerCase().equals("on");

					double pooling = 0.0;
					try {
						pooling = Double.parseDouble(splittedLine[2].trim());
					} catch (NumberFormatException ex) {
						logger.error("Bad format for polling period for attribute " + name, ex);
						ex.printStackTrace();
						error = true;
					}

					double precision = 0.0;
					boolean percentPrecision = false;
					boolean lastRead = false;
					if (splittedLine.length > 3) {
						String precisionStr = splittedLine[3].trim();
						if (precisionStr.length() > 0) {
							if (precisionStr.charAt(precisionStr.length() - 1) == '%') {
								precisionStr = precisionStr.substring(0, precisionStr.length() - 1);
								percentPrecision = true;
							}
							try {
								precision = Double.parseDouble(splittedLine[3].trim());
							} catch (NumberFormatException ex) {
								logger.error("Bad format for precision for attribute " + name, ex);
								ex.printStackTrace();
								error = true;
							}
						}
						if (splittedLine.length > 4) {
							String lastReadStr = splittedLine[4].trim();
							if (lastReadStr.length() > 0) {
								lastRead = lastReadStr.toLowerCase().equals("last_read");
							}
						}
					}

					if (!error) {
						AutoInputProperties autoInputProperties = new AutoInputProperties(name, activated, pooling,
								precision, percentPrecision, lastRead);
						result.add(autoInputProperties);
					}
				}
			}
		}

		return result;
	}

	private AutoInputProperties(String inputAttributeName, boolean activated, double poolingPeriod, double precision,
			boolean percentPrecision, boolean lastRead) {
		super();
		this.inputAttributeName = inputAttributeName;
		this.activated = activated;
		this.poolingPeriod = poolingPeriod;
		this.precision = precision;
		this.percentPrecision = percentPrecision;
		this.lastRead = lastRead;
	}

	public double getPoolingPeriod() {
		return poolingPeriod;
	}

	public double getPrecision() {
		return precision;
	}

	public String getInputAttributeName() {
		return inputAttributeName;
	}

	public boolean isActivated() {
		return activated;
	}

	public boolean isPercentPrecision() {
		return percentPrecision;
	}

	public boolean isLastRead() {
		return lastRead;
	}

}
