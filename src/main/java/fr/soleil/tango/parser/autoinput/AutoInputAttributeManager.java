package fr.soleil.tango.parser.autoinput;

import java.lang.reflect.Array;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.attribute.AttributeValue;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.tangoparser.IWriteLockAttributeBehaviour;

public class AutoInputAttributeManager {
	private static final Logger logger = LoggerFactory.getLogger(AutoInputAttributeManager.class);

	private class TaskRunner implements Runnable {

		private Object prevValueRead = null;
		private Object prevValueWrite = null;
		private boolean numeric = false;
		private boolean array = false;

		@Override
		public void run() {
			Object newValue = readValue();

			Object prevValue = properties.isLastRead() ? prevValueRead : prevValueWrite;

			if (prevValue != null && newValue != null) {
				boolean equal = false;
				if (array) {
					int length = Array.getLength(prevValue);
					equal = length == Array.getLength(newValue);
					for (int i = 0; i < length && equal; i++) {
						Object newValueI = Array.get(newValue, i);
						Object prevValueI = Array.get(prevValue, i);
						equal = compareScalarValues(prevValueI, newValueI, numeric);
					}
				} else {
					equal = compareScalarValues(prevValue, newValue, numeric);
				}
				if (!equal) {
					writeValue(newValue);
					prevValueWrite = newValue;
				}
			} else if (newValue != null) {
				// First value at start or after a shutdown.
				array = newValue.getClass().isArray();
				if (array) {
					if (Array.getLength(newValue) > 0) {
						numeric = Array.get(newValue, 0) instanceof Number;
					} else {
						newValue = null;
					}
				} else {
					numeric = newValue instanceof Number;
				}
				if (newValue != null) {
					writeValue(newValue);
					prevValueWrite = newValue;
				}
			}

			prevValueRead = newValue;
		}
	}

	private ScheduledThreadPoolExecutor executor;
	private TaskRunner taskRunner;

	private final IWriteLockAttributeBehaviour attribute;
	private final AutoInputProperties properties;

	public AutoInputAttributeManager(final IWriteLockAttributeBehaviour attribute,
			final AutoInputProperties properties) {
		super();
		this.attribute = attribute;
		this.properties = properties;

		this.executor = null;
		this.taskRunner = null;

		if (this.properties.isActivated()) {
			start();
		}
	}

	private boolean compareScalarValues(Object prevValue, Object newValue, boolean numeric) {
		boolean equal = false;
		if (numeric) {
			double newValDbl = ((Number) newValue).doubleValue();
			double prevValDbl = ((Number) prevValue).doubleValue();
			double diff = newValDbl - prevValDbl;
			if (properties.isPercentPrecision()) {
				double pctDiff = Math.abs(diff / prevValDbl)*100.0d;
				equal = pctDiff < properties.getPrecision();
			} else {
				double absDiff = Math.abs(diff);
				equal = absDiff < properties.getPrecision();
			}
		} else {
			// For boolean or strings, precision is useless.
			equal = prevValue.equals(newValue);
		}
		return equal;
	}

	private Object readValue() {
		Object value = null;
		AttributeValue attrValue = null;
		try {
			attrValue = attribute.getValue();
			if (attrValue != null) {
				value = attrValue.getValue();
			}
		} catch (DevFailed e) {
			logger.error("Error while reading attribute " + properties.getInputAttributeName(), e);
			e.printStackTrace();
		}

		return value;
	}

	private void writeValue(Object value) {
		try {
			logger.debug(
					"Auto writing value '" + value.toString() + "' to attribute " + properties.getInputAttributeName());
			AttributeValue attrValue = new AttributeValue(value);
			attribute.internalSetValue(attrValue);
			attribute.setWriteValue(attrValue);
		} catch (DevFailed e) {
			logger.error("Error while writting attribute " + properties.getInputAttributeName(), e);
			e.printStackTrace();
		}
	}

	public synchronized void start() {
		if (taskRunner == null) {
			attribute.blockExternalWriting();
			long periodMS = (long) (properties.getPoolingPeriod() * 1000.0 + 0.5);
			taskRunner = new TaskRunner();
			executor = new ScheduledThreadPoolExecutor(1);
			executor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
			executor.scheduleAtFixedRate(taskRunner, 0, periodMS, TimeUnit.MILLISECONDS);
		}
	}

	public synchronized boolean isRunning() {
		return executor != null;
	}

	public synchronized void stop() {
		if (executor != null) {
			attribute.releaseExternalWriting();
			executor.shutdown();
			executor = null;
			taskRunner = null;
		}
	}
}
