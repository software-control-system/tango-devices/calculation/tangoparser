package fr.soleil.tango.parser.constants;

import java.lang.reflect.Array;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.util.TypeConversionUtil;
import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

public class ParserConstant {
	private static final Transmorph transmorph = new Transmorph(new DefaultConverters());

	private String name;
	private Object value;

	public ParserConstant(String name, Object value) {
		super();
		this.name = name;
		this.value = value;
	}

	public ParserConstant(String name, String valueStr, Class<?> clazz) throws DevFailed {
		super();
		this.name = name;
		this.value = getValue(clazz, valueStr);
	}

	public String getName() {
		return name;
	}

	public Object getValue() {
		return value;
	}
	public <T> T getValue(Class<T> clazz) throws DevFailed {
		return getValue(clazz, value);
	}

	@SuppressWarnings("unchecked")
	private static <T> T getValue(Class<T> clazz, Object value) throws DevFailed {
		T result = null;
		if (value != null) {
			if (clazz.isArray()) {
				int length = Array.getLength(value);
				Class<?> subClazz = clazz.getComponentType();
				boolean isMatrix = subClazz.isArray();

				if (length > 0 && !isMatrix) {
					// Sometimes its an array of object which are arrays. The component type is just
					// "Object".
					Object val0 = Array.get(value, 0);
					isMatrix = val0.getClass().isArray();
					if (isMatrix) {
						subClazz = val0.getClass();
					}
				}
				if (isMatrix) {
					result = (T) Array.newInstance(subClazz, length);
					for (int i = 0; i < length; i++) {
						Object subArray = Array.get(value, i);
						Object translated = TypeConversionUtil.castToArray(subClazz, subArray);
						Array.set(result, i, translated);
					}

				} else {
					result = (T) TypeConversionUtil.castToArray(subClazz, value);
				}
			} else {
				try {
					result = transmorph.convert(value, clazz);
				} catch (ConverterException e) {
					throw DevFailedUtils.newDevFailed(e);
				}
			}
		}
		return result;
	}
}
