package fr.soleil.tango.parser.constants;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevEncoded;
import fr.esrf.Tango.DevFailed;

public class ConstantsLoader {
	private final Logger logger = LoggerFactory.getLogger(ConstantsLoader.class);
	private static final String commentChar = "#";

	static enum DupKeyOption {
		OVERWRITE, DISCARD
	}

	private static final Map<String, Class<?>> JAVA_TYPES_MAP = new HashMap<String, Class<?>>();
	static {
		JAVA_TYPES_MAP.put("int", int.class);
		JAVA_TYPES_MAP.put("long", long.class);
		JAVA_TYPES_MAP.put("devlong", long.class);
		JAVA_TYPES_MAP.put("double", double.class);
		JAVA_TYPES_MAP.put("devdouble", double.class);
		JAVA_TYPES_MAP.put("float", float.class);
		JAVA_TYPES_MAP.put("devfloat", float.class);
		JAVA_TYPES_MAP.put("boolean", boolean.class);
		JAVA_TYPES_MAP.put("devboolean", boolean.class);
		JAVA_TYPES_MAP.put("byte", byte.class);
		JAVA_TYPES_MAP.put("devuchar", byte.class);
		JAVA_TYPES_MAP.put("short", short.class);
		JAVA_TYPES_MAP.put("devshort", short.class);
		JAVA_TYPES_MAP.put("string", String.class);
		JAVA_TYPES_MAP.put("devstring", String.class);
		JAVA_TYPES_MAP.put("devstate", DeviceState.class);
		JAVA_TYPES_MAP.put("state", DeviceState.class);
		JAVA_TYPES_MAP.put("devstate", DeviceState.class);
		JAVA_TYPES_MAP.put("devencoded", DevEncoded.class);
	}

	private DupKeyOption dupKeyOption;

	private Map<String, ParserConstant> constants;
	private Map<String, ParserConstant> unmodifiableConstants;

	public ConstantsLoader() {
		this(DupKeyOption.OVERWRITE);
		this.dupKeyOption = DupKeyOption.OVERWRITE;
	}

	public ConstantsLoader(DupKeyOption dupKeyOption) {
		super();
		this.dupKeyOption = dupKeyOption;
		this.constants = new TreeMap<>();
		this.unmodifiableConstants = Collections.unmodifiableMap(this.constants);
	}

	public void loadFromStringArray(String[] inputStrings) throws DevFailed {
		loadFromStream(stringArrayToInputStream(inputStrings));
	}

	public void loadFromFile(Path baseDir, String inputFilename) throws DevFailed {
		InputStream inputStream = fileNameToInputStream(baseDir, inputFilename);
		loadFromStream(inputStream);
	}

	private static InputStream stringArrayToInputStream(String[] inputStrings) {
		StringBuilder strBldr = new StringBuilder();
		for (int i = 0; i < inputStrings.length; i++) {
			String string = inputStrings[i];
			if (i > 0) {
				strBldr.append('\n');
			}
			strBldr.append(string);
		}
		InputStream inputStream = new ByteArrayInputStream(strBldr.toString().getBytes());
		return inputStream;
	}

	private static InputStream fileNameToInputStream(Path baseDir, String inputFilename) throws DevFailed {
		Path filePath = baseDir.resolve(inputFilename);
		InputStream inputStream;
		try {
			inputStream = Files.newInputStream(filePath);
		} catch (IOException e) {
			throw DevFailedUtils.newDevFailed(e);
		}
		return inputStream;
	}

	private void storeConstant(ParserConstant pc) {
		if (DupKeyOption.OVERWRITE == dupKeyOption) {
			constants.put(pc.getName(), pc);
		} else if (DupKeyOption.DISCARD == dupKeyOption) {
			constants.putIfAbsent(pc.getName(), pc);
		}
	}

	public Map<String, String> loadFromStream(InputStream inputStream) throws DevFailed {
		HashMap<String, String> map = new HashMap<>();
		BufferedInputStream bis = new BufferedInputStream(inputStream);
		InputStreamReader isr = new InputStreamReader(bis);

		String line;
		try (BufferedReader reader = new BufferedReader(isr)) {
			while ((line = reader.readLine()) != null) {
				String[] keyValuePair = line.split(",", 2);
				if (keyValuePair.length > 1) {
					String key = keyValuePair[0].trim();
					String value = keyValuePair[1];
					String[] keyTypePair = key.split("[\\t\\s]+", 2);
					String name = key;
					Class<?> clazz = double.class;
					if (keyTypePair.length > 1) {
						String type = keyTypePair[0].trim().toLowerCase();
						clazz = JAVA_TYPES_MAP.get(type);
						name = keyTypePair[1].trim();
					}
					ParserConstant pc = new ParserConstant(name, value, clazz);
					storeConstant(pc);
				} else {
					System.out.println("No Key:Value found in line, ignoring: " + line);
				}
			}
			reader.close();
			inputStream.close();
		} catch (IOException e) {
			throw DevFailedUtils.newDevFailed(e);
		}
		return map;
	}

	public void loadDoubleArrayFromFile(String arrayName, String inputFilename) throws DevFailed {
		File inputFile = new File(inputFilename);
		FileInputStream fis;
		try {
			fis = new FileInputStream(inputFile);
		} catch (FileNotFoundException e) {
			logger.error("Error on filename: " + inputFilename, e);
			throw DevFailedUtils.newDevFailed("Error on filename: " + inputFilename);
		}
		loadDoubleArrayFromDetailedStream(arrayName, fis);
	}

	public void loadDoubleArrayFromFile(String arrayName, Path baseDir, String inputFilename) throws DevFailed {
		InputStream inputStream = fileNameToInputStream(baseDir, inputFilename);
		loadDoubleArrayFromDetailedStream(arrayName, inputStream);
	}

	public void loadSimpleDoubleArrayFromStringArray(String arrayName, String[] inputStrings) throws DevFailed {
		loadSimpleDoubleArrayFromStream(arrayName, stringArrayToInputStream(inputStrings), "");
	}

	public void loadSimpleDoubleArrayFromStream(String arrayName, InputStream inputStream, String escapeChar)
			throws DevFailed {
		Object array = null;
		BufferedInputStream bis = new BufferedInputStream(inputStream);
		InputStreamReader isr = new InputStreamReader(bis);

		List<List<Double>> arrayAsList = new ArrayList<>();
		int nbColumns = 0;
		int curLine = 0;
		try (BufferedReader reader = new BufferedReader(isr)) {
			String line;
			while ((line = getNextLineWithData(reader, escapeChar)) != null) {
				line = line.trim();
				String[] keyValuePair = line.split("[\\t\\s]+");
				if (nbColumns == 0) {
					nbColumns = keyValuePair.length;
				} else if (nbColumns != keyValuePair.length) {
					throw DevFailedUtils.newDevFailed(
							"Error on array " + arrayName + ". The number of columns must always be the same.");
				}
				List<Double> lineAsList = new ArrayList<>();
				for (int curColumn = 0; curColumn < keyValuePair.length; curColumn++) {
					String valueStr = keyValuePair[curColumn];
					double value = Double.NaN;
					try {
						value = Double.parseDouble(valueStr.trim());
					} catch (NumberFormatException e) {
						logger.error("Wrong value in array [" + curLine + "," + curColumn + "]=" + valueStr, e);
						throw DevFailedUtils
								.newDevFailed("Wrong value in array [" + curLine + "," + curColumn + "]=" + valueStr);
					}
					if (value == Double.NaN) {
						throw DevFailedUtils
								.newDevFailed("Wrong value in array [" + curLine + "," + curColumn + "]=" + valueStr);
					}
					lineAsList.add(value);
				}
				arrayAsList.add(lineAsList);
				curLine++;
			}
		} catch (IOException e) {
			throw DevFailedUtils.newDevFailed(e);
		}

		int nbLines = curLine;
		if (nbColumns > 1) {
			double[][] matrix = new double[nbLines][nbColumns];
			for (int i = 0; i < nbLines; i++) {
				List<Double> lineAsList = arrayAsList.get(i);
				for (int j = 0; j < nbColumns; j++) {
					Double value = lineAsList.get(j);
					matrix[i][j] = value.doubleValue();
				}
			}
			array = matrix;
		} else {
			double[] vector = new double[nbLines];
			for (int i = 0; i < nbLines; i++) {
				List<Double> lineAsList = arrayAsList.get(i);
				Double value = lineAsList.get(0);
				vector[i] = value.doubleValue();
			}
			array = vector;
		}

		ParserConstant pc = new ParserConstant(arrayName, array);
		storeConstant(pc);
	}

	public void loadDoubleArrayFromCommentedFile(String arrayName, String inputFilename) throws DevFailed {
		File inputFile = new File(inputFilename);
		FileInputStream fis;
		try {
			fis = new FileInputStream(inputFile);
		} catch (FileNotFoundException e) {
			logger.error("Error on filename: " + inputFilename, e);
			throw DevFailedUtils.newDevFailed("Error on filename: " + inputFilename);
		}
		loadSimpleDoubleArrayFromStream(arrayName, fis, commentChar);
	}

	public void loadDoubleArrayFromDetailedStream(String arrayName, InputStream inputStream) throws DevFailed {
		Object array = null;
		BufferedInputStream bis = new BufferedInputStream(inputStream);
		InputStreamReader isr = new InputStreamReader(bis);

		try (BufferedReader reader = new BufferedReader(isr)) {
			String nbColumnsStr = getNextLineWithData(reader);
			String nbLinesStr = getNextLineWithData(reader);
			if (nbLinesStr != null && nbColumnsStr != null) {
				int nbLines = 0;
				try {
					nbLines = Integer.parseInt(nbLinesStr);
				} catch (NumberFormatException e) {
					logger.error("Wrong number of lines.", e);
					throw DevFailedUtils.newDevFailed("Wrong number of lines : " + nbLinesStr);
				}
				int nbColumns = 0;
				try {
					nbColumns = Integer.parseInt(nbColumnsStr);
				} catch (NumberFormatException e) {
					logger.error("Wrong number of columns.", e);
					throw DevFailedUtils.newDevFailed("Wrong number of columns : " + nbColumnsStr);
				}

				// String header =
				getNextLineWithData(reader);

				if (nbColumns > 1) {
					array = read2DMatrix(reader, nbLines, nbColumns);
				} else {
					array = readVector(reader, nbLines);
				}
			}
			reader.close();
			inputStream.close();
		} catch (IOException e) {
			throw DevFailedUtils.newDevFailed(e);
		}
		ParserConstant pc = new ParserConstant(arrayName, array);
		storeConstant(pc);
	}

	private double[] readVector(BufferedReader reader, int nbLines) throws DevFailed, IOException {
		double[] vector = new double[nbLines];

		int curLine = 0;
		String line;
		while ((line = getNextLineWithData(reader)) != null && curLine < nbLines) {
			String valueStr = line.trim();
			double value = Double.NaN;
			try {
				value = Double.parseDouble(valueStr.trim());
			} catch (NumberFormatException e) {
				logger.error("Wrong value in vector [" + curLine + "]=" + valueStr, e);
				throw DevFailedUtils.newDevFailed("Wrong value in vector [" + curLine + "]=" + valueStr);
			}
			vector[curLine] = value;
			curLine++;
		}

		return vector;
	}

	private double[][] read2DMatrix(BufferedReader reader, int nbLines, int nbColumns) throws DevFailed, IOException {
		double[][] matrix = new double[nbLines][nbColumns];

		int curLine = 0;
		String line;
		while ((line = getNextLineWithData(reader)) != null && curLine < nbLines) {
			String[] keyValuePair = line.split("[\\t\\s]+", nbColumns);
			for (int curColumn = 0; curColumn < keyValuePair.length; curColumn++) {
				String valueStr = keyValuePair[curColumn];
				double value = Double.NaN;
				try {
					value = Double.parseDouble(valueStr.trim());
				} catch (NumberFormatException e) {
					logger.error("Wrong value in array [" + curLine + "," + curColumn + "]=" + valueStr, e);
					throw DevFailedUtils
							.newDevFailed("Wrong value in array [" + curLine + "," + curColumn + "]=" + valueStr);
				}
				matrix[curLine][curColumn] = value;
			}
			curLine++;
		}

		return matrix;
	}

	private static String getNextLineWithData(BufferedReader reader) throws IOException {
		return getNextLineWithData(reader, "");
	}

	private static String getNextLineWithData(BufferedReader reader, String escapeChar) throws IOException {
		String line = null;

		while ((line = reader.readLine()) != null
				&& (line.trim().isEmpty() || (!escapeChar.isEmpty() && line.trim().startsWith(escapeChar))))
			;

		return line != null ? line.trim() : line;
	}

	public Map<String, ParserConstant> getConstants() {
		return unmodifiableConstants;
	}

}
