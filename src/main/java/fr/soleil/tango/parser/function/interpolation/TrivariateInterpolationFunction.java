package fr.soleil.tango.parser.function.interpolation;

import java.util.List;
import java.util.Stack;

import org.apache.commons.math3.analysis.TrivariateFunction;
import org.nfunk.jep.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;
import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

public class TrivariateInterpolationFunction extends TangoParserCustomFunction {
	private static final Transmorph transmorph = new Transmorph(new DefaultConverters());
	private final Logger logger = LoggerFactory.getLogger(TrivariateInterpolationFunction.class);

	private TrivariateFunction univariateFunction;

	/**
	 * Constructor.
	 * 
	 * @throws DevFailed
	 */
	public TrivariateInterpolationFunction(String functionName, ETrivariateInterpolation eTrivariateInterpolation,
			List<ParserConstant> specificParams) throws DevFailed {
		super(functionName);
		numberOfParameters = 3;
		this.univariateFunction = eTrivariateInterpolation.getFunction(specificParams);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
		// check the stack
		checkStack(stack);

		Double z = Double.NaN;
		Object element = stack.pop();
		try {
			z = transmorph.convert(element, Double.class);
		} catch (final ConverterException e) {
			logger.error("Convertion error of " + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		Double y = Double.NaN;
		element = stack.pop();
		try {
			y = transmorph.convert(element, Double.class);
		} catch (final ConverterException e) {
			logger.error("Convertion error of " + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		Double x = Double.NaN;
		element = stack.pop();
		try {
			x = transmorph.convert(element, Double.class);
		} catch (final ConverterException e) {
			logger.error("Convertion error of " + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		double result = Double.NaN;
		try {
			result = univariateFunction.value(x, y, z);
		} catch (IllegalArgumentException e) {
			logger.error("Interpolation error" + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		// push the result on the inStack
		stack.push(result);
	}

}
