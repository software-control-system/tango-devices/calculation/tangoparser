package fr.soleil.tango.parser.function.interpolation;

import java.util.List;
import java.util.Stack;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.nfunk.jep.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;
import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

public class UnivariateInterpolationFunction extends TangoParserCustomFunction {
	private static final Transmorph transmorph = new Transmorph(new DefaultConverters());
	private final Logger logger = LoggerFactory.getLogger(UnivariateInterpolationFunction.class);

	private UnivariateFunction univariateFunction;

	/**
	 * Constructor.
	 * 
	 * @throws DevFailed
	 */
	public UnivariateInterpolationFunction(String functionName, EUnivariateInterpolation eUnivariateInterpolation,
			List<ParserConstant> specificParams) throws DevFailed {
		super(functionName);
		numberOfParameters = 1;
		this.univariateFunction = eUnivariateInterpolation.getFunction(specificParams);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
		Double input = Double.NaN;
		Object element = stack.pop();
		try {
			input = transmorph.convert(element, Double.class);
		} catch (final ConverterException e) {
			logger.error("Convertion error of " + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		double result = Double.NaN;
		try {
			result = univariateFunction.value(input);
		} catch (IllegalArgumentException e) {
			logger.error("Interpolation error" + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		// push the result on the inStack
		stack.push(result);
	}

}
