package fr.soleil.tango.parser.function.interpolation;

public enum EInterpolationType {
	Univariate,
	Bivariate,
	Trivariate,
	Multivariate;
	
	
	public static EInterpolationType valueOfIgnoreCase(String valueStr) {
		if (valueStr != null) {
			for (EInterpolationType v : values()) {
				if( v.name().equalsIgnoreCase(valueStr) ) {
					return v;
				}
			}
		}
		return null;
	}

}
