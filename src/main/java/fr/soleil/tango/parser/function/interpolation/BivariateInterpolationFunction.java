package fr.soleil.tango.parser.function.interpolation;

import java.util.List;
import java.util.Stack;

import org.apache.commons.math3.analysis.BivariateFunction;
import org.nfunk.jep.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;
import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

public class BivariateInterpolationFunction extends TangoParserCustomFunction {
	private static final Transmorph transmorph = new Transmorph(new DefaultConverters());
	private final Logger logger = LoggerFactory.getLogger(BivariateInterpolationFunction.class);

	private BivariateFunction univariateFunction;

	/**
	 * Constructor.
	 * 
	 * @throws DevFailed
	 */
	public BivariateInterpolationFunction(String functionName, EBivariateInterpolation eBivariateInterpolation,
			List<ParserConstant> specificParams) throws DevFailed {
		super(functionName);
		numberOfParameters = 2;
		this.univariateFunction = eBivariateInterpolation.getFunction(specificParams);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
		// check the stack
		checkStack(stack);

		Double y = Double.NaN;
		Object element = stack.pop();
		try {
			y = transmorph.convert(element, Double.class);
		} catch (final ConverterException e) {
			logger.error("Convertion error of " + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		Double x = Double.NaN;
		element = stack.pop();
		try {
			x = transmorph.convert(element, Double.class);
		} catch (final ConverterException e) {
			logger.error("Convertion error of " + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		double result = Double.NaN;
		try {
			result = univariateFunction.value(x, y);
		} catch (IllegalArgumentException e) {
			logger.error("Interpolation error" + element != null ? element.toString() : "null", e);
			throw new ParseException(e.getMessage());
		}
		// push the result on the inStack
		stack.push(result);
	}

}
