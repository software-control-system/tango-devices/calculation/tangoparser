/*
 * Created on 21 juin 2005
 * with Eclipse
 */
package fr.soleil.tango.parser.function;

import java.util.Stack;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * This class find the max value
 * 
 * @author HARDION
 * @version 0.1
 */
public final class Max extends PostfixMathCommand {

    /**
     *
     */
    public Max() {
	super();
	// Use a variable number of arguments
	numberOfParameters = -1;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
	Number result = FunctionUtils.getStackValue(stack);
	// repeat summation for each parameter
	for (int i = 1; i < curNumberOfParameters; ++i) {
	    // get the parameter from the stack
	    final Object param = stack.pop();
	    if (param instanceof Number) {
		// calculate the result
		if (result.doubleValue() < ((Number) param).doubleValue()) {
		    result = (Number) param;
		}
	    } else {
		throw new ParseException("Invalid parameter type " + param.getClass().getCanonicalName());
	    }
	}
	// push the result on the inStack
	stack.clear();
	stack.push(result);
    }
}
