/**
 *
 */
package fr.soleil.tango.parser.function.spectrum;

import java.util.Stack;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.lsmp.djep.vectorJep.Dimensions;
import org.lsmp.djep.vectorJep.function.UnaryOperatorI;
import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.MatrixValueI;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

/**
 * @author G. Pichon
 *
 */
public final class Replicate extends PostfixMathCommand implements UnaryOperatorI {

	/**
	 *
	 */
	public Replicate() {
		numberOfParameters = 2;
	}

	/**
	 * Runs the add operation on the inStack. The parameter is popped off the
	 * <code>inStack</code>, and the log of it's value is pushed back to the top of
	 * <code>inStack</code>.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run(@SuppressWarnings("rawtypes") final Stack inStack) throws ParseException {
		// check the stack
		checkStack(inStack);

		// get the 1st parameter from input stack : the number of value / the spectrum
		// size
		final Object param1 = inStack.pop();

		// get the 2nd parameter from input stack : the initial value
		final Object param2 = inStack.pop();

		// convert to int if possible
		final Transmorph transmorph = new Transmorph(new DefaultConverters());
		int size;
		try {
			size = transmorph.convert(param1, int.class);
		} catch (final ConverterException e) {
			throw new ParseException(ExceptionUtils.getStackTrace(e));
		}

		if (size < 0) {
			// Error
			throw new ParseException(Integer.toString(size) + " is not a valid size.");
		}

		MVector res = new MVector(size);
		for (int i = 0; i < size; i++) {
			res.setEle(i, param2);
		}

		// push the result on the inStack
		inStack.push(res);
	}

	@Override
	public Dimensions calcDim(Dimensions ldim) {
		return Dimensions.ONE;
	}

	@Override
	public MatrixValueI calcValue(MatrixValueI res, MatrixValueI lhs) throws ParseException {
		return null;
	}
}
