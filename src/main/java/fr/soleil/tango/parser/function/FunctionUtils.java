package fr.soleil.tango.parser.function;

import java.util.Stack;

import org.nfunk.jep.ParseException;

public final class FunctionUtils {

    private FunctionUtils() {

    }

    public static Number getStackValue(final Stack<?> stack) throws ParseException {
	// Check if stack is null
	if (null == stack) {
	    throw new ParseException("Stack argument null");
	}
	final Object param = stack.pop();
	Number result;
	if (param instanceof Number) {
	    result = (Number) param;
	} else {
	    throw new ParseException(param.getClass().getCanonicalName() + " is not a valid type - must be a Number");
	}
	return result;
    }

}
