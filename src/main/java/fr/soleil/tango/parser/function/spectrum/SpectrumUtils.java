package fr.soleil.tango.parser.function.spectrum;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Stack;

import org.lsmp.djep.vectorJep.values.MVector;
import org.nfunk.jep.ParseException;

public final class SpectrumUtils {

	private SpectrumUtils() {

	}

	public static MVector getMVector(final Stack<?> stack) throws ParseException {
		if (null == stack) {
			throw new ParseException("Stack argument null");
		}
		final Object object = stack.pop();
		MVector mVector;
		if (object.getClass().isArray()) {
			int length = Array.getLength(object);
			mVector = new MVector(length);
			for (int i = 0; i < length; i++) {
				mVector.setEle(i, Array.get(object, i));
			}
		} else if (object instanceof List<?>) {
			List<?> list = (List<?>) object;
			mVector = new MVector(list.size());
			for (int i = 0; i < list.size(); i++) {
				mVector.setEle(i, list.get(i));
			}
		} else if (object instanceof MVector) {
			mVector = (MVector) object;
		} else {
			mVector = new MVector(1);
			mVector.setEle(0, object);
		}
		return mVector;
	}

	public static MVector[] get2MVectors(final Stack<?> stack) throws ParseException {

		// get the parameter from the stack
		final MVector vect2 = SpectrumUtils.getMVector(stack);
		final MVector vect1 = SpectrumUtils.getMVector(stack);

		if (vect1.getNumEles() != vect2.getNumEles()) {
			throw new ParseException("both parameters must have the same size");
		}
		return new MVector[] { vect2, vect1 };
	}

}
