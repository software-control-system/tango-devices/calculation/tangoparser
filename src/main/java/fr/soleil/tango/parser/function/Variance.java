/*
 * Created on 22 juin 2005
 * with Eclipse
 */
package fr.soleil.tango.parser.function;

import java.util.Stack;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.Add;
import org.nfunk.jep.function.Divide;
import org.nfunk.jep.function.PostfixMathCommand;
import org.nfunk.jep.function.Power;
import org.nfunk.jep.function.Subtract;

/**
 * Variance function :
 *
 * TODO mettre la formule math�matique
 *
 * @author HARDION
 * @version TODO Insert Class Description
 *
 */
public class Variance extends PostfixMathCommand {

    private final Add addFunction = new Add();
    private final Divide divideFunction = new Divide();
    private final Mean meanFunction = new Mean();
    private final Power powerFunction = new Power();
    private final Subtract subtractFunction = new Subtract();

    /**
     * Constructor.
     */
    public Variance() {
        // Use a variable number of arguments
        numberOfParameters = -1;
    }

    /**
     * Calculates the result of summing up all parameters, which are assumed to be of the Double type.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {

        // check the stack
        checkStack(stack);

        // Calculate variance
        Number variance;

        if (curNumberOfParameters > 1) {
            // 1st : Calculate Mean
            final Stack<?> meanStack = (Stack<?>) stack.clone();
            // need to tell the class how many parameters it can take off
            // the stack because it accepts a variable number of params
            meanFunction.setCurNumberOfParameters(stack.size());
            meanFunction.run(meanStack);
            final Number mean = (Number) meanStack.pop();

            // TODO optimize
            // 2nd : Calculate Variance
            Object param = stack.pop();
            Number sum;
            if (param instanceof Number) {
                // xi-�
                param = subtractFunction.sub((Number) param, mean);
                // (xi-�)�
                sum = (Number) powerFunction.power((Number) param, Double.valueOf(2.0));
            } else {
                throw new ParseException("Invalid parameter type");
            }

            // repeat summation for each one of the current parameters
            for (int i = 1; i < curNumberOfParameters; ++i) {
                // get the parameter from the stack
                param = stack.pop();
                if (param instanceof Number) {
                    // xi-�
                    param = subtractFunction.sub((Number) param, mean);
                    // (xi-�)�
                    param = powerFunction.power(param, Double.valueOf(2.0));
                    // Add to sum
                    sum = (Number) addFunction.add(param, sum);

                } else {
                    throw new ParseException("Invalid parameter type");
                }
            }
            variance = divideFunction.div(sum, Double.valueOf(curNumberOfParameters - 1));
        } else {
            stack.clear();
            variance = Double.valueOf(0);
        }
        // push the result on the Stack
        stack.push(variance);
    }

}
