package fr.soleil.tango.parser.function;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubString extends PostfixMathCommand {

    private final Logger logger = LoggerFactory.getLogger(SubString.class);

    public SubString() {
        super();
        numberOfParameters = 3;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
        final Transmorph transmorph = new Transmorph(new DefaultConverters());
        int endIndex;
        try {
            endIndex = transmorph.convert(stack.pop(), int.class);
        } catch (final ConverterException e) {
            logger.error("convertion error", e);
            throw new ParseException(e.getMessage());
        }
        int beginIndex;
        try {
            beginIndex = transmorph.convert(stack.pop(), int.class);
        } catch (final ConverterException e) {
            logger.error("convertion error", e);
            throw new ParseException(e.getMessage());
        }

        String input;
        try {
            input = transmorph.convert(stack.pop(), String.class);
        } catch (final ConverterException e) {
            logger.error("convertion error", e);
            throw new ParseException(e.getMessage());
        }

        final String result = input.substring(beginIndex, endIndex);
        // push the result on the inStack
        stack.clear();
        stack.push(result);
    }
}
