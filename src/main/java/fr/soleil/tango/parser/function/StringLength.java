package fr.soleil.tango.parser.function;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringLength extends PostfixMathCommand {

    private final Logger logger = LoggerFactory.getLogger(StringLength.class);

    public StringLength() {
        super();
        numberOfParameters = 1;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
        final Transmorph transmorph = new Transmorph(new DefaultConverters());

        String input;
        try {
            input = transmorph.convert(stack.pop(), String.class);
        } catch (final ConverterException e) {
            logger.error("convertion error", e);
            throw new ParseException(e.getMessage());
        }

        final int result = input.length();
        // push the result on the inStack
        stack.clear();
        stack.push(result);
    }
}
