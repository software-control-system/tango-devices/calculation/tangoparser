/*
 * Created on 21 juin 2005
 * with Eclipse
 */
package fr.soleil.tango.parser.function;

import java.util.Stack;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.Divide;
import org.nfunk.jep.function.Sum;

/**
 * This class calculate the mean
 *
 * @author HARDION
 * @version 0.1
 */
public final class Mean extends Sum {

    private final Divide divideFunction = new Divide();

    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {

        super.run(stack);
        if (curNumberOfParameters > 0) {
            final Number sum = (Number) stack.pop();
            final Number result = divideFunction.div(sum, Double.valueOf(curNumberOfParameters));
            stack.push(result);
        }
    }
}
