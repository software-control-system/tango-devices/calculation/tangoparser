/**
 *
 */
package fr.soleil.tango.parser.function.spectrum;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.lsmp.djep.vectorJep.values.MVector;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * @author AYADI
 *
 */
public final class Sub extends PostfixMathCommand {

    /**
     *
     */
    public Sub() {
        numberOfParameters = 2;
    }

    /**
     * Runs the sub operation on the inStack. The parameter is popped off the <code>inStack</code>, and the substraction
     * by a scalar of it's value is pushed back to the top of <code>inStack</code>.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack inStack) throws ParseException {

        // check the stack
        checkStack(inStack);
        // get the 1st parameter from input stack
        final Object param1 = inStack.pop();
        // convert to double if possible
        final Transmorph transmorph = new Transmorph(new DefaultConverters());
        double toSub;
        try {
            toSub = transmorph.convert(param1, double.class);
        } catch (final ConverterException e) {
            throw new ParseException(ExceptionUtils.getStackTrace(e));
        }
        // get 2nd parameter from input stack
        final MVector vect = SpectrumUtils.getMVector(inStack);
        // convert all elements to doubles
        final double[] v1;
        try {
            v1 = transmorph.convert(vect.getEles(), double[].class);
        } catch (final ConverterException e) {
            throw new ParseException(ExceptionUtils.getStackTrace(e));
        }

        // calculate the result
        final MVector res = new MVector(vect.getNumEles());
        for (int i = 0; i < res.getNumEles(); i++) {
        	res.setEle(i, v1[i] - toSub);
        }
        // push the result on the inStack
        inStack.push(res);
    }
}
