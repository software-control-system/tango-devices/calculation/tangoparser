package fr.soleil.tango.parser.function;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Right extends PostfixMathCommand {

    private final Logger logger = LoggerFactory.getLogger(Right.class);

    public Right() {
        super();
        numberOfParameters = 2;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
        final Transmorph transmorph = new Transmorph(new DefaultConverters());
        int index;
        try {
            index = transmorph.convert(stack.pop(), int.class);
        } catch (final ConverterException e) {
            logger.error("convertion error", e);
            throw new ParseException(e.getMessage());
        }

        String input;
        try {
            input = transmorph.convert(stack.pop(), String.class);
        } catch (final ConverterException e) {
            logger.error("convertion error", e);
            throw new ParseException(e.getMessage());
        }
        final int stringLength = input.length();
        final String result = input.substring(stringLength - index, stringLength);
        // push the result on the inStack
        stack.clear();
        stack.push(result);
    }
}
