/**
 *
 */
package fr.soleil.tango.parser.function.spectrum;

import java.util.Stack;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.lsmp.djep.vectorJep.Dimensions;
import org.lsmp.djep.vectorJep.function.UnaryOperatorI;
import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.MatrixValueI;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

/**
 * @author G. Pichon
 *
 */
public final class SetSpectrumElem extends PostfixMathCommand implements UnaryOperatorI {

	/**
	 *
	 */
	public SetSpectrumElem() {
		numberOfParameters = 3;
	}

	/**
	 * Runs the add operation on the inStack. The parameter is popped off the
	 * <code>inStack</code>, and the log of it's value is pushed back to the top of
	 * <code>inStack</code>.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run(@SuppressWarnings("rawtypes") final Stack inStack) throws ParseException {
		// check the stack
		checkStack(inStack);

		// get the 1st parameter from input stack : the value to set in the spectrum
		final Object param1 = inStack.pop();

		// get the 2nd parameter from input stack : the index where to put the value
		final Object param2 = inStack.pop();

		// convert to double if possible
		final Transmorph transmorph = new Transmorph(new DefaultConverters());
		int index;
		try {
			index = transmorph.convert(param2, int.class);
		} catch (final ConverterException e) {
			throw new ParseException(ExceptionUtils.getStackTrace(e));
		}
		// get 3rd parameter from input stack : the original spectrum
		final MVector vect = SpectrumUtils.getMVector(inStack);

		int vectSize = vect.getNumEles();
		if (index < 0) {
			// Error
			throw new ParseException(Integer.toString(index) + " is not a valid index.");
		}

		int maxSize = Math.max(index + 1, vectSize);

		MVector res = new MVector(maxSize);
		Object defaultValue = null;
		if (maxSize > vectSize) {
			try {
				if (param1 instanceof Number) {
					defaultValue = transmorph.convert(0, param1.getClass());
				} else if (param1 instanceof Boolean) {
					defaultValue = new Boolean(false);
				} else {
					defaultValue = param1.getClass().newInstance();
				}
			} catch (InstantiationException | IllegalAccessException | SecurityException | ConverterException
					| IllegalArgumentException e) {
				throw new ParseException(ExceptionUtils.getStackTrace(e));
			}
		}
		for (int i = 0; i < maxSize; i++) {
			if (i != index) {
				if (i < vectSize) {
					res.setEle(i, vect.getEle(i));
				} else {
					res.setEle(i, defaultValue);
				}
			} else {
				res.setEle(i, param1);
			}
		}

		// push the result on the inStack
		inStack.push(res);
	}

	@Override
	public Dimensions calcDim(Dimensions ldim) {
		return Dimensions.ONE;
	}

	@Override
	public MatrixValueI calcValue(MatrixValueI res, MatrixValueI lhs) throws ParseException {
		return null;
	}
}
