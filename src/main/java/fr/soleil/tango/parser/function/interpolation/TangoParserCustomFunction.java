package fr.soleil.tango.parser.function.interpolation;

import org.nfunk.jep.function.PostfixMathCommand;

public abstract class TangoParserCustomFunction extends PostfixMathCommand {

	private String functionName;

	public TangoParserCustomFunction(String functionName) {
		super();
		this.functionName = functionName;
	}

	public String getFunctionName() {
		return functionName;
	}

}
