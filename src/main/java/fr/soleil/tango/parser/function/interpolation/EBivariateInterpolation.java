package fr.soleil.tango.parser.function.interpolation;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.math3.analysis.BivariateFunction;
import org.apache.commons.math3.analysis.interpolation.BicubicInterpolator;
import org.apache.commons.math3.analysis.interpolation.BivariateGridInterpolator;
import org.apache.commons.math3.analysis.interpolation.PiecewiseBicubicSplineInterpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;

public enum EBivariateInterpolation {

	BicubicInterpolator(BicubicInterpolator.class),
	PiecewiseBicubicSplineInterpolator(PiecewiseBicubicSplineInterpolator.class);

	private final Logger logger = LoggerFactory.getLogger(EBivariateInterpolation.class);
	private Class<? extends BivariateGridInterpolator> interpolatorClass;

	private EBivariateInterpolation(Class<? extends BivariateGridInterpolator> interpolatorClass) {
		this.interpolatorClass = interpolatorClass;
	}

	private BivariateGridInterpolator buildInterpolator() throws DevFailed {
		BivariateGridInterpolator interpolator = null;
		try {
			Constructor<? extends BivariateGridInterpolator> cstr = interpolatorClass.getConstructor();
			interpolator = cstr.newInstance();
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			logger.error("Unable to create interpolator " + this.name(), e);
			throw DevFailedUtils.newDevFailed(e);
		}
		return interpolator;
	}

	public BivariateFunction getFunction(List<ParserConstant> parameters) throws DevFailed {
		if (parameters.size() < 3) {
			throw DevFailedUtils.newDevFailed("Wrong number of argument. At least 2 vectors and 1 matrix are necessary for a bivariate interpolation.");
		}

		BivariateFunction function = null;
		BivariateGridInterpolator interpolator = buildInterpolator();
		double[] xval = parameters.get(0).getValue(double[].class);
		double[] yval = parameters.get(1).getValue(double[].class);
		double[][] fval = parameters.get(2).getValue(double[][].class);

		if (interpolator != null) {
			function = interpolator.interpolate(xval, yval, fval);
		}

		return function;
	}

	public static EBivariateInterpolation valueOfIgnoreCase(String valueStr) {
		if (valueStr != null) {
			for (EBivariateInterpolation v : values()) {
				if (v.name().equalsIgnoreCase(valueStr)) {
					return v;
				}
			}
		}
		return null;
	}
}
