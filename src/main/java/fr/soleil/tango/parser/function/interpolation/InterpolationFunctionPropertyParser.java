package fr.soleil.tango.parser.function.interpolation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;

public class InterpolationFunctionPropertyParser {

	private Map<String, ParserConstant> constants;

	public InterpolationFunctionPropertyParser(Map<String, ParserConstant> constants) {
		super();
		this.constants = constants;
	}

	/**
	 * 
	 * @param propertyLine Format : functionName, interpolation type, parameters,
	 *                     ...
	 * @return
	 * @throws DevFailed
	 */
	public TangoParserCustomFunction buildInterpolator(String propertyLine) throws DevFailed {
		TangoParserCustomFunction interpolator = null;

		String[] params = propertyLine.split(",");
		if (params.length > 1) {
			// At least the function name and the interpolation type
			String functionName = params[0].trim();
			if (functionName.contains(" ")) {
				throw DevFailedUtils.newDevFailed("A function name cannot contains spaces.",
						"A function name cannot contains spaces: '" + functionName + "'");
			}
			String interpolatorType = params[1].trim();
			EInterpolationType interType = EInterpolationType.valueOfIgnoreCase(interpolatorType);
			if (interType == null) {
				throw DevFailedUtils.newDevFailed("Unknown interpolator type.",
						"Unknown interpolator type: '" + interpolatorType + "'");
			}
			String[] specificParams = Arrays.copyOfRange(params, 2, params.length);
			interpolator = buildInterpolator(functionName, interType, specificParams);
		} else {
			throw DevFailedUtils.newDevFailed("Not enought parameters on a property line.",
					"The following property line does not contains enough parameters: " + propertyLine);
		}

		return interpolator;
	}

	private TangoParserCustomFunction buildInterpolator(String functionName, EInterpolationType interType,
			String[] specificParams) throws DevFailed {
		TangoParserCustomFunction interpolator = null;

		if (specificParams.length == 0) {
			throw DevFailedUtils.newDevFailed("Not enought parameters for an interpolation function.",
					"The following function line does not contains enough parameters: " + functionName);
		}
		String interpolatorType = specificParams[0].trim();

		List<ParserConstant> specificParamsList = new ArrayList<>(specificParams.length);
		for (int i = 1; i < specificParams.length; i++) {
			String specificParam = specificParams[i].trim();
			ParserConstant constant = constants.get(specificParam);
			if (constant == null) {
				throw DevFailedUtils.newDevFailed("Unknown interpolator constant parameter: '" + specificParam + "'",
						"Unknown interpolator constant parameter: '" + specificParam + "'");
			}
			specificParamsList.add(constant);
		}

		switch (interType) {
		case Univariate:
			interpolator = buildUnivariateInterpolator(functionName, interpolatorType, specificParamsList);
			break;
		case Bivariate:
			interpolator = buildBivariateInterpolator(functionName, interpolatorType, specificParamsList);
			break;
		case Trivariate:
			interpolator = buildTrivariateInterpolator(functionName, interpolatorType, specificParamsList);
			break;
		default:
			throw DevFailedUtils.newDevFailed("Unimplemented function.",
					"The interpolation function type " + interType.toString() + "is not yet implemented");
		}

		return interpolator;
	}

	private TangoParserCustomFunction buildTrivariateInterpolator(String functionName, String interpolatorType,
			List<ParserConstant> specificParamsList) throws DevFailed {
		ETrivariateInterpolation triInterType = ETrivariateInterpolation.valueOfIgnoreCase(interpolatorType);
		if (triInterType == null) {
			throw DevFailedUtils.newDevFailed("Unknown trivariate interpolator type.",
					"Unknown trivariate interpolator type: '" + triInterType + "'");
		}
		TangoParserCustomFunction interpolator = new TrivariateInterpolationFunction(functionName, triInterType,
				specificParamsList);
		return interpolator;
	}

	private TangoParserCustomFunction buildBivariateInterpolator(String functionName, String interpolatorType,
			List<ParserConstant> specificParamsList) throws DevFailed {
		EBivariateInterpolation biInterType = EBivariateInterpolation.valueOfIgnoreCase(interpolatorType);
		if (biInterType == null) {
			throw DevFailedUtils.newDevFailed("Unknown bivariate interpolator type.",
					"Unknown bivariate interpolator type: '" + biInterType + "'");
		}
		TangoParserCustomFunction interpolator = new BivariateInterpolationFunction(functionName, biInterType,
				specificParamsList);
		return interpolator;
	}

	private TangoParserCustomFunction buildUnivariateInterpolator(String functionName, String interpolatorType,
			List<ParserConstant> specificParamsList) throws DevFailed {
		EUnivariateInterpolation uniInterType = EUnivariateInterpolation.valueOfIgnoreCase(interpolatorType);
		if (uniInterType == null) {
			throw DevFailedUtils.newDevFailed("Unknown univariate interpolator type.",
					"Unknown univariate interpolator type: '" + uniInterType + "'");
		}
		TangoParserCustomFunction interpolator = new UnivariateInterpolationFunction(functionName, uniInterType,
				specificParamsList);
		return interpolator;
	}
}
