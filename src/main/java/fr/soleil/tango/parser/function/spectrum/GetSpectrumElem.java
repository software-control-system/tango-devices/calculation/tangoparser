/**
 *
 */
package fr.soleil.tango.parser.function.spectrum;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.lsmp.djep.vectorJep.Dimensions;
import org.lsmp.djep.vectorJep.function.UnaryOperatorI;
import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.MatrixValueI;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * @author G. Pichon
 *
 */
public final class GetSpectrumElem extends PostfixMathCommand implements UnaryOperatorI {

	/**
	 *
	 */
	public GetSpectrumElem() {
		numberOfParameters = 2;
	}

	/**
	 * Runs the add operation on the inStack. The parameter is popped off the
	 * <code>inStack</code>, and the log of it's value is pushed back to the top of
	 * <code>inStack</code>.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run(@SuppressWarnings("rawtypes") final Stack inStack) throws ParseException {
		// check the stack
		checkStack(inStack);

		// get the 1st parameter from input stack
		final Object param1 = inStack.pop();

		// convert to integer if possible
		final Transmorph transmorph = new Transmorph(new DefaultConverters());
		int index;
		try {
			index = transmorph.convert(param1, int.class);
		} catch (final ConverterException e) {
			throw new ParseException(ExceptionUtils.getStackTrace(e));
		}
		// get 2nd parameter from input stack
		final MVector vect = SpectrumUtils.getMVector(inStack);

		if (index < 0 || index > vect.getNumEles() - 1) {
			// Error
			throw new ParseException(Integer.toString(index) + " is not a valid index for a vector of size "
					+ Integer.toString(vect.getNumEles()) + ".");
		}

		// Gets the value at the given index
		Object res = vect.getEle(index);

		// push the result on the inStack
		inStack.push(res);
	}

	@Override
	public Dimensions calcDim(Dimensions ldim) {
		return Dimensions.ONE;
	}

	@Override
	public MatrixValueI calcValue(MatrixValueI res, MatrixValueI lhs) throws ParseException {
		return null;
	}
}
