package fr.soleil.tango.parser.function;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * extract a bit of a integer value. Takes 2 parameters: boolextract(integerValue, bit index)
 * 
 * @author ABEILLE
 * 
 */
public class BooleanExtractor extends PostfixMathCommand {

    private final Logger logger = LoggerFactory.getLogger(BooleanExtractor.class);

    public BooleanExtractor() {
	numberOfParameters = 2;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void run(final Stack stack) throws ParseException {

	final Transmorph transmorph = new Transmorph(new DefaultConverters());
	int index;
	try {
	    index = transmorph.convert(stack.pop(), int.class);
	} catch (final ConverterException e) {
	    logger.error("convertion error", e);
	    throw new ParseException(e.getMessage());
	}
	if (index < 0) {
	    throw new ParseException("index must be positive and less or equal to the number of bits");
	}

	int booleanCombination;
	try {
	    booleanCombination = transmorph.convert(stack.pop(), int.class);
	} catch (final ConverterException e) {
	    logger.error("convertion error", e);
	    throw new ParseException(e.getMessage());
	}
	final int maskResult = booleanCombination & (int) Math.pow(2, index);

	final boolean result = maskResult >>> index == 1;

	stack.push(result);

    }
}
