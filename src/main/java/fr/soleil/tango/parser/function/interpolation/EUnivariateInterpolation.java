package fr.soleil.tango.parser.function.interpolation;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.AkimaSplineInterpolator;
import org.apache.commons.math3.analysis.interpolation.DividedDifferenceInterpolator;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.interpolation.LoessInterpolator;
import org.apache.commons.math3.analysis.interpolation.NevilleInterpolator;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.interpolation.UnivariateInterpolator;
import org.apache.commons.math3.analysis.interpolation.UnivariatePeriodicInterpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;

public enum EUnivariateInterpolation {

	AkimaSplineInterpolator(AkimaSplineInterpolator.class),
	DividedDifferenceInterpolator(DividedDifferenceInterpolator.class), LinearInterpolator(LinearInterpolator.class),
	LoessInterpolator(LoessInterpolator.class), NevilleInterpolator(NevilleInterpolator.class),
	SplineInterpolator(SplineInterpolator.class), UnivariatePeriodicInterpolator(UnivariatePeriodicInterpolator.class);

	private final Logger logger = LoggerFactory.getLogger(EUnivariateInterpolation.class);
	private Class<? extends UnivariateInterpolator> interpolatorClass;

	private EUnivariateInterpolation(Class<? extends UnivariateInterpolator> interpolatorClass) {
		this.interpolatorClass = interpolatorClass;
	}

	private UnivariateInterpolator buildInterpolator() throws DevFailed {
		UnivariateInterpolator interpolator = null;
		try {
			Constructor<? extends UnivariateInterpolator> cstr = interpolatorClass.getConstructor();
			interpolator = cstr.newInstance();
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			logger.error("Unable to create interpolator " + this.name(), e);
			throw DevFailedUtils.newDevFailed(e);
		}
		return interpolator;
	}

	public UnivariateFunction getFunction(List<ParserConstant> parameters) throws DevFailed {
		if (parameters.size() < 1) {
			throw DevFailedUtils.newDevFailed("Wrong number of argument. At least 1 matrix or 2 vectors are necessary.");
		}
		int numberOfConstantsForData = 0;
		double[] xval = null;
		double[] yval = null;
		ParserConstant lastParam = parameters.get(parameters.size() - 1);
		if (lastParam.getValue().getClass().isArray()) {
			if (lastParam.getValue().getClass().getComponentType().isArray()) {
				// matrix case
				numberOfConstantsForData = 1;
				double[][] lookupTable = lastParam.getValue(double[][].class);
				xval = new double[lookupTable.length];
				yval = new double[lookupTable.length];
				for (int i = 0; i < lookupTable.length; i++) {
					double[] ds = lookupTable[i];
					if (ds.length > 1) {
						xval[i] = ds[0];
						yval[i] = ds[1];
					} else {
						throw DevFailedUtils.newDevFailed("Wrong size for univariate interpolator constant parameter.",
								"Wrong size for univariate interpolator constant parameter: '" + lastParam.getName()
										+ "'");
					}
				}
			} else {
				if (parameters.size() < 2) {
					throw DevFailedUtils
							.newDevFailed("Wrong number of argument. At least 1 matrix or 2 vector are necessary.");
				}
				// 2 vector case.
				numberOfConstantsForData = 2;
				ParserConstant secondToLastParam = parameters.get(parameters.size() - 2);
				xval = secondToLastParam.getValue(double[].class);
				yval = lastParam.getValue(double[].class);
			}
		} else {
			throw DevFailedUtils.newDevFailed("Wrong number of argument. At least 1 matrix or 2 vector are necessary.");
		}
		List<ParserConstant> constructorOptions = parameters.subList(0, parameters.size() - numberOfConstantsForData);

		UnivariateFunction function = null;
		UnivariateInterpolator interpolator = null;
		switch (this) {
		case AkimaSplineInterpolator:
		case DividedDifferenceInterpolator:
		case LinearInterpolator:
		case NevilleInterpolator:
		case SplineInterpolator:
			// No constructor options.
			interpolator = buildInterpolator();
			break;
		case LoessInterpolator:
			if (constructorOptions.size() >= 2) {
				double bandwidth = constructorOptions.get(0).getValue(double.class);
				int robustnessIters = constructorOptions.get(1).getValue(int.class);
				if (constructorOptions.size() == 2) {
					interpolator = new LoessInterpolator(bandwidth, robustnessIters);
				} else if (constructorOptions.size() >= 3) {
					double accuracy = constructorOptions.get(2).getValue(double.class);
					interpolator = new LoessInterpolator(bandwidth, robustnessIters, accuracy);
				}
			} else if(constructorOptions.size() == 0)  {
				interpolator = new LoessInterpolator();
			} else {
				throw DevFailedUtils.newDevFailed("Wrong number of argument for LoessInterpolator");
			}
			break;
		case UnivariatePeriodicInterpolator:
			if (constructorOptions.size() >= 2) {
				String secondInterpolatorName = constructorOptions.get(0).getValue(String.class);
				EUnivariateInterpolation secondInterpolatorType = EUnivariateInterpolation
						.valueOfIgnoreCase(secondInterpolatorName);
				if (secondInterpolatorType == null) {
					throw DevFailedUtils.newDevFailed("Wrong inner interpolator for UnivariatePeriodicInterpolator");
				}
				UnivariateInterpolator secondInterpolator = secondInterpolatorType.buildInterpolator();
				double period = constructorOptions.get(1).getValue(double.class);
				if (constructorOptions.size() == 2) {
					interpolator = new UnivariatePeriodicInterpolator(secondInterpolator, period);
				} else { // >=3
					int extend = constructorOptions.get(2).getValue(int.class);
					interpolator = new UnivariatePeriodicInterpolator(secondInterpolator, period, extend);
				}
			} else {
				throw DevFailedUtils.newDevFailed("Wrong number of argument for UnivariatePeriodicInterpolator");
			}
			break;
		default:
			break;
		}
		if (interpolator != null) {
			function = interpolator.interpolate(xval, yval);
		}

		return function;
	}

	public static EUnivariateInterpolation valueOfIgnoreCase(String valueStr) {
		if (valueStr != null) {
			for (EUnivariateInterpolation v : values()) {
				if (v.name().equalsIgnoreCase(valueStr)) {
					return v;
				}
			}
		}
		return null;
	}
}
