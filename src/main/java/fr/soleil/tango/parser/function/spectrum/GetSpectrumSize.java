/**
 *
 */
package fr.soleil.tango.parser.function.spectrum;

import java.util.Stack;

import org.lsmp.djep.vectorJep.Dimensions;
import org.lsmp.djep.vectorJep.function.UnaryOperatorI;
import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.MatrixValueI;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * @author G. Pichon
 *
 */
public final class GetSpectrumSize extends PostfixMathCommand implements UnaryOperatorI {

	/**
	 *
	 */
	public GetSpectrumSize() {
		numberOfParameters = 1;
	}

	/**
	 * Runs the add operation on the inStack. The parameter is popped off the
	 * <code>inStack</code>, and the log of it's value is pushed back to the top of
	 * <code>inStack</code>.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run(@SuppressWarnings("rawtypes") final Stack inStack) throws ParseException {
		// check the stack
		checkStack(inStack);

		// get 1st parameter from input stack
		final MVector vect = SpectrumUtils.getMVector(inStack);

		// push the result on the inStack
		inStack.push(vect.getNumEles());
	}

	@Override
	public Dimensions calcDim(Dimensions ldim) {
        return Dimensions.ONE;
	}

	@Override
	public MatrixValueI calcValue(MatrixValueI res, MatrixValueI lhs) throws ParseException {
		// TODO Auto-generated method stub
		return null;
	}
}
