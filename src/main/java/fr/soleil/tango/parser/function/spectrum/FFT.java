package fr.soleil.tango.parser.function.spectrum;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;
import org.lsmp.djep.vectorJep.Dimensions;
import org.lsmp.djep.vectorJep.function.UnaryOperatorI;
import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.MatrixValueI;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * An example custom function class for JEP.
 */
public final class FFT extends PostfixMathCommand implements UnaryOperatorI {

    /**
     * Constructor
     */
    public FFT() {
        numberOfParameters = 1;
    }

    @Override
    public Dimensions calcDim(final Dimensions arg0) {
        return Dimensions.ONE;
    }

    /**
     * Runs the fft operation on the inStack. The parameter is popped off the <code>inStack</code>, and the fft of it's
     * value is pushed back to the top of <code>inStack</code>.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void run(final Stack inStack) throws ParseException {
        // check the stack
        checkStack(inStack);
        // get input
        final MVector vect = SpectrumUtils.getMVector(inStack);
        // convert all elements to doubles
        final Transmorph transmorph = new Transmorph(new DefaultConverters());
        final double[] v1;
        try {
            v1 = transmorph.convert(vect.getEles(), double[].class);
        } catch (final ConverterException e) {
            throw new ParseException(ExceptionUtils.getStackTrace(e));
        }

        // calculate the result
        final FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);
        final Complex[] complex = fft.transform(v1, TransformType.FORWARD);
        final MVector res = new MVector(complex.length);
        for (int i = 0; i < complex.length; i++) {
        	res.setEle(i, complex[i].getReal());
        }

        // push the result on the inStack
        inStack.push(res);
    }

    @Override
    public MatrixValueI calcValue(final MatrixValueI arg0, final MatrixValueI arg1) throws ParseException {
        return null;
    }
}
