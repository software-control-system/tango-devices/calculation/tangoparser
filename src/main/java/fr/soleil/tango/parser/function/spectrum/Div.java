/**
 *
 */
package fr.soleil.tango.parser.function.spectrum;

import java.util.Stack;

import net.entropysoft.transmorph.ConverterException;
import net.entropysoft.transmorph.DefaultConverters;
import net.entropysoft.transmorph.Transmorph;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.lsmp.djep.vectorJep.Dimensions;
import org.lsmp.djep.vectorJep.function.UnaryOperatorI;
import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.MatrixValueI;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * @author AYADI
 *
 */
public final class Div extends PostfixMathCommand implements UnaryOperatorI {

    /**
     *
     */
    public Div() {
        numberOfParameters = 2;
    }

    /**
     * Runs the Div operation on the inStack. The parameter is popped off the <code>inStack</code>, and the log of it's
     * value is pushed back to the top of <code>inStack</code>.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack inStack) throws ParseException {
        checkStack(inStack);
        final MVector[] vects = SpectrumUtils.get2MVectors(inStack);
        final MVector vect2 = vects[0];
        final MVector vect1 = vects[1];

        // convert all elements to doubles
        final Transmorph transmorph = new Transmorph(new DefaultConverters());
        final double[] v1;
        final double[] v2;
        try {
            v1 = transmorph.convert(vect1.getEles(), double[].class);
        } catch (final ConverterException e) {
            throw new ParseException(ExceptionUtils.getStackTrace(e));
        }
        try {
            v2 = transmorph.convert(vect2.getEles(), double[].class);
        } catch (final ConverterException e) {
            throw new ParseException(ExceptionUtils.getStackTrace(e));
        }

        // calculate the result
        final MVector res = new MVector(vect1.getNumEles());
        for (int i = 0; i < res.getNumEles(); i++) {
        	res.setEle(i, v1[i] / v2[i]);
        }
        // push the result on the inStack
        inStack.push(res);
    }

    @Override
    public Dimensions calcDim(final Dimensions arg0) {
        return Dimensions.ONE;
    }

    @Override
    public MatrixValueI calcValue(final MatrixValueI arg0, final MatrixValueI arg1) throws ParseException {
        return null;
    }
}
