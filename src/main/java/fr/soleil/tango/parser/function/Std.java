/*
 * Created on 22 juin 2005
 * with Eclipse
 */
package fr.soleil.tango.parser.function;

import java.util.Stack;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.SquareRoot;

/**
 * @author HARDION
 * @version
 * 
 *          TODO Insert Class Description
 * 
 */
public final class Std extends Variance {

    private final SquareRoot squareRootFunction = new SquareRoot();

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void run(final Stack stack) throws ParseException {

	super.run(stack);
	stack.push(squareRootFunction.sqrt(stack.pop()));
    }
}
