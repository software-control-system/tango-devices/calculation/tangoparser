package fr.soleil.tango.parser.function.interpolation;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.math3.analysis.TrivariateFunction;
import org.apache.commons.math3.analysis.interpolation.TricubicInterpolator;
import org.apache.commons.math3.analysis.interpolation.TrivariateGridInterpolator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;

public enum ETrivariateInterpolation {

	TricubicInterpolator(TricubicInterpolator.class);

	private final Logger logger = LoggerFactory.getLogger(ETrivariateInterpolation.class);
	private Class<? extends TrivariateGridInterpolator> interpolatorClass;

	private ETrivariateInterpolation(Class<? extends TrivariateGridInterpolator> interpolatorClass) {
		this.interpolatorClass = interpolatorClass;
	}

	private TrivariateGridInterpolator buildInterpolator() throws DevFailed {
		TrivariateGridInterpolator interpolator = null;
		try {
			Constructor<? extends TrivariateGridInterpolator> cstr = interpolatorClass.getConstructor();
			interpolator = cstr.newInstance();
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			logger.error("Unable to create interpolator " + this.name(), e);
			throw DevFailedUtils.newDevFailed(e);
		}
		return interpolator;
	}

	public TrivariateFunction getFunction(List<ParserConstant> parameters) throws DevFailed {
		if (parameters.size() < 4) {
			throw DevFailedUtils
					.newDevFailed("Wrong number of argument. At least 3 vectors and 1 3D matrix are necessary.");
		}
		double[] xval = parameters.get(0).getValue(double[].class);
		double[] yval = parameters.get(1).getValue(double[].class);
		double[] zval = parameters.get(2).getValue(double[].class);
		double[][][] fval = parameters.get(3).getValue(double[][][].class);

		TrivariateFunction function = null;
		TrivariateGridInterpolator interpolator = buildInterpolator();

		if (interpolator != null) {
			function = interpolator.interpolate(xval, yval, zval, fval);
		}

		return function;
	}

	public static ETrivariateInterpolation valueOfIgnoreCase(String valueStr) {
		if (valueStr != null) {
			for (ETrivariateInterpolation v : values()) {
				if (v.name().equalsIgnoreCase(valueStr)) {
					return v;
				}
			}
		}
		return null;
	}
}
