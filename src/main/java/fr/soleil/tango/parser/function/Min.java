/*
 * Created on 21 juin 2005
 * with Eclipse
 */
package fr.soleil.tango.parser.function;

import java.util.Stack;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommand;

/**
 * This class find the max value
 *
 * @author HARDION
 * @version 0.1
 */
public final class Min extends PostfixMathCommand {

    /**
     *
     */
    public Min() {
        super();
        // Use a variable number of arguments
        numberOfParameters = -1;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void run(@SuppressWarnings("rawtypes") final Stack stack) throws ParseException {
        Number result = FunctionUtils.getStackValue(stack);
        // repeat summation for each one of the current parameters
        for (int i = 1; i < curNumberOfParameters; ++i) {
            // get the parameter from the stack
            final Object current = stack.pop();
            if (current instanceof Number) {
                // calculate the result
                if (result.doubleValue() > ((Number) current).doubleValue()) {
                    result = (Number) current;
                }
            } else {
                throw new ParseException("Invalid parameter type " + current.getClass().getCanonicalName());
            }

        }
        // push the result on the inStack
        stack.clear();
        stack.push(result);
    }
}
