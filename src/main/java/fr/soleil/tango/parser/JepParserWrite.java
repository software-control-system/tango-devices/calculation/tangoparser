package fr.soleil.tango.parser;

import java.util.List;
import java.util.Map;

import org.nfunk.jep.JEP;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.SymbolTable;
import org.nfunk.jep.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import com.google.common.collect.BiMap;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;
import fr.soleil.tango.parser.datasource.TangoSource;
import fr.soleil.tango.parser.function.interpolation.TangoParserCustomFunction;

/**
 * Manage a JEP expression to write on Tango attributes. NB: If this class is
 * used in multi-threaded environment: set lock to true for
 * {@link #refresh(boolean)} and {@link #getValue(boolean)}
 *
 * @see JEP
 * @author fourneau
 *
 */
public final class JepParserWrite extends AJepParser {
	private final Logger logger = LoggerFactory.getLogger(JepParserWrite.class);

	private String attributeName = "";
	private String writeSymbol = "";
	private String resultSymbol = "";
	private final String expression;

	/**
	 *
	 * @param expression The expression with pattern *=*
	 * @param name       The name of expression
	 * @param variables  The map of variables {key = variable name, value =
	 *                   attribute name}
	 * @throws DevFailed
	 */
	public JepParserWrite(final String deviceName, final String expression, final String name,
			final BiMap<String, String> variables, final List<AJepParser> variablesJepToRead,
			final Map<String, ParserConstant> constants, List<TangoParserCustomFunction> functions, final TangoSource dataSource) throws DevFailed {
		super(deviceName, name, expression, variables, variablesJepToRead, constants, functions, dataSource);
		this.expression = expression.trim();
		this.variables = variables;
		int equalIndex = expression.indexOf('=');
		if (equalIndex < 1) {
			// The equal symbol cannot be absent or the first character.
			throw DevFailedUtils.newDevFailed("The equal symbol cannot be absent or the first character.",
					"The symbol '=' is absent or is the first character.\nThe expression should be like x = f(y).");
		}
		resultSymbol = expression.substring(0, equalIndex).trim();
		if (resultSymbol.isEmpty()) {
			// The result symbol is the name of the writer, it's mandatory.
			throw DevFailedUtils.newDevFailed("The result symbol is missing.",
					"A result symbol is required before the '='.");
		}
		logger.debug("resultSymbol is {} for {} ", resultSymbol, expression);
	}

	@Override
	public String getExpression() {
		return expression;
	}

	/**
	 * Init JEP
	 *
	 * @throws ParseException
	 * @throws DevFailed
	 */
	@Override
	public void initExpression() throws ParseException, DevFailed {
		logger.debug("***building write expression - {} with variables {}", expression, variables);
		// retrieve the result name result=expression
		int equalIndex = expression.indexOf('=');
		if (equalIndex < 1) {
			// The equal symbol cannot be absent or the first character.
			throw DevFailedUtils.newDevFailed("The equal symbol cannot be absent or the first character.",
					"The symbol '=' is absent or is the first character.\nThe expression should be like x = f(y).");
		}
		resultSymbol = expression.substring(0, equalIndex).trim();
		if (resultSymbol.isEmpty()) {
			// The result symbol is the name of the writer, it's mandatory.
			throw DevFailedUtils.newDevFailed("The result symbol is missing.",
					"A result symbol is required before the '='.");
		}
		logger.debug("resultSymbol is {} for {} ", resultSymbol, expression);
		if (variables.containsValue(resultSymbol)) {
			// result attribute, will write on it
			attributeName = variables.inverse().get(resultSymbol);
			logger.debug("result will be written on attribute {} for {} ", attributeName, expression);
		}
		final String exp = expression.substring(equalIndex + 1).trim();

		initExpression(exp);

		final SymbolTable symbols = jep.getSymbolTable();
		for (final Object entry : symbols.entrySet()) {
			@SuppressWarnings("rawtypes")
			final Variable symbol = (Variable) ((Map.Entry) entry).getValue();
			final String symbolName = symbol.getName();
			if (name.equals(symbolName)) {
				writeSymbol = symbolName;
				logger.debug("write input is {} for {} ", writeSymbol, expression);
			}
		}
	}

	/**
	 * set a variable in expression
	 *
	 * @param value
	 * @throws DevFailed
	 */
	public void setValue(final Object value) throws DevFailed {
		// set write value to parser
		logger.debug("set write value \"{}\" = {} for {}", new Object[] { writeSymbol, value, expression });
		jep.addVariable(writeSymbol, fromStandardToJepType(value));
	}

	/**
	 * Evaluate the expression with latest values refreshed by
	 * {@link #refresh(boolean)} and last set value by {@link #setValue(Object)}
	 *
	 * @param lock true to use synchronization. For using Parser in multi-threaded
	 *             environment.
	 * @return the result of evaluation
	 * @throws DevFailed
	 */
	@Override
	public Object getValue(final boolean lock) throws DevFailed {
		Object lastWriteResult = super.getValue(lock);
		if (!attributeName.isEmpty()) {
			logger.debug("{} ({}) = {}", new Object[] { resultSymbol, attributeName, lastWriteResult });
		} else {
			logger.debug("result variable {} = {}", resultSymbol, lastWriteResult);
		}
		return lastWriteResult;
	}

	public boolean isAttribute() {
		return !attributeName.isEmpty();
	}

	@Override
	public String getName() {
		return resultSymbol;
	}

	public String getAttributeName() {
		return attributeName;
	}

}
