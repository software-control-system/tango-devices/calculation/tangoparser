package fr.soleil.tango.parser;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.lsmp.djep.vectorJep.Dimensions;
import org.lsmp.djep.vectorJep.VectorJep;
import org.lsmp.djep.vectorJep.values.MVector;
import org.lsmp.djep.vectorJep.values.Matrix;
import org.lsmp.djep.xjep.function.FromBase;
import org.lsmp.djep.xjep.function.ToBase;
import org.nfunk.jep.FunctionTable;
import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;
import org.nfunk.jep.Operator;
import org.nfunk.jep.ParseException;
import org.nfunk.jep.SymbolTable;
import org.nfunk.jep.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.CaseInsensitiveMap;
import org.tango.utils.DevFailedUtils;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;
import fr.soleil.tango.parser.datasource.AttributeResult;
import fr.soleil.tango.parser.datasource.TangoSource;
import fr.soleil.tango.parser.function.BooleanExtractor;
import fr.soleil.tango.parser.function.Left;
import fr.soleil.tango.parser.function.Max;
import fr.soleil.tango.parser.function.Mean;
import fr.soleil.tango.parser.function.Min;
import fr.soleil.tango.parser.function.Right;
import fr.soleil.tango.parser.function.Std;
import fr.soleil.tango.parser.function.StringLength;
import fr.soleil.tango.parser.function.SubString;
import fr.soleil.tango.parser.function.interpolation.TangoParserCustomFunction;
import fr.soleil.tango.parser.function.spectrum.Add;
import fr.soleil.tango.parser.function.spectrum.Div;
import fr.soleil.tango.parser.function.spectrum.FFT;
import fr.soleil.tango.parser.function.spectrum.GetSpectrumElem;
import fr.soleil.tango.parser.function.spectrum.GetSpectrumSize;
import fr.soleil.tango.parser.function.spectrum.Mul;
import fr.soleil.tango.parser.function.spectrum.Replicate;
import fr.soleil.tango.parser.function.spectrum.SetSpectrumElem;
import fr.soleil.tango.parser.function.spectrum.Sub;
import fr.soleil.tango.parser.function.spectrum.VCos;
import fr.soleil.tango.parser.function.spectrum.VLog;
import fr.soleil.tango.parser.function.spectrum.VSinus;

/**
 *
 * @author fourneau
 *
 */
public abstract class AJepParser {

	private Logger logger = LoggerFactory.getLogger(AJepParser.class);
	/**
	 * Use in throw DevFailed
	 */
	protected static final String CONFIG_ERROR = "CONFIG_ERROR";

	// private boolean isVector;

	protected final JEP jep;
	private boolean isVector;

	protected final String name;
	/**
	 * key = attribute name, value = variable name
	 */
	protected BiMap<String, String> variables = HashBiMap.create();
	protected final List<AJepParser> variablesJep;
	protected final Map<String, ParserConstant> constants;
	protected final Map<String, AJepParser> variablesJepToRead = new HashMap<>();
	protected final Lock locker = new ReentrantLock();
	protected final TangoSource dataSource;
	protected final String sourceName;
	protected String expressions;
	protected final CaseInsensitiveMap<String> attributes = new CaseInsensitiveMap<String>();

	protected AJepParser(final String deviceName, final String name, final String expressions,
			final BiMap<String, String> variables, final List<AJepParser> variablesJep,
			final Map<String, ParserConstant> constants, List<TangoParserCustomFunction> functions,
			final TangoSource dataSource) {
		sourceName = deviceName;
		this.name = name;
		this.expressions = expressions;
		this.variables = variables;
		this.variablesJep = variablesJep;
		this.constants = constants;
		this.dataSource = dataSource;

		jep = new VectorJep();
		jep.addStandardFunctions();
		jep.addStandardConstants();
		jep.addComplex();
		jep.setAllowUndeclared(true);
		jep.setAllowAssignment(true);
		jep.setTraverse(true);
		jep.setImplicitMul(true);

		jep.addFunction("fft", new FFT());
		jep.addFunction("vsin", new VSinus());
		jep.addFunction("vcos", new VCos());
		jep.addFunction("vlog", new VLog());
		jep.addFunction("add", new Add());
		jep.addFunction("sub", new Sub());
		jep.addFunction("div", new Div());
		jep.addFunction("mul", new Mul());
		jep.addFunction("mean", new Mean());
		jep.addFunction("std", new Std());
		jep.addFunction("max", new Max());
		jep.addFunction("min", new Min());
		jep.addFunction("boolextract", new BooleanExtractor());
		jep.addFunction("toBase", new ToBase());
		jep.addFunction("toDec", new ToBase(10));
		jep.addFunction("toHex", new ToBase(16, "0x"));
		jep.addFunction("fromDec", new FromBase(10));
		jep.addFunction("fromHex", new FromBase(16, "0x"));
		jep.addFunction("fromBase", new FromBase());
		jep.addFunction("substring", new SubString());
		jep.addFunction("stringlen", new StringLength());
		jep.addFunction("left", new Left());
		jep.addFunction("right", new Right());
		jep.addFunction("setSpectrumElem", new SetSpectrumElem());
		jep.addFunction("getSpectrumElem", new GetSpectrumElem());
		jep.addFunction("getSpectrumSize", new GetSpectrumSize());
		jep.addFunction("replicate", new Replicate());
		for (ParserConstant constant : constants.values()) {
			jep.addConstant(constant.getName(), constant.getValue());
		}
		for (TangoParserCustomFunction function : functions) {
			jep.addFunction(function.getFunctionName(), function);
		}
		if (logger.isInfoEnabled()) {
			logAvailableFunctions();
		}
	}

	protected final void setVector(final boolean isVector) {
		this.isVector = isVector;
		// When set the multiplication of vectors and matrices will be element by
		// element.
		// * Otherwise multiplication will be matrix multiplication
		((VectorJep) jep).setElementMultiply(isVector);
	}

	protected final Object parse(final String name, final String expression) throws DevFailed {
		Object result = null;
		logger.debug("parsing expression {} = {}", name, expression);
		try {
			final Node node = jep.parse(expression);
			logger.debug("using parser of type vector = {}", isVector);
			result = jep.evaluate(node);
		} catch (final ParseException e) {
			if (!isVector) {
				// if parsing is not OK, try with vector options
				try {
					setVector(true);
					final Node node = jep.parse(expression);
					result = jep.evaluate(node);
				} catch (final ParseException e1) {
					logger.error("Error in parsing of '{}' with expression '{}'.", name, expression);
					setVector(false);
					throw DevFailedUtils.newDevFailed(e1);
				}
			} else {
				logger.error("Error in parsing of '{}' with expression '{}'.", name, expression);
				throw DevFailedUtils.newDevFailed(e);
			}
		}
		logger.debug("result is {} = {}", expression, result);
		return result;
	}

	private void logAvailableFunctions() {
		final FunctionTable fc = jep.getFunctionTable();
		StringBuilder sb = new StringBuilder();
		for (final Object object : fc.keySet()) {
			sb.append(object).append(" ");
		}
		logger.info("available functions are: {}", sb);
		// for (final Object object : fc.entrySet()) {
		// logger.info("{}", object);
		// }
		sb = new StringBuilder();
		final Operator[] operators = jep.getOperatorSet().getOperators();
		for (int i = 0; i < operators.length; i++) {
			sb.append(operators[i].getSymbol()).append(" ");
		}
		logger.info("available operators are: {}", sb);
	}

	/**
	 * Getter on expression(s) (read or/and write)
	 *
	 * @return
	 */
	public abstract String getExpression();

	/**
	 * Update variables of the expression (ie reading tango attributes asynchrously)
	 *
	 * @param lock true to use synchronisation. For using Parser in multi-threaded
	 *             environment.
	 * @throws DevFailed
	 */
	public void refresh(boolean lock) throws DevFailed {
		if (lock) {
			locker.lock();
		}
		try {
			// refresh tango attributes
			dataSource.refresh();
			// refresh other variables
			for (final AJepParser variable : variablesJepToRead.values()) {
				variable.refresh(lock);
			}
		} finally {
			if (lock) {
				locker.unlock();
			}
		}
	}

	/**
	 * Evaluate the expression with latest values refreshed by
	 * {@link #refresh(boolean)}
	 *
	 * @param lock true to use synchronization. For using Parser in multi-threaded
	 *             environment.
	 * @return the result of evaluation
	 * @throws DevFailed
	 */
	public Object getValue(boolean lock)  throws DevFailed {
		return getValue(lock, new HashMap<>());
	}

	private Object getValue(boolean lock, Map<String, Object> tangoVariable) throws DevFailed {
		if (lock) {
			locker.lock();
		}
		try {
			// get results for tango attributes
			for (final String variableName : variables.values()) {
				if(!tangoVariable.containsKey(variableName)) {
					final AttributeResult r = dataSource.getResult(sourceName, variableName);
					if (r != null) {
						final Object value = r.getValue();
						final DevFailed e = r.getError();
						if (e != null && attributes.containsValue(variableName)) {
							throw e;
						} else if (value != null) {
							tangoVariable.put(variableName, value);
							jep.addVariable(variableName, fromStandardToJepType(value));
							logger.debug("add tango result : {}={}", variableName, value);
						}
					}
				} else {
					final Object value = tangoVariable.get(variableName);
					jep.addVariable(variableName, fromStandardToJepType(value));
				}
			}

			// get results for variables
			for (final AJepParser variable : variablesJepToRead.values()) {
				final Object r = variable.getValue(lock, tangoVariable);
				logger.debug("get input variable {} = {}", variable, r);
				jep.addVariable(variable.getName(), fromStandardToJepType(r));
			}
			Object lastWriteResult = parse(name, expressions);
			lastWriteResult = fromJepToStandardType(lastWriteResult);
			return lastWriteResult;
		} finally {
			if (lock) {
				locker.unlock();
			}
		}
	}

	public abstract String getName();

	@Override
	public String toString() {
		final ToStringBuilder toStringBuilder = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
		toStringBuilder.append("name", getName());
		toStringBuilder.append("expression", getExpression());
		return toStringBuilder.toString();
	}

	public abstract void initExpression() throws ParseException, DevFailed;

	protected final void initExpression(String expression) throws ParseException, DevFailed {
		List<String> missingSymbols = new ArrayList<>();

		jep.parse(expression);
		final SymbolTable symbols = jep.getSymbolTable();
		for (final Object entry : symbols.entrySet()) {
			@SuppressWarnings("rawtypes")
			final Variable symbol = (Variable) ((Map.Entry) entry).getValue();
			final String symbolName = symbol.getName();
			logger.debug("symbol {}", symbolName);
			if (variables.containsValue(symbolName)) {
				// attribute to read before parsing write expression
				final String attrName = variables.inverse().get(symbolName);
				logger.debug("read attribute {} ({}) for {} ", new Object[] { attrName, symbolName, expression });
				attributes.put(attrName, symbolName);
			} else { // not an attribute, may be the write input
				if (!symbol.isConstant() && !name.equals(symbolName) && !symbolName.equalsIgnoreCase("ii")
						&& !symbolName.equalsIgnoreCase("jj")) {
					boolean founded = false;
					for (final AJepParser jepReader : variablesJep) {
						if (jepReader.getName().equals(symbolName)) {
							logger.debug("add existing variable {}", jepReader.getName());
							variablesJepToRead.put(jepReader.getName(), jepReader);
							founded = true;
						}
					}
					if (!founded) {
						// The symbol is unknown. It's not a
						missingSymbols.add(symbolName);
					}
				}
			}
		}

		if (!missingSymbols.isEmpty()) {
			// At least 1 symbol is missing, an error is thrown
			StringBuilder symbolsStrBldr = new StringBuilder();
			for (String missingSymbol : missingSymbols) {
				if (symbolsStrBldr.length() > 0) {
					symbolsStrBldr.append(", ");
				}
				symbolsStrBldr.append(missingSymbol);
			}

			logger.warn("Some symbols in the expression are unknown: " + symbolsStrBldr.toString(),
					"In the expression '" + expression + "' the following symbols are missing:\n"
							+ symbolsStrBldr.toString());
		}

		if (!attributes.isEmpty()) {
			dataSource.addSource(sourceName, attributes);
		}
	}

	public CaseInsensitiveMap<String> getAttributes() {
		return attributes;
	}

	public Collection<AJepParser> getDependantJepParserList() {
		return variablesJepToRead.values();
	}

	/**
	 * This method transform a Jep vector into a standard array.
	 * 
	 * @param value The input value which may be an array.
	 * @return An array if the input value was a Jep vector.
	 */
	public static final Object fromJepToStandardType(Object value) {
		Object result = value;
		if (value instanceof MVector) {
			final MVector vect = (MVector) value;
			Class<? extends Object> clazz = vect.getEle(0).getClass();
			result = Array.newInstance(clazz, vect.getNumEles());
			// result = new Object[vect.getNumEles()];
			for (int j = 0; j < vect.getNumEles(); j++) {
				Array.set(result, j, vect.getEle(j));
			}
		} else if (value instanceof Matrix) {
			final Matrix matrix = (Matrix) value;
			Dimensions dims = matrix.getDim();
			int dim1 = dims.getFirstDim();
			int dim2 = dims.getLastDim();
			Class<? extends Object> clazz = Object.class;
			if (dim1 > 0 && dim2 > 0) {
				clazz = matrix.getEle(0).getClass();
			}
			Class<? extends Object> rowClazz = Array.newInstance(clazz, 0).getClass();
			result = Array.newInstance(rowClazz, dim1);
			for (int i = 0; i < dim1; i++) {
				Object subArray = Array.newInstance(clazz, dim2);
				for (int j = 0; j < dim2; j++) {
					Array.set(subArray, j, matrix.getEle(i, j));
				}
				Array.set(result, i, subArray);
			}
		}
		return result;
	}

	/**
	 * This method transform a standard array into a Jep vector.
	 * 
	 * @param value The input value
	 * @return A value which can be used in Jep.
	 */
	public static final Object fromStandardToJepType(Object value) {
		Object result = value;
		if (value.getClass().isArray()) {
			int dim1 = Array.getLength(value);
			if (value.getClass().getComponentType().isArray()) {
				// An array of array is a matrix.
				Object subArray0 = Array.get(value, 0);
				int dim2 = Array.getLength(subArray0);
				final Matrix matrix = (Matrix) Matrix.getInstance(dim1, dim2);
				for (int i = 0; i < dim1; i++) {
					Object subArray = Array.get(value, i);
					for (int j = 0; j < dim2; j++) {
						Object ele = Array.get(subArray, j);
						matrix.setEle(i, j, ele);
					}
				}
				result = matrix;
			} else {
				final MVector vect = new MVector(Array.getLength(value));
				for (int j = 0; j < Array.getLength(value); j++) {
					vect.setEle(j, Array.get(value, j));
				}
				result = vect;
			}
		}

		return result;
	}
}
