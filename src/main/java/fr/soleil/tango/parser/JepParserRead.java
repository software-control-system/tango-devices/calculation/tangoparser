package fr.soleil.tango.parser;

import java.util.List;
import java.util.Map;

import org.nfunk.jep.JEP;
import org.nfunk.jep.ParseException;

import com.google.common.collect.BiMap;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.parser.constants.ParserConstant;
import fr.soleil.tango.parser.datasource.TangoSource;
import fr.soleil.tango.parser.function.interpolation.TangoParserCustomFunction;

/**
 * Manage a read expression that is able to read Tango Attributes. Base on JEP
 * library. NB: If this class is used in multi-threaded environment: set lock to
 * true for {@link #refresh(boolean)} and {@link #getValue(boolean)}
 *
 * @author fourneau
 * @see JEP
 */
public final class JepParserRead extends AJepParser {

	private final String expressionsR;

	/**
	 * Build a read expression
	 *
	 * @param name         The name of the result
	 * @param expressionsR The read expression
	 * @param variables    The map of variables {key = variable name, value =
	 *                     attribute name}
	 * @param variablesJep The already build expressions. Allow interlinked
	 *                     expression
	 * @throws DevFailed
	 */
	public JepParserRead(final String deviceName, final String name, final String expressionsR,
			final BiMap<String, String> variables, final List<AJepParser> variablesJep,
			final Map<String, ParserConstant> constants, List<TangoParserCustomFunction> functions,
			final TangoSource dataSource) throws DevFailed {
		super(deviceName, name, expressionsR, variables, variablesJep, constants, functions, dataSource);
		this.expressionsR = expressionsR;
	}

	@Override
	public String getExpression() {
		return expressionsR;
	}

	/**
	 * Init the JEP parser
	 *
	 * @throws ParseException
	 * @throws DevFailed
	 */
	@Override
	public void initExpression() throws ParseException, DevFailed {
		initExpression(expressionsR);
	}

	@Override
	public String getName() {
		return name;
	}
}
