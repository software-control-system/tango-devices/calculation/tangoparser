package fr.soleil.tango.server.tangoparser.testserver;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.tango.server.ServerManager;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.parser.datasource.SourceManager;
import fr.soleil.tango.parser.datasource.TangoSource;
import fr.soleil.tango.server.tangoparser.TangoParser;

@RunWith(MockitoJUnitRunner.class)
public class TangoParserInitExprFailedTest {
	// Instance
	private static final String INSTANCE_NAME = "junit2";

	// DeviceName Parser
	private static final String deviceNameParser = "ICA/TEST/TANGOPARSER.JUNIT2";
    private static final String wDoublePub = "ICA/TEST/TANGOPARSER.PUB/value1DoubleW";

	private static final TangoSource mockSource = Mockito.mock(TangoSource.class);

	private static final SourceManager mngr = Mockito.spy(SourceManager.class);

	@BeforeClass
	public static void setUp() throws DevFailed {
		System.setProperty("org.tango.server.checkalarms", "false");

		// configure mocking
		doNothing().when(mockSource).getResults();

		doAnswer(new Answer<Boolean>() {
			@Override
			public Boolean answer(final InvocationOnMock invocation) {
				System.out.println("mock add source " + invocation);
				@SuppressWarnings("unchecked")
				final Map<String, String> arg = invocation.getArgumentAt(1, Map.class);
				for (final String s : arg.keySet()) {
					if (s.contains("spec")) {
						return true;
					}
				}
				return false;
			}
		}).when(mockSource).addSource(anyString(), anyMapOf(String.class, String.class));

		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(final InvocationOnMock invocation) {
				System.out.println("mock add write source " + invocation);
				return null;
			}
		}).when(mockSource).addWriteSource(anyString(), anyString());

		doAnswer(new Answer<TangoSource>() {
			@Override
			public TangoSource answer(final InvocationOnMock invocation) {
				System.out.println("mock build " + invocation);
				return mockSource;
			}
		}).when(mngr).build(anyBoolean(), anyString(), anyString());
		TangoParser.setSrcManager(mngr);

		// create device in tangodb
		System.out.println("create device in tango db");
		final Database db = ApiUtil.get_db_obj();
		db.add_device(deviceNameParser, TangoParser.class.getSimpleName(),
				TangoParser.class.getSimpleName() + "/" + INSTANCE_NAME);

		// configure properties
		final DbDatum[] dbDatum = new DbDatum[7];

		final String[] propAttributeNames = new String[] {"value1DoubleW," + wDoublePub};

		final String[] propOutputNames = new String[] {};

		final String[] propInputNames = new String[] {};
		final String[] propIONames = new String[] {
				"DevDouble IO1;IO2+IO3;value1DoubleW=IO1+1",
				"DevDouble IO2;IO3;value1DoubleW=IO2+2",
				"DevDouble IO3;IO1;value1DoubleW=IO3+3" };

		dbDatum[0] = new DbDatum("AttributeNames", propAttributeNames);
		dbDatum[1] = new DbDatum("OutputNames", propOutputNames);
		dbDatum[2] = new DbDatum("DiagnosticFunctions", false);
		dbDatum[3] = new DbDatum("InputNames", propInputNames);
		dbDatum[4] = new DbDatum("IONames", propIONames);
		dbDatum[5] = new DbDatum("MovingState", "RUNNING");
		dbDatum[6] = new DbDatum("ScanMode", true);

		System.out.println("put properties in DB");
		db.put_device_property(deviceNameParser, dbDatum);

		// start the tangoparser
		System.out.println("start the server");
		ServerManager.getInstance().addClass(TangoParser.class.getSimpleName(), TangoParser.class);
		ServerManager.getInstance().start(new String[] { INSTANCE_NAME }, TangoParser.class.getSimpleName());

		boolean launched = false;
		long maximumTime = 20000;
		long timer = maximumTime;
		long stepTime = 1500;
		DevState startState = DevState.UNKNOWN;
		while (!launched && timer > 0) {
			try {
				Thread.sleep(stepTime);
			} catch (final InterruptedException e) {
			}
			try {
				final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/state");
				startState = (DevState) tangoParser.read();
				System.out.println("State readed : " + startState.toString());
				launched = true;
			} catch (DevFailed e) {
				launched = false;
			}
			timer -= stepTime;
		}

		if (launched) {
			System.out.println("Device started in " + Double.toString((maximumTime - timer) / 1000.0) + " seconds.");
			System.out.println("Device state: " + startState.toString());
		} else {
			System.out.println("Device failed to start: " + startState.toString());
		}
		System.out.println("Set up done");
	}

	@AfterClass
	public static void tearDown() throws DevFailed {
		final Database db = ApiUtil.get_db_obj();
		db.delete_device_property(deviceNameParser, "*");
	}

	@Test
    @Ignore(value = "Doesn't work outside IDE. I Don't know why.")
	public void testExpDevFailed() throws DevFailed {
		final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/state");
		DevState startState = (DevState) tangoParser.read();

        assertThat(startState, equalTo(DevState.FAULT));
	}
}
