package fr.soleil.tango.server.tangoparser.testserver;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Array;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lsmp.djep.vectorJep.values.MVector;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.tango.server.ServerManager;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;
import fr.soleil.tango.parser.datasource.AttributeResult;
import fr.soleil.tango.parser.datasource.SourceManager;
import fr.soleil.tango.parser.datasource.TangoSource;
import fr.soleil.tango.server.tangoparser.TangoParser;

@RunWith(MockitoJUnitRunner.class)
public class TangoParserTest {
    // Instance
    private static final String INSTANCE_NAME = "junit";
    // Boolean
    private static final String booleanParser = "calculBoolean";
    // Boolean Publisher
    private static final String booleanPub = "ICA/TEST/TANGOPARSER.PUB/value1Boolean";
    // DeviceName Parser
    private static final String deviceNameParser = "ICA/TEST/TANGOPARSER.JUNIT";
    // Double
    private static final String doubleImgParser = "calculDoubleImg";
    private static final String doubleSumImgParser = "calculDoubleImgSum";
    private static final String doubleImgPub = "ICA/TEST/TANGOPARSER.PUB/value1DoubleIm";
    private static final String doubleParser = "calculDouble";
    private static final String doublePub = "ICA/TEST/TANGOPARSER.PUB/value1Double";
    private static final String doublePub2 = "ICA/TEST/TANGOPARSER.PUB/value2Double";
    private static final String doublePub3 = "ICA/TEST/TANGOPARSER.PUB/value3Double";
    private static final String doubleSpectrumParserAdd = "calculDoubleSpectrumAdd";
    private static final String doubleSpectrumParserCos = "calculDoubleSpectrumCos";
    private static final String doubleSpectrumParserDiv = "calculDoubleSpectrumDiv";
    private static final String doubleSpectrumParserDiv2 = "calculDoubleSpectrumDiv2";
    private static final String doubleSpectrumParserFft = "calculDoubleSpectrumFft";
    private static final String doubleSpectrumParserLog = "calculDoubleSpectrumLog";
    private static final String doubleSpectrumParserMax = "calculDoubleSpectrumMax";
    private static final String doubleSpectrumParserMean = "calculDoubleSpectrumMean";
    private static final String doubleSpectrumParserMin = "calculDoubleSpectrumMin";
    private static final String doubleSpectrumParserMul = "calculDoubleSpectrumMul";
    private static final String doubleSpectrumParserElem = "calculDoubleSpectrumElem";
    private static final String doubleSpectrumParserSet = "calculDoubleSpectrumSet";
    private static final String doubleSpectrumParserRep = "calculDoubleSpectrumRep";
    private static final String doubleSpectrumParserSize = "calculDoubleSpectrumSize";
    private static final String doubleSpectrumParserSin = "calculDoubleSpectrumSin";
    private static final String calculDoubleSpectrumVSUM = "calculDoubleSpectrumVSUM";
    private static final String doubleSpectrumParserStd = "calculDoubleSpectrumStd";
    private static final String doubleSpectrumParserSub = "calculDoubleSpectrumSub";
    private static final String doubleInterpol1 = "calculDoubleInterpol1";
    private static final String doubleInterpol2 = "calculDoubleInterpol2";
    private static final String doubleInterpol3 = "calculDoubleInterpol3";
    // Double Publisher
    private static final String doubleSpectrumPub = "ICA/TEST/TANGOPARSER.PUB/value1DoubleSpec";
    // Float
    private static final String floatParser = "calculFloat";
    private static final String floatPub = "ICA/TEST/TANGOPARSER.PUB/value1Float";
    private static final String floatSpectrumParserAdd = "calculFloatSpectrumAdd";
    private static final String floatSpectrumParserDiv = "calculFloatSpectrumDiv";
    // Float Publisher
    private static final String floatSpectrumPub = "ICA/TEST/TANGOPARSER.PUB/value1FloatSpec";
    // Integer
    private static final String intSpectrumParserAdd = "calculIntSpectrumAdd";

    private static final String intSpectrumParserCos = "calculIntSpectrumCos";
    private static final String intSpectrumParserDiv = "calculIntSpectrumDiv";
    // private static String intSpectrumParserDivForFail =
    // "calculIntSpectrumDivForFail";
    private static final String intSpectrumParserFft = "calculIntSpectrumFft";
    private static final String intSpectrumParserLog = "calculIntSpectrumLog";
    private static final String intSpectrumParserMul = "calculIntSpectrumMul";
    private static final String intSpectrumParserSin = "calculIntSpectrumSin";
    private static final String intSpectrumParserSub = "calculIntSpectrumSub";
    // Integer Publisher
    private static final String intSpectrumPub = "ICA/TEST/TANGOPARSER.PUB/value1IntSpec";
    private static final String ioParser = "IO";
    private static final String ioParserComplex = "IOComplex";

    // Long (like Interger)
    private static final String longParser = "calculLong";
    // Long Publisher
    private static final String longPub = "ICA/TEST/TANGOPARSER.PUB/value1Long";

    private static final String orderComplexeParser = "calculComplexeOrdre";

    private static final String orderParser = "calculOrdre";

    // Short
    private static final String shortParser = "calculShort";
    private static final String shortPub = "ICA/TEST/TANGOPARSER.PUB/value1Short";
    private static final String shortParserBooleanExtract = "booleanExtract";
    private static final String shortSpectrumParserAdd = "calculShortSpectrumAdd";
    private static final String shortSpectrumParserCos = "calculShortSpectrumCos";
    private static final String shortSpectrumParserDiv = "calculShortSpectrumDiv";
    private static final String shortSpectrumParserFft = "calculShortSpectrumFft";
    private static final String shortSpectrumParserLog = "calculShortSpectrumLog";
    private static final String shortSpectrumParserMul = "calculShortSpectrumMul";
    private static final String shortSpectrumParserSin = "calculShortSpectrumSin";
    private static final String shortSpectrumParserSub = "calculShortSpectrumSub";
    // Short Publisher
    private static final String shortSpectrumPub = "ICA/TEST/TANGOPARSER.PUB/value1ShortSpec";

    // Other
    private static final String stringParser = "calculString";
    private static final String stringPub = "ICA/TEST/TANGOPARSER.PUB/value1String";
    private static final String stringSpectrumParser = "calculStringSpectrum";
    private static final String stringSpectrumPub = "ICA/TEST/TANGOPARSER.PUB/value1StringSpec";
    private static final String wDoubleComplexParser = "D";
    private static final String wDoubleComplexPub = "ICA/TEST/TANGOPARSER.PUB/value2DoubleW";
    private static final String wDoubleParser = "A";
    private static final String wDoubleParserSpec = "B";
    private static final String wDoublePub = "ICA/TEST/TANGOPARSER.PUB/value1DoubleW";
    private static final String wDoublePubSpec = "ICA/TEST/TANGOPARSER.PUB/value1DoubleWSpec";
    private static final String wDoublesParser = "X";

    private static final String wDoubleSQRTParser = "C";

    //private static TangoSource mockSource = Mockito.mock(TangoSource.class, withSettings().verboseLogging());
    private static TangoSource mockSource = Mockito.mock(TangoSource.class);

    private static SourceManager mngr = Mockito.spy(SourceManager.class);

    @BeforeClass
    public static void setUp() throws DevFailed, InterruptedException {
        System.setProperty("org.tango.server.checkalarms", "false");
        // assertThat(System.getProperty("TANGO_HOST"), notNullValue());
        //System.setProperty("TANGO_HOST", "192.168.56.101:10000");

        // configure mocking
        doNothing().when(mockSource).getResults();

        doAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(final InvocationOnMock invocation) {
                System.out.println("mock add source " + invocation);
                @SuppressWarnings("unchecked") final Map<String, String> arg = invocation.getArgumentAt(1, Map.class);
                for (final String s : arg.keySet()) {
                    if (s.contains("spec")) {
                        return true;
                    }
                }
                return false;
            }
        }).when(mockSource).addSource(anyString(), anyMapOf(String.class, String.class));

        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) {
                System.out.println("mock add write source " + invocation);
                return null;
            }
        }).when(mockSource).addWriteSource(anyString(), anyString());

        doAnswer(new Answer<TangoSource>() {
            @Override
            public TangoSource answer(final InvocationOnMock invocation) {
                System.out.println("mock build " + invocation);
                return mockSource;
            }
        }).when(mngr).build(anyBoolean(), anyString(), anyString());
        TangoParser.setSrcManager(mngr);

        // create device in tangodb
        System.out.println("create device in tango db");
        final Database db = ApiUtil.get_db_obj();
        db.add_device(deviceNameParser, TangoParser.class.getSimpleName(), TangoParser.class.getSimpleName() + "/" + INSTANCE_NAME);

        // configure properties

        final String[] propAttributeNames = new String[]{"value1Boolean," + booleanPub, "value1Double," + doublePub,
        		"value2Double," + doublePub2, "value3Double," + doublePub3,
                "value1String," + stringPub, "value1DoubleSpec," + doubleSpectrumPub, "value1Short," + shortPub,
                "value1Long," + longPub, "value1Float," + floatPub, "value1StringSpec," + stringSpectrumPub,
                "value1DoubleW," + wDoublePub, "value1DoubleWSpec," + wDoublePubSpec,
                "value2DoubleW," + wDoubleComplexPub, "value1DoubleIm," + doubleImgPub,
                "value1FloatSpec," + floatSpectrumPub, "value1ShortSpec," + shortSpectrumPub,
                "value1IntSpec," + intSpectrumPub,};

        final String[] propOutputNames = new String[]{
        		"IMAGE DevDouble " + doubleImgParser + ",value1DoubleIm*2",
        		"DevDouble " + doubleSumImgParser + ",vsum(value1DoubleIm)",
                doubleParser + ",value1Double>2",
                "DevString " + stringParser + ",value1String+\"test\"",
                "DevBoolean " + booleanParser + ",!value1Boolean",
                "DevShort " + shortParser + ",value1Short*2",
                "DevBoolean " + shortParserBooleanExtract + ",boolextract(value1Short,3)",
                "DevLong " + longParser + ",value1Long^5",
                "DevFloat " + floatParser + ",value1Float+2",

                "SPECTRUM DevString " + stringSpectrumParser + ",add(value1StringSpec,\"aa\")",
                // "IMAGE DevDouble " + doubleImgParser + ",value1DoubleIm*2",
                // Addition
                "SPECTRUM DevDouble " + doubleSpectrumParserAdd + ",add(value1DoubleSpec,1)",
                "SPECTRUM DevFloat " + floatSpectrumParserAdd + ",add(value1FloatSpec,0.5)",
                "SPECTRUM DevLong " + intSpectrumParserAdd + ",add(value1IntSpec,1)",
                "SPECTRUM DevShort " + shortSpectrumParserAdd + ",add(value1ShortSpec,1)",
                // Division
                "SPECTRUM DevDouble " + doubleSpectrumParserDiv2 + ",value1DoubleSpec/2",
                "SPECTRUM DevDouble " + doubleSpectrumParserDiv + ",div(value1DoubleSpec,[2,2,2,2,2])",
                "SPECTRUM DevFloat " + floatSpectrumParserDiv + ",div(value1FloatSpec,value1FloatSpec)",
                "SPECTRUM DevLong " + intSpectrumParserDiv + ",div(value1IntSpec,value1IntSpec)",
                // "SPECTRUM DevLong " + intSpectrumParserDivForFail +
                // ",div(value1IntSpec,xx)",
                "SPECTRUM DevShort " + shortSpectrumParserDiv + ",div(value1ShortSpec,value1ShortSpec)",
                // Multiplication
                "SPECTRUM DevDouble " + doubleSpectrumParserMul + ",mul(value1DoubleSpec,value1DoubleSpec)",
                "SPECTRUM DevLong " + intSpectrumParserMul + ",mul(value1IntSpec,value1IntSpec)",
                "SPECTRUM DevShort " + shortSpectrumParserMul + ",mul(value1ShortSpec,value1ShortSpec)",
                //Spectrum manipulation
                "DevDouble " + doubleSpectrumParserElem + ",getSpectrumElem(value1DoubleSpec, value1Short)",
                "SPECTRUM DevDouble " + doubleSpectrumParserSet + ",setSpectrumElem(value1DoubleSpec,value1Short,value1Double)",
                "DevLong " + doubleSpectrumParserSize + ",getSpectrumSize(value1DoubleSpec)",
                "SPECTRUM DevDouble " + doubleSpectrumParserRep + ",replicate(value1Double,value1Short)",
                // Soustraction
                "SPECTRUM DevDouble " + doubleSpectrumParserSub + ",sub(value1DoubleSpec,1)",
                "SPECTRUM DevLong " + intSpectrumParserSub + ",sub(value1IntSpec,1)",
                "SPECTRUM DevShort " + shortSpectrumParserSub + ",sub(value1ShortSpec,1)",
                // Cos
                "SPECTRUM DevDouble " + doubleSpectrumParserCos + ",vcos(value1DoubleSpec)",
                "SPECTRUM DevLong " + intSpectrumParserCos + ",vcos(value1IntSpec)",
                "SPECTRUM DevShort " + shortSpectrumParserCos + ",vcos(value1ShortSpec)",
                // Sin
                "SPECTRUM DevDouble " + doubleSpectrumParserSin + ",vsin(value1DoubleSpec)",
                "SPECTRUM DevLong " + intSpectrumParserSin + ",vsin(value1IntSpec)",
                "SPECTRUM DevShort " + shortSpectrumParserSin + ",vsin(value1ShortSpec)",
                // Log
                "SPECTRUM DevDouble " + doubleSpectrumParserLog + ",vlog(value1DoubleSpec)",
                "SPECTRUM DevLong " + intSpectrumParserLog + ",vlog(value1IntSpec)",
                "SPECTRUM DevShort " + shortSpectrumParserLog + ",vlog(value1ShortSpec)",
                // Fft
                "SPECTRUM DevDouble " + doubleSpectrumParserFft + ",fft(value1DoubleSpec)",
                "SPECTRUM DevLong " + intSpectrumParserFft + ",fft(value1IntSpec)",
                "SPECTRUM DevShort " + shortSpectrumParserFft + ",fft(value1ShortSpec)",
                // TODO : test for GenMat
                "SPECTRUM DevDouble genMath, " + "GenMat(size(value1DoubleSpec), 1)"
                        + "+(GenMat(size(value1DoubleSpec), ii, ii)" + "-GenMat(size(value1DoubleSpec),1))"
                        + "+(GenMat(size(value1DoubleSpec), ii, ii)"
                        + "-GenMat(size(value1DoubleSpec),1))*(GenMat(size(value1DoubleSpec), ii, ii)"
                        + "-GenMat(size(value1DoubleSpec),1))",
                "SPECTRUM DevDouble genMath2, " + "GenMat(size(value1DoubleSpec),exp(ele(value1DoubleSpec,ii)),ii)",
                // vsum
                "DevDouble " + calculDoubleSpectrumVSUM + ",vsum(value1DoubleSpec)",
                // Max
                "DevDouble " + doubleSpectrumParserMax + ",max(1,2,3,4,5,6,7,8,9,10)",
                // Min
                "DevDouble " + doubleSpectrumParserMin + ",min(1,2,3,4,5,6,7,8,9,10)",
                // Mean
                "DevDouble " + doubleSpectrumParserMean + ",mean(1,2,3,4,5,6,7,8,9,10)",
                // Std
                "DevDouble " + doubleSpectrumParserStd + ",std(1,2,3,4,5,6,7,8,9,10)",
                // Interpolation
                "DevDouble " + doubleInterpol1 + ",interpol1(value1Double)",
                "DevDouble " + doubleInterpol2 + ",interpol2(value1Double)",
                "DevDouble " + doubleInterpol3 + ",interpol3(value1Double, value2Double)",
                // Test Order
                orderParser + ",value1DoubleW/value2DoubleW",
                orderComplexeParser
                        + ",(42*(value1DoubleW/(value1DoubleW*value2DoubleW))*(value1Short-value1Long+(value1Short/value1Long)))/(value1DoubleW/value2DoubleW*value1Short/value1Long)"
        };

        final String[] propInputNames = new String[]{"A,value1DoubleW=A+1", "SPECTRUM DevDouble B,value1DoubleWSpec=B*2",
                "C,value1DoubleW=sqrt(C)", "D,value2DoubleW=(((D^2)/2)>2) || 0", "X,value1DoubleW=X+2",
                "X,value2DoubleW=X+2"};

        final String[] propIONames = new String[]{
                "DevDouble IO;value1DoubleW+value1Short; value1DoubleW=IO+1",
                ioParserComplex
                        + ";0.5*12398.42/lambda/(sin(0.5*atan(150.0/value1DoubleW)));lambda=12398.42/(value2DoubleW*1000.0);value1DoubleW=150.0/tan(2.0*asin(lambda/2.0/IOComplex))",
                "SPECTRUM DevDouble IOSpectrum;[1, 2, 3, 4, 5, 6];value1DoubleWSpec=IOSpectrum/2",
                "DevDouble IOSpectrumFactor;A;value1DoubleWSpec=[1, 2, 3, 4, 5, 6]*IOSpectrumFactor"
        };
        final String[] propArrayData = new String[]{
        		"-10    0.0  1",
        		"-100   0.0  10",
        		"-1000  0.0  100"
        };
        final String[] propArrayTestInterX = new String[]{
        		"-1",
        		"0",
        		"1"
        };
        final String[] propArrayTestInterY = new String[]{
        		"-10",
        		"0",
        		"10"
        };
        final String[] propArrayTestInterX2 = new String[]{
        		"0",
        		"10",
        		"30",
        		"40",
        };
        final String[] propArrayTestInterY2 = new String[]{
        		"0",
        		"20",
        		"-20",
        		"0"
        };
        
        final String[] propInterpolationFunctions = new String[] {
            	"interpol1, univariate, LinearInterpolator, interx2, intery2",
            	"interpol2, univariate, SplineInterpolator, interx2, intery2",
            	"interpol3, bivariate, BicubicInterpolator, interx, intery, interdata"
        };

        final DbDatum[] dbDatum = new DbDatum[13];
        dbDatum[0] = new DbDatum("AttributeNames", propAttributeNames);
        dbDatum[1] = new DbDatum("OutputNames", propOutputNames);
        dbDatum[2] = new DbDatum("DiagnosticFunctions", false);
        dbDatum[3] = new DbDatum("InputNames", propInputNames);
        dbDatum[4] = new DbDatum("IONames", propIONames);
        dbDatum[5] = new DbDatum("MovingState", "RUNNING");
        dbDatum[6] = new DbDatum("ScanMode", true);
        dbDatum[7] = new DbDatum("Array_interData", propArrayData);
        dbDatum[8] = new DbDatum("Array_interX", propArrayTestInterX);
        dbDatum[9] = new DbDatum("Array_interY", propArrayTestInterY);
        dbDatum[10] = new DbDatum("Array_interX2", propArrayTestInterX2);
        dbDatum[11] = new DbDatum("Array_interY2", propArrayTestInterY2);
        dbDatum[12] = new DbDatum("InterpolationFunctions", propInterpolationFunctions);

        System.out.println("put properties in DB");
        db.put_device_property(deviceNameParser, dbDatum);

        // start the tangoparser
        System.out.println("start the server");
        ServerManager.getInstance().addClass(TangoParser.class.getSimpleName(), TangoParser.class);
        ServerManager.getInstance().start(new String[]{INSTANCE_NAME}, TangoParser.class.getSimpleName());

        boolean launched = true;
        long maximumTime = 20000;
        long timer = maximumTime;
        long stepTime = 1500;
        final DeviceProxy tangoParser = new DeviceProxy(deviceNameParser);
        DevState startState = tangoParser.state();
        try {
            while (startState.equals(DevState.INIT) && timer > 0) {
                TimeUnit.MILLISECONDS.sleep(stepTime);
                startState = tangoParser.state();
                System.out.println("State = " + startState);
                timer -= stepTime;
            }
            launched = timer > 0;
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }

        if (launched) {
            System.out.println("Device started in " + Double.toString(TimeUnit.MILLISECONDS.toSeconds(maximumTime - timer)) + " seconds.");
            System.out.println("Device state: " + tangoParser.state());
            System.out.println(tangoParser.status());
        } else {
            System.out.println("Device start up took too much time, aborting");
        }
        System.out.println("Set up done");

        // test its state
        // final DeviceProxy proxy = new DeviceProxy(deviceNameParser);
        // DevState state;
        // do {
        // state = proxy.state();
        // try {
        // Thread.sleep(150);
        // } catch (final InterruptedException e) {
        // }
        // } while (state.equals(DevState.INIT));
        // if (!state.equals(DevState.ON)) {
        // final String status = proxy.status();
        // System.err.println("device is not ON, status:\n" + status);
        // System.err.println("errors attribute value:\n"
        // + Arrays.toString(proxy.read_attribute("errors").extractStringArray()));
        // DevFailedUtils.throwDevFailed("INIT_ERROR", DeviceState.toString(state) + ":" + status);
        // }
    }

    @AfterClass
    public static void tearDown() throws DevFailed {
        final Database db = ApiUtil.get_db_obj();
        db.delete_device_property(deviceNameParser, "*");
    }

    @Test(expected = DevFailed.class)
    public void getExpDevFailedTest() throws DevFailed {

        final TangoCommand cmd = new TangoCommand(deviceNameParser, "GetExpression");
        cmd.executeExtract("XX");
    }

    @Test
    public void testInterpolLinear() throws DevFailed {
        final AttributeResult x = new AttributeResult(deviceNameParser, getAtt(doublePub), AttrDataFormat.SCALAR,
                doubleSpectrumParserSet);
        x.setValue(6);
        doReturn(x).when(mockSource).getResult(deviceNameParser, getAtt(doublePub));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleInterpol1);
        final Object result = tangoParser.read();
        //System.out.println("result interpol linear=" + result);
        //final Object value = valueOut;
        assertThat(result, equalTo(12.0));
    }

    @Test
    public void testInterpolSpline() throws DevFailed {
        final AttributeResult x = new AttributeResult(deviceNameParser, getAtt(doublePub), AttrDataFormat.SCALAR,
                doubleSpectrumParserSet);
        x.setValue(8);
        doReturn(x).when(mockSource).getResult(deviceNameParser, getAtt(doublePub));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleInterpol2);
        final Object result = tangoParser.read();
        //System.out.println("result interpol spline=" + result);
        //final Object value = valueOut;
        assertThat(result, equalTo(18.88));
    }

    @Test
    public void testInterpolBicubic() throws DevFailed {
        final AttributeResult x = new AttributeResult(deviceNameParser, getAtt(doublePub), AttrDataFormat.SCALAR,
                doubleSpectrumParserSet);
        x.setValue(-0.9);
        final AttributeResult y = new AttributeResult(deviceNameParser, getAtt(doublePub2), AttrDataFormat.SCALAR,
                doubleSpectrumParserSet);
        y.setValue(9.9);
        doReturn(x).when(mockSource).getResult(deviceNameParser, getAtt(doublePub));
        doReturn(y).when(mockSource).getResult(deviceNameParser, getAtt(doublePub2));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleInterpol3);
        final Object result = tangoParser.read();
        //System.out.println("result interpol bicubic=" + result);
        //final Object value = valueOut;
        assertThat(result, equalTo(1.25153678925));
    }

    @Test
    public void getExpTest() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(deviceNameParser, "GetExpression");
        final Object res = cmd.executeExtract("A");
        final Object value = new String[]{"value1DoubleW=A+1"};
        assertThat(res, equalTo(value));

    }

    @Test
    public void IOTest() throws DevFailed {

        // final TangoAttribute pub1 = new TangoAttribute(wDoublePub);
        // final TangoAttribute pub2 = new TangoAttribute(shortPub);
        // pub1.write(1D);
        // pub2.write((short) 1);
        final AttributeResult r1 = new AttributeResult(deviceNameParser, getAtt(wDoublePub), AttrDataFormat.SCALAR,
                ioParser);
        r1.setValue(1D);
        doReturn(r1).when(mockSource).getResult(deviceNameParser, getAtt(wDoublePub));

        final AttributeResult r2 = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SCALAR,
                ioParser);
        r2.setValue((short) 1);
        doReturn(r2).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));

        // final double expected = 1.0;

        // doAnswer(new Answer<Void>() {
        // @Override
        // public Void answer(final InvocationOnMock invocation) {
        // System.out.println("mock ioParser " + invocation);
        // return null;
        // }
        // }).when(mockSource).write(eq(ioParser), anyObject());
        Object expected = 2D;
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + ioParser);
        Object result = tangoParser.read();
        System.out.println("read " + result);
        assertThat(result, equalTo(expected));
        System.out.println("write");
        tangoParser.write(1D);
        // result = pub1.read();
        // System.out.println("read after write " + result);
        // assertThat(result, equalTo(valueR));
        verify(mockSource).write(eq(ioParser), eq(expected));
        final AttributeResult r3 = new AttributeResult(deviceNameParser, getAtt(wDoublePub), AttrDataFormat.SCALAR,
                ioParser);
        r3.setValue(expected);
        doReturn(r3).when(mockSource).getResult(deviceNameParser, getAtt(wDoublePub));

        result = tangoParser.read();
        expected = 3D;
        System.out.println("2 read after write " + result);
        assertThat(result, equalTo(expected));
    }

    @Test
    public void IOComplexTest() throws DevFailed {
        // final TangoAttribute pub1 = new TangoAttribute(wDoublePub);
        // final TangoAttribute pub2 = new TangoAttribute(wDoubleComplexPub);
        // pub1.write(10);
        // pub2.write(1);
        final AttributeResult r1 = new AttributeResult(deviceNameParser, getAtt(wDoublePub), AttrDataFormat.SCALAR,
                ioParserComplex);
        r1.setValue(10);
        doReturn(r1).when(mockSource).getResult(deviceNameParser, getAtt(wDoublePub));

        final AttributeResult r2 = new AttributeResult(deviceNameParser, getAtt(wDoubleComplexPub),
                AttrDataFormat.SCALAR, ioParserComplex);
        r2.setValue(1);
        doReturn(r2).when(mockSource).getResult(deviceNameParser, getAtt(wDoubleComplexPub));

        final double expected = 131.21255367817318;
        // doAnswer(new Answer<Void>() {
        // @Override
        // public Void answer(final InvocationOnMock invocation) {
        // System.out.println("mock ioParser " + invocation);
        // final double result = invocation.getArgumentAt(1, double.class);
        // assertThat(result, equalTo(expected));
        // return null;
        // }
        // }).when(mockSource).write(ioParser, expected);

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + ioParserComplex);
        System.out.println("######read ");
        final double result = tangoParser.read(double.class);
        System.out.println("read " + result);
        assertThat(result, equalTo(731.8671652065663));
        System.out.println("######write ");
        tangoParser.write(15);
        verify(mockSource).write(eq(ioParserComplex), eq(expected));
        // result = pub1.read(double.class);
        // System.out.println("read after write " + result);
        // assertThat(result, equalTo(131.21255367817318));
        // result = tangoParser.read();
        // valueR = 3D;
        // assertThat(result, equalTo(valueR));
    }

    @Test
    public void orderExpComplexeTest() throws DevFailed {
        // final TangoAttribute pub1 = new TangoAttribute(wDoublePub);
        // pub1.write(0.9876543210);
        // final TangoAttribute pub2 = new TangoAttribute(wDoubleComplexPub);
        // pub2.write((short) 10);
        // final TangoAttribute pub3 = new TangoAttribute(shortPub);
        // pub3.write((short) 8);
        // final TangoAttribute pub4 = new TangoAttribute(longPub);
        // pub4.write((short) 1990);
        final AttributeResult r1 = new AttributeResult(deviceNameParser, getAtt(wDoublePub), AttrDataFormat.SCALAR,
                orderComplexeParser);
        r1.setValue(0.9876543210);
        doReturn(r1).when(mockSource).getResult(deviceNameParser, getAtt(wDoublePub));
        final AttributeResult r2 = new AttributeResult(deviceNameParser, getAtt(wDoubleComplexPub),
                AttrDataFormat.SCALAR, orderComplexeParser);
        r2.setValue((short) 10);
        doReturn(r2).when(mockSource).getResult(deviceNameParser, getAtt(wDoubleComplexPub));
        final AttributeResult r3 = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SCALAR,
                orderComplexeParser);
        r3.setValue((short) 8);
        doReturn(r3).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));
        final AttributeResult r4 = new AttributeResult(deviceNameParser, getAtt(longPub), AttrDataFormat.SCALAR,
                orderComplexeParser);
        r4.setValue((short) 1990);
        doReturn(r4).when(mockSource).getResult(deviceNameParser, getAtt(longPub));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + orderComplexeParser);
        final Object result = tangoParser.read();
        final Object value = -20965739.28723793;
        assertThat(result, equalTo(value));
    }

    @Test
    public void orderExpTest() throws DevFailed {
        final AttributeResult r1 = new AttributeResult(deviceNameParser, getAtt(wDoublePub), AttrDataFormat.SCALAR,
                orderParser);
        r1.setValue((short) 2);
        doReturn(r1).when(mockSource).getResult(deviceNameParser, getAtt(wDoublePub));
        final AttributeResult r2 = new AttributeResult(deviceNameParser, getAtt(wDoubleComplexPub),
                AttrDataFormat.SCALAR, orderParser);
        r2.setValue((short) 10);
        doReturn(r2).when(mockSource).getResult(deviceNameParser, getAtt(wDoubleComplexPub));

        // final TangoAttribute pub1 = new TangoAttribute(wDoublePub);
        // pub1.write((short) 2);
        // final TangoAttribute pub2 = new TangoAttribute(wDoubleComplexPub);
        // pub2.write((short) 10);
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + orderParser);
        final Object result = tangoParser.read();
        final Object value = 0.2D;
        assertThat(result, equalTo(value));
    }

    @Test(expected = DevFailed.class)
    public void setExpDevFailedTest() throws DevFailed {
        final TangoCommand cmd = new TangoCommand(deviceNameParser, "SetExpression");
        cmd.executeExtract(new String[]{"XX", "A7=XX+2"});
    }

    @Test
    public void testReadBool() throws DevFailed {
        final AttributeResult booleanR = new AttributeResult(deviceNameParser, getAtt(booleanPub),
                AttrDataFormat.SCALAR, booleanParser);
        booleanR.setValue(true);
        doReturn(booleanR).when(mockSource).getResult(deviceNameParser, getAtt(booleanPub));
        // final TangoAttribute pub = new TangoAttribute(booleanPub);
        // pub.write(true);
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + booleanParser);
        final Object result = tangoParser.read();
        final Object value = false;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleIm() throws DevFailed {
        final AttributeResult imgR = new AttributeResult(deviceNameParser, getAtt(doubleImgPub),
                AttrDataFormat.IMAGE, doubleImgParser);
        imgR.setValue(new Double[][]{{1D, 2D, 3D, 4D, 5D}, {1D, 2D, 3D, 4D, 5D}});
        doReturn(imgR).when(mockSource).getResult(deviceNameParser, getAtt(doubleImgPub));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleImgParser);
        final Object result = tangoParser.read();
        final Object value = new Double[][]{{2D, 4D, 6D, 8D, 10D}, {2D, 4D, 6D, 8D, 10D}};
        assertThat(tangoParser.isImage(), equalTo(true));
        if (tangoParser.isImage()) {
            assertThat(result.getClass().isArray(), equalTo(true));
            assertThat(result.getClass().getComponentType().isArray(), equalTo(true));
            assertThat(Array.getLength(value), equalTo(Array.getLength(result)));
            assertThat(result, equalTo(value));
        }
    }

    @Test
    public void testReadDoubleSumIm() throws DevFailed {
        final AttributeResult imgR = new AttributeResult(deviceNameParser, getAtt(doubleImgPub),
                AttrDataFormat.IMAGE, doubleSumImgParser);
        imgR.setValue(new Double[][]{{1D, 2D, 3D, 4D, 5D}, {1D, 2D, 3D, 4D, 5D}});
        doReturn(imgR).when(mockSource).getResult(deviceNameParser, getAtt(doubleImgPub));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSumImgParser);
        final Object result = tangoParser.read();
        final Object value = 30.0;
        assertThat(tangoParser.isImage(), equalTo(false));
        assertThat(result.getClass().isArray(), equalTo(false));
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecAdd() throws DevFailed {
        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserAdd);
        final MVector vect = new MVector(5);
        vect.setEle(0, 1);
        vect.setEle(1, 2);
        vect.setEle(2, 3);
        vect.setEle(3, 4);
        vect.setEle(4, 5);
        r.setValue(vect);
        // r.setValue(new double[] { 1D, 2D, 3D, 4D, 5D });
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserAdd);
        final Object result = tangoParser.read();
        final Object value = new Double[]{2D, 3D, 4D, 5D, 6D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecCos() throws DevFailed {
        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 0D, 1D });

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserCos);
        final MVector vect = new MVector(2);
        vect.setEle(0, 0.0);
        vect.setEle(1, 1.0);
        r.setValue(vect);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserCos);
        final Object result = tangoParser.read();
        final Object value = new Double[]{1D, 0.5403023058681398D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecGenMath() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, "genMath");
        final MVector vect = new MVector(2);
        vect.setEle(0, 0);
        vect.setEle(1, 1);
        r.setValue(vect);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new double[] { 0, 1 });
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/genMath");
        final double[] result = (double[]) tangoParser.read();
        final Object value = new double[]{1, 3};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecGenMath2() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, getAtt(doubleSpectrumPub));
        final MVector vect = new MVector(2);
        vect.setEle(0, 0);
        vect.setEle(1, 1);
        r.setValue(vect);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new double[] { 0, 1 });
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/genMath2");
        final double[] result = (double[]) tangoParser.read();
        final Object value = new double[]{1.0, Math.exp(1)};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecDiv() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserDiv);
        r.setValue(new Double[]{1D, 2D, 3D, 4D, 5D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserDiv);
        final Object result = tangoParser.read();
        final Object value = new Double[]{0.5D, 1D, 1.5D, 2D, 2.5D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecDiv1element() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserDiv2);
        final MVector vect = new MVector(1);
        vect.setEle(0, 1.0);
        r.setValue(vect);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserDiv2);
        final Object result = tangoParser.read();
        System.out.println("result " + result);
        final Object value = new Double[]{0.5D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecFft() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserFft);
        r.setValue(new Double[]{1D, 2D, 3D, 4D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserFft);
        final Object result = tangoParser.read();
        final Object value = new Double[]{10D, -2D, -2D, -2D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecLog() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserLog);
        r.setValue(new Double[]{1D, 2D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserLog);
        final Object result = tangoParser.read();
        final Object value = new Double[]{0D, 0.6931471805599453D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecMax() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserMax);
        r.setValue(new Double[]{1D, 2D, 3D, 4D, 5D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserMax);
        final Object result = tangoParser.read();
        final Object value = 10D;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecMean() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserMean);
        r.setValue(new Double[]{1D, 2D, 3D, 4D, 5D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserMean);
        final Object result = tangoParser.read();
        final Object value = 5.5D;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecMin() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserMin);
        r.setValue(new Double[]{1D, 2D, 3D, 4D, 5D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserMin);
        final Object result = tangoParser.read();
        final Object value = 1D;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecMul() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserMul);
        r.setValue(new Double[]{1D, 2D, 3D, 4D, 5D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserMul);
        final Object result = tangoParser.read();
        final Object value = new Double[]{1D, 4D, 9D, 16D, 25D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecElem() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserElem);
        Double[] valueIn = new Double[]{Math.random(), Math.random(), Math.random(), Math.random(), Math.random()};
        r.setValue(valueIn);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        short index = (short) Math.round(Math.random() * (valueIn.length - 1));
        final AttributeResult indexR = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SCALAR,
                doubleSpectrumParserElem);
        indexR.setValue(index);
        doReturn(indexR).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserElem);
        final Object result = tangoParser.read();
        final Object value = valueIn[index];
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecSetElem() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserSet);
        Double[] valueIn = new Double[]{Math.random(), Math.random(), Math.random(), Math.random(), Math.random()};
        r.setValue(valueIn);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        short index = (short) (valueIn.length + 3);
        final AttributeResult indexR = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SCALAR,
                doubleSpectrumParserSet);
        indexR.setValue(index);
        doReturn(indexR).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));

        double valueToSet = Math.random();
        final AttributeResult doubleR = new AttributeResult(deviceNameParser, getAtt(doublePub), AttrDataFormat.SCALAR,
                doubleSpectrumParserSet);
        doubleR.setValue(valueToSet);
        doReturn(doubleR).when(mockSource).getResult(deviceNameParser, getAtt(doublePub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        Double[] valueOut = new Double[index + 1];
        for (int i = 0; i < valueIn.length; i++) {
            valueOut[i] = valueIn[i];
        }
        for (int i = valueIn.length; i < valueOut.length - 1; i++) {
            valueOut[i] = 0.0;
        }
        valueOut[index] = valueToSet;

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserSet);
        final Object result = tangoParser.read();
        final Object value = valueOut;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecRepElem() throws DevFailed {

        short size = (short) Math.round(Math.random() * 10);
        final AttributeResult sizeR = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SCALAR,
                doubleSpectrumParserRep);
        sizeR.setValue(size);
        doReturn(sizeR).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));

        double valueToSet = Math.random();
        final AttributeResult doubleR = new AttributeResult(deviceNameParser, getAtt(doublePub), AttrDataFormat.SCALAR,
                doubleSpectrumParserRep);
        doubleR.setValue(valueToSet);
        doReturn(doubleR).when(mockSource).getResult(deviceNameParser, getAtt(doublePub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        Double[] valueOut = new Double[size];
        for (int i = 0; i < size; i++) {
            valueOut[i] = valueToSet;
        }

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserRep);
        final Object result = tangoParser.read();
        final Object value = valueOut;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecSize() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserSize);
        r.setValue(new Double[]{1D, 2D, 3D, 4D, 5D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserSize);
        final Object result = tangoParser.read();
        final Object value = (int) 5;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecSin() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserSin);
        r.setValue(new Double[]{0D, 1D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 0D, 1D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserSin);
        final Object result = tangoParser.read();
        final Object value = new Double[]{0D, 0.8414709848078965D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecStd() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserStd);
        r.setValue(new Double[]{1D, 2D, 3D, 4D, 5D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 1D, 2D, 3D, 4D, 5D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserStd);
        final Object result = tangoParser.read();
        final Object value = 3.0276503540974917D;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadDoubleSpecSub() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, doubleSpectrumParserSub);
        r.setValue(new Double[]{2D, 3D, 4D, 5D, 6D});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new Double[] { 2D, 3D, 4D, 5D, 6D });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleSpectrumParserSub);
        final Object result = tangoParser.read();
        final Object value = new Double[]{1D, 2D, 3D, 4D, 5D};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadFloat() throws DevFailed {
        try {
            final AttributeResult floatR = new AttributeResult(deviceNameParser, getAtt(floatPub),
                    AttrDataFormat.SCALAR, floatParser);
            floatR.setValue((float) 1);
            doReturn(floatR).when(mockSource).getResult(deviceNameParser, getAtt(floatPub));
            // final TangoAttribute pub = new TangoAttribute(floatPub);
            // pub.write((float) 1);
            final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + floatParser);
            final Object result = tangoParser.read();
            final Object value = (float) 3;
            assertThat(result, equalTo(value));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testReadFloatSpecAdd() throws DevFailed {

        final AttributeResult floatR = new AttributeResult(deviceNameParser, getAtt(floatSpectrumPub),
                AttrDataFormat.SCALAR, floatSpectrumParserAdd);
        floatR.setValue(new Float[]{1.0f, 2.0f, 3.0f, 4.0f, 5.0f});
        doReturn(floatR).when(mockSource).getResult(deviceNameParser, getAtt(floatSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(floatSpectrumPub);
        // pub.write(new Float[] { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + floatSpectrumParserAdd);
        final Object result = tangoParser.read();
        final Object value = new Float[]{1.5f, 2.5f, 3.5f, 4.5f, 5.5f};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadIntSpecAdd() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserAdd);
        r.setValue(new Integer[]{1, 2, 3, 4, 5});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 1, 2, 3, 4, 5 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserAdd);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{2, 3, 4, 5, 6};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadBooleanExtractor() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SPECTRUM,
                shortParserBooleanExtract);
        r.setValue((short) 4);
        when(mockSource.getResult(deviceNameParser, getAtt(shortPub))).thenReturn(r);
        // final TangoAttribute pub = new TangoAttribute(shortPub);
        // pub.write((short) 4);
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortParserBooleanExtract);
        final boolean result = tangoParser.read(boolean.class);
        assertThat(result, equalTo(false));
    }

    @Test
    public void testReadBooleanExtractor2() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SPECTRUM,
                shortParserBooleanExtract);
        r.setValue((short) 9);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));
        // final TangoAttribute pub = new TangoAttribute(shortPub);
        // pub.write((short) 9);
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortParserBooleanExtract);
        final boolean result = tangoParser.read(boolean.class);

        assertThat(result, equalTo(true));
    }

    @Test
    public void testReadIntSpecCos() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserCos);
        r.setValue(new Integer[]{0, 1});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 0, 1 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserCos);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{1, 0};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadIntSpecDiv() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserDiv);
        r.setValue(new Integer[]{2, 4, 6, 8, 10});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 2, 4, 6, 8, 10 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserDiv);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{1, 1, 1, 1, 1};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadIntSpecFft() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserFft);
        r.setValue(new Integer[]{1, 2, 3, 4});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 1, 2, 3, 4 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserFft);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{10, -2, -2, -2};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadVSUM() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(doubleSpectrumPub),
                AttrDataFormat.SPECTRUM, calculDoubleSpectrumVSUM);
        final MVector vect = new MVector(2);
        vect.setEle(0, 1);
        vect.setEle(1, 2);
        r.setValue(vect);
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doubleSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(doubleSpectrumPub);
        // pub.write(new double[] { 1, 2 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + calculDoubleSpectrumVSUM);
        final double result = tangoParser.read(double.class);
        assertThat(result, equalTo(3.0));

    }

    @Test
    public void testReadIntSpecLog() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserLog);
        r.setValue(new Integer[]{1, 2});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 1, 2 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserLog);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{0, 0};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadIntSpecMul() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserMul);
        r.setValue(new Integer[]{2, 4, 6, 8, 10});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 2, 4, 6, 8, 10 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserMul);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{4, 16, 36, 64, 100};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadIntSpecSin() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserSin);
        r.setValue(new Integer[]{0, 1});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 0, 1 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserSin);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{0, 0};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadIntSpecSub() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(intSpectrumPub),
                AttrDataFormat.SPECTRUM, intSpectrumParserSub);
        r.setValue(new Integer[]{2, 4, 6, 8, 10});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(intSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(intSpectrumPub);
        // pub.write(new Integer[] { 2, 4, 6, 8, 10 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + intSpectrumParserSub);
        final Object result = tangoParser.read();
        final Object value = new Integer[]{1, 3, 5, 7, 9};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadTimestampDouble() throws DevFailed {
        try {
            final AttributeResult r = new AttributeResult(deviceNameParser, doublePub, AttrDataFormat.SCALAR,
                    doubleParser);
            r.setValue(64.0);
            doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(doublePub));
            // final TangoAttribute pub = new TangoAttribute(doublePub);
            // pub.write(64);
            final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleParser);
            tangoParser.read();
            final long time1 = tangoParser.getTimestamp();
            System.out.println("time 1 " + time1);
            tangoParser.read();
            final long time2 = tangoParser.getTimestamp();
            System.out.println("time 2 " + time2);
            assertThat(time1, not(time2));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    private String getAtt(final String name) {
        return name.substring(name.lastIndexOf("/") + 1);
    }

    @Test
    public void testReadDouble() throws DevFailed {
        final AttributeResult doubleR = new AttributeResult(deviceNameParser, getAtt(doublePub), AttrDataFormat.SCALAR,
                doubleParser);
        doubleR.setValue(64.0);
        doReturn(doubleR).when(mockSource).getResult(deviceNameParser, getAtt(doublePub));
        try {
            // final TangoAttribute pub = new TangoAttribute(doublePub);
            // pub.write(64);
            final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + doubleParser);
            final Object result = tangoParser.read();
            final Object value = 1.0;
            assertThat(result, equalTo(value));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testReadLong() throws DevFailed {
        final AttributeResult floatR = new AttributeResult(deviceNameParser, getAtt(longPub), AttrDataFormat.SCALAR,
                longParser);
        floatR.setValue(64);
        doReturn(floatR).when(mockSource).getResult(deviceNameParser, getAtt(longPub));
        // final TangoAttribute pub = new TangoAttribute(longPub);
        // pub.write(64);
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + longParser);
        final Object result = tangoParser.read();
        final Object value = 1073741824;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShort() throws DevFailed {
        final AttributeResult floatR = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SCALAR,
                shortParser);
        floatR.setValue((short) 128);
        doReturn(floatR).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));
        // final TangoAttribute pub = new TangoAttribute(shortPub);
        // pub.write((short) 128);
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortParser);
        final Object result = tangoParser.read();
        final Object value = (short) 256;
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecAdd() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserAdd);
        r.setValue(new Short[]{1, 2, 3, 4, 5});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 1, 2, 3, 4, 5 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserAdd);
        final Object result = tangoParser.read();
        final Object value = new Short[]{2, 3, 4, 5, 6};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecCos() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserCos);
        r.setValue(new Short[]{0, 1});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));
        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 0, 1 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserCos);
        final Object result = tangoParser.read();
        final Object value = new Short[]{1, 0};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecDiv() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserDiv);
        r.setValue(new Short[]{2, 4, 6, 8, 10});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 2, 4, 6, 8, 10 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserDiv);
        final Object result = tangoParser.read();
        final Object value = new Short[]{1, 1, 1, 1, 1};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecFft() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserFft);
        r.setValue(new Short[]{1, 2});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 1, 2 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserFft);
        final Object result = tangoParser.read();
        final Object value = new Short[]{3, -1};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecLog() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserLog);
        r.setValue(new Short[]{1, 2});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 1, 2 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserLog);
        final Object result = tangoParser.read();
        final Object value = new Short[]{0, 0};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecMul() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserMul);
        r.setValue(new Short[]{2, 4, 6, 8, 10});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 2, 4, 6, 8, 10 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserMul);
        final Object result = tangoParser.read();
        final Object value = new Short[]{4, 16, 36, 64, 100};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecSin() throws DevFailed {

        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserSin);
        r.setValue(new Short[]{0, 1});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 0, 1 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserSin);
        final Object result = tangoParser.read();
        final Object value = new Short[]{0, 0};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadShortSpecSub() throws DevFailed {
        final AttributeResult r = new AttributeResult(deviceNameParser, getAtt(shortSpectrumPub),
                AttrDataFormat.SCALAR, shortSpectrumParserSub);
        r.setValue(new Short[]{2, 3, 4, 5, 6});
        doReturn(r).when(mockSource).getResult(deviceNameParser, getAtt(shortSpectrumPub));

        // final TangoAttribute pub = new TangoAttribute(shortSpectrumPub);
        // pub.write(new Short[] { 2, 3, 4, 5, 6 });

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + shortSpectrumParserSub);
        final Object result = tangoParser.read();
        final Object value = new Short[]{1, 2, 3, 4, 5};
        assertThat(result, equalTo(value));
    }

    @Test
    public void testReadString() throws DevFailed {
        final AttributeResult doubleR = new AttributeResult(deviceNameParser, getAtt(stringPub), AttrDataFormat.SCALAR,
                stringParser);
        doubleR.setValue("blabla");
        doReturn(doubleR).when(mockSource).getResult(deviceNameParser, getAtt(stringPub));
        // final TangoAttribute pub = new TangoAttribute(stringPub);
        // pub.write("blabla");
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + stringParser);
        final Object result = tangoParser.read();
        final Object value = "blablatest";
        assertThat(result, equalTo(value));
    }

    @Test
    public void testWriteComplexDouble() throws DevFailed {
        try {
            // final TangoAttribute pub = new TangoAttribute(wDoubleComplexPub);
            // pub.write(1.0);

            final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + wDoubleComplexParser);
            tangoParser.write(4.0);
            final double expected = 1.0;
            verify(mockSource).write(eq(wDoubleComplexParser), eq(expected));
            // final Object value = pub.read();
            // final Object result = (double) 1;
            // assertThat(result, equalTo(value));
        } catch (final DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void testWriteDouble() throws DevFailed {
        // final TangoAttribute pub = new TangoAttribute(wDoublePub);
        // pub.write(1.0);

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + wDoubleParser);
        tangoParser.write(1.0); // It's this write that important here
        final double expected = 2.0;
        verify(mockSource).write(eq(wDoubleParser), eq(expected));
        // final Object result = pub.read();
        // final Object value = (double) 2D;
        // assertThat(result, equalTo(value));
    }

    @Test
    public void testWriteDoubles() throws DevFailed {
        // final TangoAttribute pub1 = new TangoAttribute(wDoublePub);
        // pub1.write(1.0);
        //
        // final TangoAttribute pub2 = new TangoAttribute(wDoubleComplexPub);
        // pub2.write(1.0);

        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + wDoublesParser);
        tangoParser.write(3.0); // It's this write that important here

        final double expected = 5;
        verify(mockSource).write(eq(wDoublesParser), eq(expected), eq(expected));
        // final Object result1 = pub1.read();
        // final Object value1 = (double) 3D;
        // final Object result2 = pub2.read();
        // final Object value2 = (double) 3D;
        // assertThat(result1, equalTo(value1));
        // assertThat(result2, equalTo(value2));
    }

    @Test
    public void testWriteDoubleSpec() throws DevFailed {
        // As it's difficult to build another device in a unitary test, the test is
        // valid if there's no exceptions raised.
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + wDoubleParserSpec);
        Double[] writeVal = new Double[]{1.0, 2.0, 3.0, 4.0, 5.0};
        tangoParser.write(new Double[]{1.0, 2.0, 3.0, 4.0, 5.0});
        
        //On inputNames attribute, write value and read values are equals.
        Object readVal = tangoParser.read();
        assertThat(readVal, equalTo(writeVal));

        final Object[] expected = new Double[]{2.0, 4.0, 6.0, 8.0, 10.0};
        verify(mockSource).write(eq(wDoubleParserSpec), eq(expected));
    }

    @Test
    public void testWriteDoubleIOSpec() throws DevFailed {
        // As it's difficult to build another device in a unitary test, the test is
        // valid if there's no exceptions raised.
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/IOSpectrum");
        tangoParser.write(new Double[]{2.0, 4.0, 6.0, 8.0, 10.0});
        final Object[] expected = new Double[]{1.0, 2.0, 3.0, 4.0, 5.0};
        verify(mockSource).write(eq("IOSpectrum"), eq(expected));
    }

    @Test
    public void testWriteDoubleIOSpecFact() throws DevFailed {
        // As it's difficult to build another device in a unitary test, the test is
        // valid if there's no exceptions raised.
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/IOSpectrumFactor");
        tangoParser.write(3.0);
        final Object[] expected = new Double[]{3.0, 6.0, 9.0, 12.0, 15.0, 18.0};
        verify(mockSource).write(eq("IOSpectrumFactor"), eq(expected));
    }

    @Test
    public void testWriteSQRTDouble() throws DevFailed {
        // final TangoAttribute pub = new TangoAttribute(wDoublePub);
        // pub.write(1.0);
        final TangoAttribute tangoParser = new TangoAttribute(deviceNameParser + "/" + wDoubleSQRTParser);
        tangoParser.write(4);
        final double expected = 2.0;
        verify(mockSource).write(eq(wDoubleSQRTParser), eq(expected));

        // final Object result = pub.read();
        // final Object value = (double) 2;
        // assertThat(result, equalTo(value));
    }

    /**
     * Test command setExpression for READ attribute. The name of the test start
     * by z because we want execute it at the end to not affect other test.
     *
     * @throws DevFailed
     */
    @Test
    public void zSetExpReadTest() throws DevFailed {
        final AttributeResult r1 = new AttributeResult(deviceNameParser, getAtt(shortPub), AttrDataFormat.SCALAR,
                "calculShort");
        r1.setValue((short) 1);
        doReturn(r1).when(mockSource).getResult(deviceNameParser, getAtt(shortPub));

        // Write 1 on publisher
        // final TangoAttribute pub = new TangoAttribute(shortPub);
        // pub.write((short) 1);
        // Execute command to change expression
        final TangoCommand cmd = new TangoCommand(deviceNameParser, "SetExpression");
        cmd.executeExtract(new String[]{shortParser, "value1Short*3"});
        // Read attribute
        final TangoAttribute attr = new TangoAttribute(deviceNameParser + "/calculShort");
        final Object res = attr.read();

        // Assert
        final Object value = (short) 3;
        assertThat(res, equalTo(value));
    }

    /**
     * Test command setExpression for WRITE attribute. The name of the test
     * start by z because we want execute it at the end to not affect other
     * test.
     *
     * @throws DevFailed
     */
    @Test
    public void setExpWriteTest() throws DevFailed {

        // Write 1 on publisher
        // final TangoAttribute pub = new TangoAttribute(wDoublePub);
        // pub.write((short) 1);

        final TangoCommand cmd = new TangoCommand(deviceNameParser, "SetExpression");
        try {
            // Execute command to change expression
            cmd.executeExtract(new String[]{"A", "value1DoubleW=A+2"});
            // mocking
            final double expected = 3;
            // Write attribute
            final TangoAttribute attr = new TangoAttribute(deviceNameParser + "/A");
            attr.write(1.0D);
            verify(mockSource).write(eq("A"), eq(expected));
            // Read the new value on the publisher
            // final Object res = pub.read();
            // Assert
            // final Object value = 3.0D;
            // assertThat(res, equalTo(value));
        } finally {
            // reset initial value for others tests
            cmd.executeExtract(new String[]{"A", "value1DoubleW=A+1"});
        }
    }
}
