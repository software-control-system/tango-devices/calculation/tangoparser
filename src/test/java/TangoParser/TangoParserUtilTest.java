/**
 *
 */
package TangoParser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Pattern;

import org.junit.Test;

import fr.soleil.tango.server.tangoparser.TangoParserUtil;

/**
 * @author GRAMER
 * 
 */
public class TangoParserUtilTest {

    /**
     * Test method for {@link TangoParser.TangoParserUtil.#removeEmptyLines()}.
     */
    @Test
    public void testRemoveEmptyLines() {

	String[] par = { "" };
	String[] excepted = { "" };
	assertEquals(excepted, TangoParserUtil.removeEmptyLines(par));

	par = new String[] { "", "", "" };
	excepted = new String[] { "" };
	assertEquals(excepted, TangoParserUtil.removeEmptyLines(par));

	par = new String[] { "", "a" };
	excepted = new String[] { "a" };
	assertEquals(excepted, TangoParserUtil.removeEmptyLines(par));

	par = new String[] { "", "a", "" };
	excepted = new String[] { "a" };
	assertEquals(excepted, TangoParserUtil.removeEmptyLines(par));

	par = new String[] { "a", "b", "" };
	excepted = new String[] { "a", "b" };
	assertEquals(excepted, TangoParserUtil.removeEmptyLines(par));

	par = new String[] { "a", "", "b" };
	excepted = new String[] { "a", "b" };
	assertEquals(excepted, TangoParserUtil.removeEmptyLines(par));

	par = new String[] { "", "a", "", "b", "" };
	excepted = new String[] { "a", "b" };
	assertEquals(excepted, TangoParserUtil.removeEmptyLines(par));

    }

    @Test
    public void testRegex() {
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable,domain/family/member/attribute"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable ,domain/family/member/attribute"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable, domain/family/member/attribute"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable , domain/family/member/attribute"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "var-iab_L1e, doM_-ain/fam9ily/memb1er/attribute2"));

	// no tabulation accepted
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "var-iab_L1e,\t doM_-ain/fam9ily/memb1er/attribute2"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "domain/family/member/attribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, ",domain/family/member/attribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, " ,domain/family/member/attribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable,,domain/family/member/attribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variabl,e,domain/family/member/attribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable,domainfamily/member/attribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable,domainfamilymember/attribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable,domainfamilymemberattribute"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable,"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable,  "));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable  "));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_ATTR, "variable"));

	// ///////////////////////////////////InputNames\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,variable=expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "vA-_8riable,variable=expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable, variable = expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,variab-_9le=expression*/-()\"\"12. &&R||P"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "DevDouble variable,variable=expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "DEvDoUble variable,variable=expression"));

	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable ,variable=expression"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,variable="));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,variable= "));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,=expression"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable, =expression"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,cexpression"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable, = "));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "variable,="));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "vari,able,variable=expression"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "vari,able, variable=expression"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "DEvDoUble  variable,variable=expression"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_INPUT, "pasUnTypeTango variable,variable=expression"));

	// ///////////////////////////////////OutputNames\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "variable,expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "variable, expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "variable, eX-_pression*/-113()&&||p"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "SCALAR variable,expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "SCALAR DevDoUble variable,expression"));
	assertTrue(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "DevDoUble variable,expression"));

	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "PasUnFormatOuTypeAngo variable,variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "PasUnFormat OuTypeAngo variable,variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "PasUnFormat OuTypeAngo ,variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "DevDoUble SCALAR variable ,variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "scalar DevDouble variable ,variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "scalar variable ,variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "variable,"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "variable"));
	assertFalse(Pattern.matches(TangoParserUtil.PATTERN_OUTPUT, "variable expression variable"));
    }
}
