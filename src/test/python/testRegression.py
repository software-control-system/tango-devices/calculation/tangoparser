from PyTango import *
import sys
import time

# 
try :
    # A reference on the DataBase
    db = Database()
    # The device name we want to create
    new_device_name="dev/tangoparser/parser"
    # Define the Tango Class served by this DServer
    new_device_info = DbDevInfo()
    new_device_info._class = "TangoParser"
    new_device_info.server = "tangoparser/1"
    # add this device
    print "Creation Device :" , new_device_name
    new_device_info.name = new_device_name
    db.add_device(new_device_info)

    # Get proxy on the new device
    print "Getting DeviceProxy :" , new_device_name
    testDeviceProxy = DeviceProxy(new_device_name)

    
    # begin of test
    
    ##############################################
    #
    # Property test
    #
    ##############################################
    #
    # 1.1.1 Good definition of properties
    print "1.1.1 Good definition of properties"
    properties = {"OutputNames" : ["exp1,X+Y"], 
                  "AttributeNames" : ["X,tango/tangotest/1/long_scalar",
                                      "Y,tango/tangotest/1/long_scalar"]
                 }
    db.put_device_property(new_device_name, properties)
    testDeviceProxy.command_inout("Init");
    print "Test pass :"
    print " 1- State = StandBy"    
    print " 1- input properties = output properties"    

    #
    # 1.1.2 One expression of two is configured
    properties = {"OutputNames" : ["exp1,X+Y","exp2"], 
                  "AttributeNames" : ["X,tango/tangotest/1/long_scalar",
                                      "Y,tango/tangotest/1/long_scalar"]
                 }
    db.put_device_property(new_device_name, properties)
    testDeviceProxy.command_inout("Init");
    testDeviceProxy
    # delete this device
    # print "Delete Device :" , new_device_name
    # db.delete_device(new_device_name1)
    
except:
    print "failed with exception !"
    print sys.exc_info()[0]
    #print sys.exc_type
    #print sys.exc_value
    #print sys.exc_traceback
    raise
    
