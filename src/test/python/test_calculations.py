from PyTango import *
import sys
import time

# This test expects a TangoParser device have been defined
# with these device properties :
#    AttributeNames : tango/tangotest/1/short_scalar
#                     tango/tangotest/2/short_scalar
#
#    OutputNames    : mean
# N.B. : the device expects for now only double typed attributes

try :
	parser = DeviceProxy("test/tangoparser/parser")
	parser.SetExpression(["exp1","(X+Y)/2.0"])
	for i in range(10):
            exp1 = parser.read_attribute("Exp1")
            print "exp1 = ",exp1.value	

except:
	print "failed with exception !"
	print sys.exc_info()[0]
	#print sys.exc_type
	#print sys.exc_value
	#print sys.exc_traceback
	raise
