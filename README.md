# Table des matières
[[_TOC_]]

# Description
L'objectif du TangoParser est de fournir de nouveaux attributs TANGO qui soient le résultat de calculs à partir d’attributs déjà existants
## Exemple
Je calcule la position du faisceau au Point Z en fonction des mesures effectuées sur les xbpm 1 et xbpm2 (mesures déjà disponibles dans TANGO)
<img src="doc/img/exemple_usage.png" alt="Exemple d'usage" width="600px"/>

# Liste des propriétés
* AttributesNames
    * Liste des attributs des devices sources avec leur nom local.
* InputNames
    * Définit les attributs "write-only" du device
* OuputNames
    * Définit les attributs "read-only" du device 
* IONames
    * Définit les attributs "read-write" du device 
* AutoInputProperties
    * Définit les propriétés d’écriture automatique des attributs IONames
* ScanMode:
    * TRUE : lecture des attributs et de l’état de façon synchrone
    * FALSE : lecture de façon asynchrone
* PeriodRefresh
    * Délai de rafraîchissement des attributs et état en mode asynchrone en ms.
* DiagnosticFunctions
    * TRUE : le parser ajoute le calcul des fonctions MIN, MAX, MEAN, STD sur les AttributesNames
* MovingState
    * Indique au ScanServer l’état auquel il doit s’attendre lorsque le device est en « moving »
* PriorityList
    * Liste des états selon leur priorité. Cela permet de composer les états des différents devices appelés et de remonter le plus important.
* AutoInitEnabled
    * TRUE : Active l'auto-init. Etat par défaut. Le TangoParser restera dans l'état d'init jusqu'à ce que tous les attributs aient pu être construits et que tout les attribut externe dont ils dépendent aient pu être atteints.
    * FALSE : Désactive l'auto-init. Le TangoParser ne fera qu'une tentative par attribut si certains ne peuvent être construit ou dépendent d'attributs injoignables, le Tangoparser sera dans un état FAULT.
* ScalarConstants
    * Liste de constantes de type scalaire. Chaque ligne est du type `int c, 299792458` ou encore `double planck, 6.62607015e10-34`
* DoubleArrayConstants
    * Liste de fichiers contenants des tableaux 2D de type double
* Array_XXXXXX
    * Propriété à nommage dynamique pour déclarer un tableau de double. Le nom de la contante sera `XXXXXX`.
* InterpolationFunctions
    * Liste de définition de fonctions d'interpolation (Spline, linéaire, ...). Ces fonctions sont définies à l'aide de constantes.

![Propriétés](doc/img/properties_full.png)

# State et status
Le parser indique le State de chacun des Devices utilisés pour les calculs
L’état du parser prend le state avec la priorité la plus élevée. (Voir propriété : PriorityList)

![Exemple de state et status](doc/img/state_status.png)

# Commandes
## EvaluateExpression
La commande execute la fonction donnée en paramètre et renvoie le résultat. Si l'expression contient des erreurs, un message apparaitra.
![Evaluation d'expression](doc/img/expression.png)

## GetExpression
En donnant le nom d'un attribut en paramètre, la fonction renvoie son expression.

# Détail des propriétés principales
## AttributesNames
Les attributs à lire dans le système Tango sont décrits par la propriété `AttributeNames`

### Exemple
* `p1,TDL-D13-1/DG/XBPM.1/verticalPosition1`
* `p2,TDL-D13-1/DG/XBPM.1/verticalPosition2`
* `p3,TDL-D13-1/DG/XBPM.2/verticalPosition1`
* `p4,TDL-D13-1/DG/XBPM.2/verticalPosition2`

La « variable » `p1` va maintenant contenir le résultat de la lecture de l’attribut `TDL-D13-1/DG/XBPM.1/verticalPosition1` et sera utilisable dans les autres propriétés.

## InputNames
Les attributs dynamiques en écriture seule sont dans la propriété `InputNames`

### Exemple
* `DevDouble p5, p1=p5*3`
* `DevDouble p5, p2=p5/2`

* Créer un nouveau attribut `p5`,
* A chaque fois qu’un client écrit dans l’attribut p5, le Parser écrit dans les attributs p1 et p2 les nouvelles valeurs calculées en fonction de p5 et selon les expressions définies dans InputNames.
* La partie read visible dans l'ATKPanel contient la dernière valeur saisie.

<img src="doc/img/input_names.png" alt="Exemple d'InputNames" width="600px"/>

## OuputNames
Les attributs en lecture seule sont décrits par la propriété `OutputNames`

### Exemple
* `DevDouble positionZ1,(p1+p3)/2`
* `DevDouble positionZ2,(p2+p4)/2`
* `DevDouble angleZ1,(p3-p1)/3.030`
* `DevDouble angleZ2,(p4-p2)/3.030`

4 nouveaux attributs de type DevDouble sont maintenant disponibles dans TANGO 

<img src="doc/img/output_names.png" alt="Exemple d'OutputNames" width="400px"/>

## IONames
Les nouveaux attributs en écriture et en lecture sont décrits par la propriété IONames

### Exemple
DevDouble IO;<span style="color: green">p1+p2</span>;<span style="color: orange">p1=IO+1</span>;<span style="color: orange">p2=IO+2</span>

> :warning: « `;` » est le séparateur

Un nouvel attribut `IO` est disponible. Sa partie read contiendra le résultat de « <span style="color: green">p1+p2</span> » et lorsque l’on écrira dessus, le Parser écrira sur les attributs p1 et p2. 

On ne peut définir qu’une seule expression (la première) pour la lecture et autant que souhaitées pour l’écriture.

![Exemple d'IONames](doc/img/io_names.png)

### Variables
On peut créer de nouvelles variables (non définies dans AttributeNames) dans InputNames ou IONames:
#### Exemple
Création des variables « aux1 » et « coef »: 

nbbin;<span style="color: green">aux1</span>;<span style="color: orange">aux1=nbbin</span>  
detcRW;<span style="color: green">10*(coef^(100/aux1))</span>;<span style="color: orange">coef=detcRW</span>

![Exemple de variables](doc/img/variables.png)

Il y a deux attributs en Read/Write. Chaque écriture sur detcRW ou nbbin modifie la partie read de detcRW.  
On peut réutiliser les variables créées dans OuputNames.

## AutoInputProperties
On peut automatiser l’écriture des attributs définis dans IONames par la propriété AutoInputProperties.  
Chaque ligne paramètre un attribut en précisant :
* Son nom,
* Son activation au démarrage du TangoParser
* Sa fréquence d’actualisation,
* La précision, elle est optionnelle et ignorée dans le cas des types non numériques (booléen, chaines de caractères). Dans le cas des vecteurs, elle s’applique à chaque élément. Si le symbole "%" en présent à la fin, alors l'écart sera évaluer en pourcentage.

Pour chaque attribut IOName présent dans la liste des valeurs de AutoInputProperties, un attribut booléen en Read/Write est créé pour permettre son activation ou désactivation. Il n’est visible qu’en mode expert.

### Exemple
valIO;true;0.5;0.01  
valIO2;false;1  
valIO3;false;1;1%;last_read

* valIO : La valeur présente dans la partie « output » sera vérifiée toutes les 0,5 secondes et, si elle change de plus de 0,01 en valeur absolue, elle est écrite dans la partie « input ». Dès le démarrage du TangoParser, le processus de recopie est activé et une première recopie à lieu.  
 :warning: Attention, ce mécanisme est dangereux. :warning:  
* valIO2 : La recopie devra être activée via l’attribut booléen « valIO2AutoInputActivation ». Il sera vérifié toutes les secondes et recopié en cas de changement strict.
* valIO3 : La recopie devra être activée via l’attribut booléen « valIO2AutoInputActivation ». Il sera vérifié toutes les secondes et recopié en cas de changement de plus de 1% par rapport à la dernière valeur *lue*.
* valIO4 : La recopie devra être activée via l’attribut booléen « valIO2AutoInputActivation ». Il sera vérifié toutes les secondes et recopié en cas de changement strict.

### Exemple d'usage
Dans l'exemple ci-dessous, une device Publisher accessible par test/tools/test.pub contient 3 attributs booléen : valX, valZ, valGo et fofbPrev.  
La configuration va faire en sorte que :
* valGo deviendra faux si valX ou valY deviennent faux mais ne redeviendra pas vrai si valX et valY repasse à vrai.
* fofbLock ne sera vrai que si valX, valZ et valGo sont vrai.
* Si valX ou valY passe de vrai à faux, alors il faudra passer valGo manuellement à vrai pour que fofbLock redevienne vrai lorsque valX et valY repasseront à vrai.

| Property name       | Value                                                                                                                                         |
| :------------------ | :-------------------------------------------------------------------------------------------------------------------------------------------- |
| AttributeNames      | valX,test/tools/test.pub/valX<br/>valZ,test/tools/test.pub/valZ<br/>valGo,test/tools/test.pub/valGo<br/>fofbPrev,test/tools/test.pub/fofbPrev |
| IONames             | DevBoolean fofbWatcher ; valX && valZ ;valGo = if(fofbPrev && !fofbWatcher, 0, valGo); fofbPrev=fofbWatcher                                   |
| AutoInputProperties | fofbWatcher;false;0.5                                                                                                                         |
| OutputNames         | DevBoolean fofbLock, fofbWatcher&&valGo                                                                                                       |

## ScalarConstants
Cette propriété permet de définir une liste de constantes pouvant être utilisées dans les fonctions. Chaque ligne est du type `int c, 299792458` ou encore `planck, 6.62607015e10-34`.
Le format exact est :
* Type
* Séparateur espace '` `'
* Nom
* Séparateur virgule '`,`'
* Valeur

Le type est optionnel. S'il est ommis, le type par défaut est `double`.

Les types possibles sont :
* int
* long, DevLong
* double, DevDouble
* float, DevFloat
* byte, DevUChar
* boolean, DevBoolean
* short, DevShort
* string, DevString
* state, DevState

## DoubleArrayConstants
Cette propriété liste des fichiers contenants des tableaux 1D ou 2D de type double. Le format de la propriété est :
* Nom
* Séparateur virgule '`,`'
* Le chemin complet du fichier

Les lignes du fichier commencant par le caractère `#` sont considérées comme des commentaires et seront ignorées.

| Property name        | Value                                                           |
| :------------------- | :---------------------------------------------------------------|
| DoubleArrayConstants | FeLinear, /usr/Local/configFiles/Attenuator/Tables/FeLinear.txt<br/>gap4energy, /usr/Local/configFiles/Insertion/gmid_tables/GMID_GAP_ENERGY_HARMONIC11.txt |

### Exemple de fichier
```
#Description du tableau
#Theta		Theta			Correction_en_radians		Theta
3		0.052359878		0.003				0.055359878
4		0.06981317		0.004				0.07381317
5		0.087266463		0.005				0.092266463
6		0.104719755		0.006				0.110719755
7		0.122173048		0.007				0.129173048
8		0.13962634		0.008				0.14762634
9		0.157079633		0.009				0.166079633
10		0.174532925		0.01				0.184532925
11		0.191986218		0.011				0.202986218
12		0.20943951		0.012				0.22143951
13		0.226892803		0.013				0.239892803
14		0.244346095		0.014				0.258346095
15		0.261799388		0.015				0.276799388
16		0.27925268		0.016				0.29525268
17		0.296705973		0.017				0.313705973
18		0.314159265		0.018				0.332159265
19		0.331612558		0.019				0.350612558
20		0.34906585		0.02				0.36906585
```

## Array_XXXXXX
Il est possible de déclarer plusieures propriétés dont le nom commence par `Array_` pour definir des constantes de 2D de type double.

### Exemple
Dans l'exemple ci-dessous, les varaibles `MyTable` et `Transform` contiendrons des tableaux 2D.

| Property name       | Value        |
| :------------------ | :----------- |
| Array_MyTable       | 1    6.2<br/>11.5  7<br/>12   7.5<br/>15   0<br/>22   -11.5<br/>35   -12<br/>100  0 |
| Array_Transform     | 1    -0.005    0<br/>-0.00128   1   0.5684<br/>0.008   -0.0641  1 |


## InterpolationFunctions
Dans cette propriété sont décrit les fonctions d'interpolation à utilisées. Les paramètres pour décrire une fonction d'interpolation sont :
* Son nom
* Son type
  * Univarié (1D)
  * Bivarié (2D)
* La fonction d'interpolation
  * Pour le type univarié
    * AkimaSplineInterpolator
      * Computes a cubic spline interpolation for the data set using the Akima algorithm, as originally formulated by Hiroshi Akima in his 1970 paper "A New Method of Interpolation and Smooth Curve Fitting Based on Local Procedures." J. ACM 17, 4 (October 1970), 589-602. DOI=10.1145/321607.321609 http://doi.acm.org/10.1145/321607.321609
    * DividedDifferenceInterpolator
      * [Newton's Divided Difference Interpolation Formula](https://mathworld.wolfram.com/NewtonsDividedDifferenceInterpolationFormula.html)
    * LinearInterpolator
      * Linear interpolation
    * LoessInterpolator
      * Implements the [Local Regression Algorithm](https://en.wikipedia.org/wiki/Local_regression) (also Loess, Lowess) for interpolation of real univariate functions.
    * NevilleInterpolator
      * Implements the [Neville's Algorithm](https://mathworld.wolfram.com/NevillesAlgorithm.html) for interpolation of real univariate functions. For reference, see Introduction to Numerical Analysis, ISBN 038795452X, chapter 2.
    * SplineInterpolator
      * Computes a natural (also known as "free", "unclamped") cubic spline interpolation for the data set. 
  * Pour le type bivarié
    * BicubicInterpolator
    * PiecewiseBicubicSplineInterpolator
* Les paramètres spécifiques à la fonction d'interpolation
  * Pour l'interpolateur univarié LoessInterpolator, il peut y avoir :
    * Bandwith,
    * RobustnessIter,
    * Accuracy
  * Obligatoirement :
    * Un vecteur par dimension (1 ou 2)
    * Un vecteur de données pour les type univariés et une matrice pour les bivariés.

### Exemple
| Property name          | Value        |
| :--------------------- | :----------- |
| ScalarConstants        | bandwidth, 0.3<br/>int robustnessIters, 2<br/>accuracy, 1e-12 |
| Array_interData        | 24.59176619	24.16909934	23.20788907	13.65140635	21.14026751<br/>24.03879847	1.357068304	9.238308332	8,884726966	9,964167974<br/>1.390188089	12.02936105	10.6542284	20.11593882	10.87249646<br/>12.7620879	26.87401138	11.75438807	12.959019	8.308822284<br/>9.291367244	12.13630009	8.646546494	26.49188221	17.7991182<br/> |
| Array_interX           | 0<br/>1<br/>2<br/>3<br/>4 |
| Array_interF           | 24.35093012<br/>19.34897096<br/>20.31292052<br/>18.55523895<br/>13.02620498 |
| Array_interY           | -10<br/>-5<br/>0<br/>5<br/>10 |
| InterpolationFunctions | interpol1DLoess, Univariate, LoessInterpolator, bandwidth, robustnessiters, accuracy, interx, interf<br/>interpol1DLinear, Univariate, LinearInterpolator, interx, interf<br/> interpol2D, Bivariate, BicubicInterpolator, interx, intery, interdata |

# Format d'attributs
* SPECTRUM  
Tous les attributs créés peuvent être de type SPECTRUM. Il suffit de spécifier "SPECTRUM" devant leur définition. Si c'est le cas, le type d'attribut devient obligatoire.
* IMAGE
Tous les attributs créés peuvent être de type IMAGE. Il suffit de spécifier "IMAGE" devant leur définition. Si c'est le cas, le type d'attribut devient obligatoire.
* SCALAR

Le type par défaut est SCALAR.

# Type d'attribut
Les type d'attributs possibles sont les suivants :
* DevBoolean,
* DevShort,
* DevLong,
* DevFloat,
* DevDouble,
* DevUShort,
* DevULong,
* DevString,
* State,
* DevUChar,
* DevLong64,
* DevULong64

Le type par défaut est DevDouble.

# Opérateurs

 |                              |        |       Double       |      Complex       |           String            |       Vector       |
 | :--------------------------- | :----: | :----------------: | :----------------: | :-------------------------: | :----------------: |
 | Power                        |   ^    | :heavy_check_mark: | :heavy_check_mark: |                             |                    |
 | Boolean Not                  |   !    | :heavy_check_mark: |                    |                             |                    |
 | Unary Plus, Unary Minus      | +x, -x | :heavy_check_mark: | :heavy_check_mark: |                             |                    |
 | Dot product, cross product   | ., ^^  |                    |                    |                             | :heavy_check_mark: |
 | Modulus                      |   %    | :heavy_check_mark: |                    |                             |                    |
 | Division                     |   /    | :heavy_check_mark: | :heavy_check_mark: |                             | :heavy_check_mark: |
 | Multiplication               |   *    | :heavy_check_mark: | :heavy_check_mark: |                             | :heavy_check_mark: |
 | Addition, Subtraction        |  +, -  | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: (only +) | :heavy_check_mark: |
 | Less or Equal, More or Equal | <=, >= | :heavy_check_mark: |                    |     :heavy_check_mark:      |                    |
 | Less Than, Greater Than      |  <, >  | :heavy_check_mark: |                    |     :heavy_check_mark:      |                    |
 | Not Equal, Equal             | !=, == | :heavy_check_mark: | :heavy_check_mark: |     :heavy_check_mark:      |                    |
 | Boolean And                  |   &&   | :heavy_check_mark: |                    |                             |                    |
 | Boolean Or                   |   \|   |                    | :heavy_check_mark: |                             |                    |
 | Assignment                   |   =    | :heavy_check_mark: | :heavy_check_mark: |     :heavy_check_mark:      | :heavy_check_mark: |

# Fonctions et constantes disponibles
## Constantes
Les constantes suivantes sont disponibles :
* i: (0.0, 1.0) 
* e: 2.718281828459045 
* pi: 3.141592653589793

## Trigonometric Functions
All functions accept arguments of the Double and Complex type, except atan2 which only accepts Double arguments.

| Description                | Function Name |
| :------------------------- | :------------ |
| Sine                       | sin(x)        |
| Cosine                     | cos(x)        |
| Tangent                    | tan(x)        |
| Arc Sine2                  | asin(x)       |
| Arc Cosine2                | acos(x)       |
| Arc Tangent                | atan(x)       |
| Arc Tan with 2 parameters  | atan2(y, x)   |
| Secant                     | sec(x)        |
| Cosecant                   | cosec(x)      |
| Co-tangent                 | cot(x)        |
| Hyperbolic Sine            | sinh(x)       |
| Hyperbolic Cosine          | cosh(x)       |
| Hyperbolic Tangent         | tanh(x)       |
| Inverse Hyperbolic Sine    | asinh(x)      |
| Inverse Hyperbolic Cosine  | acosh(x)      |
| Inverse Hyperbolic Tangent | atanh(x)      |

## Log and Exponential Functions
All functions accept arguments of the Double and Complex types.
| Description       | Function Name |
| :---------------- | :------------ |
| Natural Logarithm | ln(x)         |
| Logarithm base 10 | log(x)        |
| Logarithm base 2  | lg(x)         |
| Exponential (e^x) | exp(x)        |
| Power             | pow(x)        |

## Statistical Functions
All functions accept either a vector (e.g. min([1,2,3])) or a set of numbers (e.g. min(1,2,3)).
| Description | Function Name      |
| :---------- | :----------------- |
| Average     | avg(x1,x2,x3,...)  |
| Minimum     | min(x1,x2,x3,...)  |
| Maximum     | max(x1,x2,x3,...)  |
| Vector Sum  | vsum(x1,x2,x3,...) |

## Rounding Functions
| Description      | Function Name         |
| :--------------- | :-------------------- |
| Round            | round(x), round(x, p) |
| Round to integer | rint(x), rint(x, p)   |
| Floor            | floor(x)              |
| Ceiling          | ceil(x)               |

## Miscellaneous Functions
| Description                                                        | Function Name                           |
| :----------------------------------------------------------------- | :-------------------------------------- |
| If                                                                 | if(cond, trueval, falseval)             |
| Str (convert number to string)                                     | str(x)                                  |
| Absolute Value / Magnitude                                         | abs(x)                                  |
| Random number   (between 0 and 1)                                  | rand()                                  |
| Modulus        = x % y                                             | mod(x,y)                                |
| Square Root                                                        | sqrt(x)                                 |
| Sum                                                                | sum(x,y,...)                            |
| Binomial coefficients                                              | binom(n, i)                             |
| Signum (-1,0,1 depending on sign of argument)                      | signum(x)                               |
| Apply a fast fourier transform on a vector                         | fft(x)                                  |
| Sinus on vectors. Apply sinus on all vector elements               | vsin(x)                                 |
| Cosinus on vectors. Apply cosinus on all vector elements           | vcos(x)                                 |
| Logarithm on vectors. Apply logarithm on all vector elements       | vlog(x)                                 |
| Adds a scalar value to each vector elements                        | add(vector, scalar)                     |
| Substract a scalar value to each vector elements                   | sub(vector, scalar)                     |
| Multiply on vectors. Multiply elements of two vectors one by one   | mul(x, y)                               |
| Division on vectors. Divide elements of two vectors one by one     | div(x, y)                               |
| Retrieve the lenght of the given vector                            | getSpectrumSize(vector)                 |
| Retrieve the element of the given vector at the given index        | getSpectrumElem(vector, index)          |
| Set the given element of the given vector at the given index       | setSpectrumElem(vector, index, element) |
| Return a vector composed by the given element n times              | replicate(element, n)                   |
| Mean computation on a vector                                       | mean(x)                                 |
| Standard deviation computation on a vector                         | std(x)                                  |
| Extract the maximum value on a vector                              | max(x)                                  |
| Extract the minimum value on a vector                              | min(x)                                  |
| Extract a bit of an integer value. Takes 2 parameters.             | boolextract(integerValue, bit index)    |
| Convert a number to a string in a given base.                      | toBase(x)                               |
| Shortcut to toBase(x, 10)                                          | toDec(x)                                |
| Shortcut to toBase(x, 16)                                          | toHex(x)                                |
| Convert a string representing a number in a given base to a number | fromBase(str, x)                        |
| Shortcut to fromBase(str, 10)                                      | fromDec(str)                            |
| Shortcut to fromBase(str, 16)                                      | fromHex(str)                            |
| Extract a substring from a string.                                 | substring(str, beginIndex, endIndex)    |
| Returns the length of a given string                               | stringlen(str)                          |
| Shortcut to substring(str, 0, index)                               | left(str, index)                        |
| Shortcut to substring(str, stringlen(str) - index, stringlen(str)) | right(str, index)                       |

## Complex Functions
| Description                                                        | Function Name   |
| :----------------------------------------------------------------- | :-------------- |
| Real Component                                                     | re(c)           |
| Imaginary Component                                                | im(c)           |
| Complex Modulus (Absolute Value)                                   | cmod(c)         |
| Argument (Angle of complex value, in radians)                      | arg(c)          |
| Complex conjugate                                                  | conj(c)         |
| Complex, constructs a complex number from real and imaginary parts | complex(x, y)   |
| Polar, constructs a complex number from modulus and argument       | polar(r, theta) |

## Notes
* Functions like sqrt(-1) will return a complex result.
* Functions like acos(2) will return Double.NaN.

# Exemples
## Exemple d'atttribut SPECTRUM avec transformée de fourier
Un nouvel attribut A5 est crée et est le résultat de la multiplication de B2 par un scalaire
B2 est un attribut spectrum READ-ONLY relu sur un Device Tango
Un nouvel attribut A6 est la somme de 2 attributs spectrum

![Exemple de propriétés spectrum](doc/img/spectrum_prop.png)

![Exemple de spectrum dans l'ATKPanel](doc/img/spectrum_atk.png)

# Exemple d'utilisation des fonction sur les attributs SPECTRUM
* AttributeNames:
  * B2,tango/tangotest/sa_2/double_spectrum_ro
* OutputNames:
  * SPECTRUM DevDouble A1,vlog(B2)
      * Chaque point du spectrum A1 contient le log du point de même indice du spectrum B2
  * SPECTRUM DevDouble A2,vcos(B2)
    * Chaque point du spectrum A2 contient le cos du point de même indice du spectrum B2
  * SPECTRUM DevDouble A3,vsin(B2)
    * Chaque point du spectrum A3 contient le sin du point de même indice du spectrum B2
  * SPECTRUM DevDouble A4,add(B2,5)
    * Chaque point du spectrum A4 contient la somme du scalaire de valeur 5 et du point de même indice du spectrum B2
  * SPECTRUM DevDouble A5,sub(B2,3)
    * Chaque point du spectrum A5 contient la différence du point de même indice du spectrum B2 et du scalaire de valeur 3


# Aide au diagnostic
## Les logs
Si les équations contiennent des erreurs, celles-ci seront listées dans l'attribut "log".

![Exemple de log d'erreur](doc/img/log_error.png)

## Tester les fonctions
A l'aide de la commande `evaluateExpression`, il est possible de tester une fonctions qui n'aurait pas fonctionnée à l'init du device.

## Vérifier l'expression d'un attribut
Cela peut se faire soit par la commande `GetExpression` soit dans le détail de l'attribut dans l'ATKPanel.

![Exemple de variables](doc/img/attribut_description.png)
